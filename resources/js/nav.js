//Menu
$(".hamburguesa").click(function () {
    $("body").toggleClass('no-scroll');
    $(".static-background").toggleClass('visible');
    $(".left-menu").toggleClass('visible');
})

$(".static-background").click(function () {
    $("body").removeClass('no-scroll');
    $(".static-background").removeClass('visible');
    $(".left-menu").removeClass('visible');
})

$(".left-menu .close").click(function () {
    $("body").removeClass('no-scroll');
    $(".static-background").removeClass('visible');
    $(".left-menu").removeClass('visible');
})

//User panel
$(".user-panel").on("click", function () {
    $(".user-menu").toggleClass("active");
});

$("body").on("click", function (e) {
    if(!$(".right-items").is(e.target) && $(".right-items").has(e.target).length === 0 && !$(".user-menu").is(e.target) && $(".user-menu").has(e.target).length === 0) {
        $(".user-menu").removeClass("active");
    }
});

$(".open-register").on("click", function () {
    showLogin();
    $(".login-panel form.register").addClass("active");
    $(".login-panel form.login").removeClass("active");
});

$(".open-login").on("click", function () {
    showLogin();
    $(".login-panel form.register").removeClass("active");
    $(".login-panel form.login").addClass("active");
});

function showLogin() {
    $(".user-menu").removeClass("active");
    $(".login-panel").addClass("active");
    $("body").addClass('no-scroll');
}

$(".login-panel").on("click", function (e) {
    if(!$(".login-panel .wrapper").is(e.target) && $(".login-panel .wrapper").has(e.target).length === 0) {
        $(".login-panel").removeClass("active");
        $("body").removeClass('no-scroll');
    }
});

$(".show-fields").on("click", function () {
    $(this).siblings(".hidden").removeClass("hidden");
});
