$(".share").on("click", function () {
    console.log($(this).attr('url'))
    $(this).attr('url')
    $("#shareModal .fb a").attr("href", 'https://www.facebook.com/sharer/sharer.php?u=https://www.wikihomes.es/anuncio/' + $(this).attr('url'));
    $("#shareModal .tw a").attr("href", 'https://twitter.com/intent/tweet?text=Wikihomes+Anuncio&url=https://www.wikihomes.es/anuncio/' + $(this).attr('url'));
    $("#shareModal .wt a").attr("href", 'https://wa.me/?text=https://www.wikihomes.es/anuncio/' + $(this).attr('url'));
    $("#shareModal .gm a").attr("href", 'https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=&su=Wikihomes+Anuncio&body=www.wikihomes.es/anuncio/'+$(this).attr('url')+'&ui=2&tf=1&pli=1');
    $("#shareModal .cursor-pointer").attr("url", 'https://www.wikihomes.es/anuncio/' + $(this).attr('url'));
});

$(".share-clipboard").on("click", function () {
    copyToClipboard($(this).attr('url'));
});

function copyToClipboard(text) {
    var textarea = document.createElement('textarea');
    textarea.textContent = text;
    document.body.appendChild(textarea);

    var selection = document.getSelection();
    var range = document.createRange();

    range.selectNode(textarea);
    selection.removeAllRanges();
    selection.addRange(range);

    console.log('copy success', document.execCommand('copy'));
    selection.removeAllRanges();

    document.body.removeChild(textarea);
}

$("#contactBtmButton").on("click", function () {
    if($("#contactBtmButton").html() == 'Cerrar') {
        $("#contactBtmButton").html("Contactar").removeClass('open');
        $("#sendContact").removeClass('open');
        $('#contactBottomDropdown').removeClass('show');
        return
    }
    $("#sendContact").addClass('open');
    $("#contactBtmButton").html("Cerrar").addClass('open');
    $('#contactBottomDropdown').addClass('show');
});

$("#sendContactForm").on("submit", function () {
    $("#contactBtmButton").html("Contactar").removeClass('open');
    $("#sendContact").removeClass('open');
    $('#contactBottomDropdown').removeClass('show');
    $("#solicitudEnviadaModal").modal('show');
});

$("#solicitarBtmButton").on("click", function () {
    if($("#solicitarBtmButton").html() == 'Cerrar') {
        $("#solicitarBtmButton").html("Solicitar información").removeClass('open');
        $("#sendTourContact").removeClass('open');
        $('#contactBottomDropdown').removeClass('show');
        return
    }
    $("#sendTourContact").addClass('open');
    $("#solicitarBtmButton").html("Cerrar").addClass('open');
    $('#contactBottomDropdown').addClass('show');
});


$("#sendTourContactForm").on("submit", function () {
    $("#solicitarBtmButton").html("Solicitar información").removeClass('open');
    $("#sendContact").removeClass('open');
    $('#contactBottomDropdown').removeClass('show');
    $("#solicitudTourEnviadaModal").modal('show');
});
