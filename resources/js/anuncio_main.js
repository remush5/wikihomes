//Mapa
$(".mapa_button").on('click', function () {
    $("#address-map-container").toggleClass("hidden");
    if($("#address-map-container").hasClass("hidden")){
        $(".mapa_button").html("Mostrar Mapa");
    } else {
        $(".mapa_button").html("Ocultar Mapa");
    }
})

//Filtros
$(".filtros_button").on('click', function () {
    $("body").toggleClass('stop-scroll');
    $(".static-background").toggleClass('visible');
    $(".filtros").toggleClass('visible');
    $('html, body').animate({
        scrollTop: $("#cabecera_botones").offset().top
    }, 0);
})

//Filtros
$(".habitaciones label").on('click', function () {
    $(".habitaciones label").removeClass('c-pink');
    $(this).addClass('c-pink');
})

$(".baños label").on('click', function () {
    $(".baños label").removeClass('c-pink');
    $(this).addClass('c-pink');
})

$(".static-background").on('click', function () {
    $(".filtros").removeClass('visible');
    $("body").removeClass('stop-scroll');
})

$(".filtros .bottom-buttons button").on('click', function () {
    $("body").removeClass('stop-scroll');
    $(".static-background").removeClass('visible');
    $(".filtros").removeClass('visible');
});

window.livewire.on('urlChange', param => {
    history.pushState({}, null, "?"+param);
});

//Ordenar Mobile
$(".ordenar_mobile").on('click', function () {
    $('#ordenarBottomDropdown').collapse('toggle');
    $(".static-background-grey").toggleClass('visible');
    if($('#ordenarBottomDropdown').height() == 0) {
        $('.ordenar_mobile').html('Aplicar');
        $("body").addClass('stop-scroll');
    } else {
        $('.ordenar_mobile').html('Ordenar');
        $("body").removeClass('stop-scroll');
    }

    if($(".bg-lpink").offset().top <= ($(window).scrollTop() + $(window).height() - 20)) {
        $('html, body').animate({
            scrollTop: $(".bg-lpink").offset().top - $(".bg-lpink").height()
        }, 0);
    }
})

$(".ordernar_normal").on('click', function () {
    $('#ordenarDropdown').collapse('toggle');
    if($('#ordenarDropdown').height() == 0) {
        $('.ordernar_normal').addClass('clicked');
    } else {
        $('.ordernar_normal').removeClass('clicked');
    }
})

$(".static-background-grey").on('click', function () {
    $("#ordenarBottomDropdown").collapse('hide');
    $('.ordenar_mobile').html('Ordenar');
    $(".static-background-grey").removeClass('visible');
    $("body").removeClass('stop-scroll');
})


$(window).on('load resize scroll', function () {
    if($(".cards-block").offset().top > ($(window).scrollTop() + $(window).height() - 20)) {
        $(".telefono_botones").addClass("d-none")
    } else {
        $(".telefono_botones").removeClass("d-none")
    }
})

$(".empezar_buscar").on('click', function () {
    $('html, body').animate({
        scrollTop: $("#cabecera_botones").offset().top
    }, 1000);
});

$(".go_contact").on('click', function () {
    $('html, body').animate({
        scrollTop: $("#contact").offset().top + 60
    }, 1000);
});
