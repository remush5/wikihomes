$(".search .buy").on("click", function () {
    $(".search .rent").removeClass("active");
    $(".search .buy").addClass("active");
});

$(".search .rent").on("click", function () {
    $(".search .buy").removeClass("active");
    $(".search .rent").addClass("active");
});

$(".search .lugar input").on("click", function () {
    selectBySeach();
});

$(".search .lugar input").on("keyup", function () {
    selectBySeach($(this).val());
});

function selectBySeach(val = '') {
    if(val != '') {
        $(".search .lugar .dropdown-item").css('display', 'none');
        $(".search .lugar .dropdown-item[data-x*=" + val + "]").css('display', 'flex');
    }
}

$(".search .lugar .dropdown-item").on("click", function () {
    $(".search .lugar input").val($(this).html());
});

$(".search .tipo .dropdown-item").on("click", function () {
    $(".search .tipo .value").html($(this).html());
});

$(".lens button").on("click", function () {
    var objetivo = $(".buttons .active").attr('objt');
    var tipo = $(".search .tipo .value").html();
    var provincia = $(".search .lugar input").val();

    //$(location).attr('href','http://www.example.com')
    $(location).attr('href','/anuncios/' + objetivo + '?tipo=' + tipo + '&provincia=' + provincia );
});
