$(".leer_mas").on("click", function () {
    $(".description").toggleClass("visible");
    if($('.description').hasClass('visible')) {
        $('.leer_mas').html('Leer menos');
    } else {
        $('.leer_mas').html('Leer más');
    }
});
if($("#anuncios_view").length > 0){
    $(window).on('load resize scroll', function () {
        if($(".description").offset().top > ($(window).scrollTop() + $(window).height())) {
            $(".mobile_info").addClass("d-none")
        } else {
            $(".mobile_info").removeClass("d-none")
        }
    })
}

