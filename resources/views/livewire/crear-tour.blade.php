@section('title', 'Profesionales que crearán tu tour virtual inmobiliario')
@section('description', 'Si no cuentas con tour virtual para tu propiedad y quieres venderla o alquilarla en un tiempo recors, en Wikihomes lo creamos por ti.')

<main id="crear_tour" class="bg-white">
    <form wire:submit.prevent="save">
        @csrf
        <section class="cabecera container">
            <div class="d-flex col-12">
                {{--<div class="col-6">
                    <a href="/anuncios" class="atras d-flex justify-content-center align-items-center">< Atras</a>
                </div>--}}

                <p class="c-ulgrey t-m text-center w-100 my-4">TOUR VIRTUAL DEMOSTRACIÓN</p>
            </div>
        </section>

        <section class="tour-container container-xl mb-5">
            <iframe id="inlineFrameExample" title="virtual tour iframe" class="w-100" height="500px" src="https://app.cloudpano.com/tours/t1LtFvj5DK">
            </iframe>
        </section>

        <section class="titulo container text-center">
            <p class=" c-brown font-weight-bold">Creamos tours virtuales, con fotografías y videos 360º</p>
        </section>

        <section class="details container ">
            <div class="d-flex flex-wrap text-center">
                <div class="col-12 col-md-4 mb-3 ">
                    <img src="{{asset('/imgs/crear_tour/destination.svg')}}" alt="Avance desde un punto a otro"
                         width="97px" height="95px">
                    <p class="c-brown t-lg font-weight-bold mb-1">Recorrido 3D</p>
                    <p class="c-brown t-b">Realizamos fotografías panorámicas HDR o 4K.</p>
                </div>

                <div class="col-12 col-md-4 mb-3">
                    <img src="{{asset('/imgs/crear_tour/galeria.svg')}}" alt="Galería de fotografías y paisajes"
                         width="119px" height="95px">
                    <p class="c-brown t-lg font-weight-bold mb-1">Fotografía 360º</p>
                    <p class="c-brown t-b">Recreación de un recorrido por las habitaciones.</p>
                </div>

                <div class="col-12 col-md-4 ">
                    <img src="{{asset('/imgs/crear_tour/etiquetas.svg')}}" alt="Etiquetas de diferentes colores"
                         width="94px" height="95px">
                    <p class="c-brown t-lg font-weight-bold mb-1">Etiquetas informativas</p>
                    <p class="c-brown t-b">Por el recorrido se incluyen etiquetas con información.</p>
                </div>
            </div>
        </section>

        <section class="insta container ">
            <div class="d-flex flex-wrap">
                <div class="col-12 col-md-6 d-flex align-items-end order-1 order-md-0">
                    <img src="{{asset('/imgs/crear_tour/camera_pc.jpg')}}" alt="Cámara de alta calidad 4K insta360"
                         class="img-fluid">
                </div>
                <div class="col-12 col-md-6 mb-4 pt-lg-4 pr-3 pr-xl-5 order-0 order-md-1 mb-5">
                    <p class="t-b c-dpink text-center text-md-left pt-lg-1">Cámara 360º</p>
                    <p class="c-brown font-weight-bold t-xl text-center text-md-left">Captura 5.7K 360 con doble lente.</p>
                    <p class="c-brown t-b text-center text-md-left  pr-xl-5">Descubre la libertad creativa del 360. Captura todo y reencuadra después a resolución súper 5.7K y codificación H.265</p>
                </div>
            </div>
        </section>
    </form>

    <section class="mobile_info nofloat container-lg mt-1">
        <form class="d-flex justify-content-between align-items-center w-100 flex-wrap" wire:submit.prevent="enviarMensaje" id="sendTourContactForm">
            <div class="d-flex flex-nowrap w-100 justify-content-center">
                <div class="d-flex flex-nowrap w-100">
                    <button type="submit" class="b-dpink ml-auto contact " id="sendTourContact">Enviar</button>
                    <button type="button" class="b-dpink contact solicitar" id="solicitarBtmButton">Solicitar información</button>
                </div>
            </div>

            <div class="collapse w-100 pt-4 px-0" id="contactBottomDropdown" wire:ignore.self>
                <div class="d-flex flex-wrap col-lg-10 px-0 pr-lg-4" >
                    @if(!$loged)
                        <div class="col-12 col-md-6 px-0 pr-md-2">
                            <input type="text" placeholder="Nombre y apellidos" class="mb-2 t-m form-control py-2 px-3 pr-5 block" wire:model.defer="nombre" required/>
                        </div>
                        <div class="col-12 col-md-6 px-0 pl-md-2">
                            <input type="email" placeholder="Email" class="mb-2 t-m form-control py-2 px-3 pr-5 block"  wire:model.defer="email" required/>
                        </div>
                    @endif
                    <div class="col-12 px-0">
                        <textarea name="mensaje" id="" cols="30" rows="10" class="w-100 mb-2 t-m form-control py-2 px-3 pr-5 block" placeholder="Mensaje al anunciante"  wire:model.defer="mensaje" required>
                        </textarea>
                    </div>
                    <div class="aditional-info">
                        Responsable: WikiHomes. Finalidad: Responder a las solicitudes de los usuarios. Derechos: Acceso, rectificación, supresión y demás derechos cómo se explica en la
                        <a href="/politicas_privacidad" class="c-pink">información adicional.</a>
                    </div>
                    <div class="col-12 px-0 d-flex d-lg-none">
                        <a href="tel:+34620785243" class="button {{$mostrar_telefono ? 'b-tlf c-dpink' : 'p_s_button'}} mr-1 w-50 d-flex align-items-center justify-content-center cursor-pointer" wire:click="setTelefono">
                            @if($mostrar_telefono)
                                +34 620 78 52 43
                            @else
                                Llamar
                            @endif
                        </a>
                        <button type="submit" class="button p_s_button w-50">
                            Enviar
                        </button>
                    </div>
                </div>
                <div class="col-2 px-0 d-none d-lg-flex flex-column user-info">
                    <a href="tel:+34620785243" class="c-dpink mb-2 t-b d-block">
                        +34 620 78 52 43
                    </a>

                    <div class="direccion">
                        Calle Faro, número 1 Almerimar (el ejido), 04711 - Almería
                    </div>
                </div>
            </div>
        </form>
    </section>
</main>
