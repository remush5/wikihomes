<main id="como-empezar">
    <section class="header">
        <div class="d-flex flex-column">
            <h1>¿Cómo Funciona? </h1>

            <picture>
                <source media="(min-width: 768px)" srcset="/imgs/como_empezar/COMOFUNCIONA.png">
                <img src="/imgs/como_empezar/COMOFUNCIONAMOBILE.png" alt="sofa" class="img-fluid w-100">
            </picture>
        </div>
    </section>

    <section class="secs container d-flex flex-wrap">
        <div class="col-12 col-md-6 mb-3 d-flex flex-column align-items-start flex-xl-row justify-content-xl-between sec">
            <img src="{{asset('/imgs/como_empezar/eco-home.svg')}}" alt="Avance desde un punto a otro">
            <div>
                <p class="c-pink  t-vb font-weight-bold mb-2">Comisión unica de venta 4%</p>
                <p class="c-brown ">Hablemos de lo principal. El factor económico es algo importante que debemos contemplar
                    a la hora de vender nuestra casa.  <span class="c-dbrown">Sólo un 4% de comisión postventa.</span> <br> <br> <span class="c-dpink">¿Porqué? te lo
                    justificamos a continuación.</span></p>
            </div>
        </div>

        <div class="col-12 col-md-6 mb-3 d-flex flex-column align-items-start flex-xl-row justify-content-xl-between ml-xl-auto sec">
            <img src="{{asset('/imgs/como_empezar/add-friend.svg')}}" alt="Chica sonriente">
            <div>
                <p class="c-pink  t-vb font-weight-bold mb-2">Trabajamos en exclusiva para vender tu propiedad</p>
                <p class="c-brown">La tranquilidad de saber que estás en buenas manos, de que todo va a salir bien.
                    <span class="c-dbrown">Venderemos tu casa, rápidamente, con garantías de calidad y profesionalidad.</span></p>
            </div>

        </div>

        <div class="col-12 col-md-6 mb-3 d-flex flex-column align-items-start flex-xl-row justify-content-xl-between sec" id="sofa-sec">
            <img src="{{asset('/imgs/como_empezar/furniture.svg')}}" alt="sofá de casa">
            <div>
                <p class="c-pink  t-vb font-weight-bold mb-2">Home staging / Decoramos tu casa</p>
                <p class="c-brown"><span class="c-dbrown">Preparamos y acondicionamos la propiedad para que el comprador quiera comprarla,</span> Esta
                    fase tan importante la realiza el estudio de interiorismo PL ARCHITECTURE. Ellos saben lo que se
                    hacen.</p>
            </div>
        </div>

        <div class="col-12 col-md-6 mb-3 d-flex flex-column align-items-start flex-xl-row justify-content-xl-between ml-xl-auto sec">
            <img src="{{asset('/imgs/como_empezar/360-degrees.svg')}}" alt="vista de 360 grados">
            <div>
                <p class="c-pink  t-vb font-weight-bold mb-2">Creación del tour virtual</p>
                <p class="c-brown"><span class="c-dbrown">Un profesional realizará fotografías 360º y videos de su casa</span> cuidando todos los
                    detalles. Después creará con ello un estupendo <span class="c-dpink">tour virtual</span> que recorrerá toda la casa, reduciendo así
                    visitas innecesarias.</p>
            </div>
        </div>

        <div class="col-12 col-md-6 mb-3 d-flex flex-column align-items-start flex-xl-row justify-content-xl-between sec">
            <img src="{{asset('/imgs/como_empezar/etiquetas.svg')}}" alt="serie de etiquetas">
            <div>
                <p class="c-pink  t-vb font-weight-bold mb-2">Creamos el anuncio en WikiHomes y también lo compartimos en
                    los portales TOP</p>
                <p class="c-brown">Nos encargamos de todo el proceso de comunicación, redactamos el anuncio, características
                    de la propiedad, m2, información del entorno, etc. Una vez terminado, <span class="c-dbrown">lo publicamos en toda las
                        plataformas online.</span></p>
            </div>
        </div>

        <div class="col-12 col-md-6 mb-3 d-flex flex-column align-items-start flex-xl-row justify-content-xl-between ml-xl-auto sec">
            <img src="{{asset('/imgs/como_empezar/vista-360.svg')}}" alt="visión de 360º">
            <div>
                <p class="c-pink  t-vb font-weight-bold mb-2">Jornadas de puertas abiertas</p>
                <p class="c-brown">Con la casa perfectamente limpia y decorada, ofrecemos jornadas donde <span class="c-dbrown">los clientes podrán
                        ir físicamente a ver su casa y hacer ofertas.</span></p>
            </div>
        </div>

        <div class="col-12 col-md-6 mb-3 d-flex flex-column align-items-start flex-xl-row justify-content-xl-between sec">
            <img src="{{asset('/imgs/como_empezar/videollamada.svg')}}" alt="videollamada">
            <div>
                <p class="c-pink  t-vb font-weight-bold mb-2">Videollamada (agente/cliente) viendo el tour virtual</p>
                <p class="c-brown">Incluimos el servicio de recorrido virtual a través de videollamada, donde <span class="c-dbrown">el
                            potencial comprador recorre junto al agente su propiedad a través del tour virtual en</span> <span class="c-dpink">WikiHomes.</span></p>
            </div>
        </div>

        <div class="col-12 col-md-6 mb-3 d-flex flex-column align-items-start flex-xl-row justify-content-xl-between ml-xl-auto sec" id="last-sec">
            <img src="{{asset('/imgs/como_empezar/house-key.svg')}}" alt="llaves de casa">
            <div>
                <p class="c-pink  t-vb font-weight-bold mb-2">Nos encargamos de toda la gestión para su venta</p>
                <p class="c-brown"><span class="c-dbrown">Olvídese de papeleos e historias. Nos encargamos de todo</span>, investigación de
                    mercados, información de la propiedad, asesoramiento en el precio, decoración, anuncio, gestión
                    de ofertas, cierre del contrato, entrega de llaves. <span class="c-dpink">TODO.</span></p>
            </div>
        </div>

        <div id="logotipo" class="col-12  mb-3">
            <p>
                <span>Wiki</span>Homes.es
            </p>
            <p>
                Inmobiliaria virtual & home staging
            </p>
        </div>
    </section>

    <section class="mobile_info nofloat container-lg mt-4">
        <form class="d-flex justify-content-between align-items-center w-100 flex-wrap" wire:submit.prevent="enviarMensaje" id="sendContactForm">
            <div class="d-flex flex-nowrap w-100 justify-content-between">
                <div class="d-flex flex-nowrap align-items-center">
                    <img class="img-fluid" src="/imgs/user/pedro_1.png" alt="header img "/>
                    <div class="ml-2 d-flex flex-column justify-content-center">
                        <div class="font-weight-bold">Pedro Lirola</div>
                        <div class="c-pink">Agente Exclusivo</div>
                    </div>
                </div>
                <div class="d-flex flex-nowrap">
                    <button type="submit" class="b-dpink mr-2 contact" id="sendContact">Enviar</button>
                    <button type="button" class="b-dpink contact" id="contactBtmButton">Contactar</button>
                </div>
            </div>

            <div class="collapse w-100 pt-4 px-0" id="contactBottomDropdown" wire:ignore.self>
                <div class="d-flex flex-wrap col-lg-10 px-0 pr-lg-4" >
                    @if(!$loged)
                        <div class="col-12 col-md-6 px-0 pr-md-2">
                            <input type="text" placeholder="Nombre y apellidos" class="mb-2 t-m form-control py-2 px-3 pr-5 block" wire:model.defer="nombre" required/>
                        </div>
                        <div class="col-12 col-md-6 px-0 pl-md-2">
                            <input type="email" placeholder="Email" class="mb-2 t-m form-control py-2 px-3 pr-5 block"  wire:model.defer="email" required/>
                        </div>
                    @endif
                    <div class="col-12 px-0">
                        <textarea name="mensaje" id="" cols="30" rows="10" class="w-100 mb-2 t-m form-control py-2 px-3 pr-5 block" placeholder="Mensaje al anunciante"  wire:model.defer="mensaje" required>
                        </textarea>
                    </div>
                    <div class="aditional-info">
                        Responsable: WikiHomes. Finalidad: Responder a las solicitudes de los usuarios. Derechos: Acceso, rectificación, supresión y demás derechos cómo se explica en la
                        <a href="/politicas_privacidad" class="c-pink">información adicional.</a>
                    </div>
                    <div class="col-12 px-0 d-flex d-lg-none">
                        <a href="tel:+34620785243" class="button {{$mostrar_telefono ? 'b-tlf c-dpink' : 'p_s_button'}} mr-1 w-50 d-flex align-items-center justify-content-center cursor-pointer" wire:click="setTelefono">
                            @if($mostrar_telefono)
                                +34 620 78 52 43
                            @else
                                Llamar
                            @endif
                        </a>
                        <button type="submit" class="button p_s_button w-50">
                            Enviar
                        </button>
                    </div>
                </div>
                <div class="col-2 px-0 d-none d-lg-flex flex-column user-info">
                    <a href="tel:+34620785243" class="c-dpink mb-2 t-b d-block">
                        +34 620 78 52 43
                    </a>

                    <div class="direccion">
                        Calle Faro, número 1 Almerimar (el ejido), 04711 - Almería
                    </div>
                </div>
            </div>
        </form>
    </section>
</main>
