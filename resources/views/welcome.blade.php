@section('title', 'Wikihomes, plataforma inmobilaria de tours virtuales')
@section('description', 'Encuentra de manera sencilla la casa de tus sueños. Ya quieras alquilar o comprar una vivienda, los tours virtuales te ayudarán en la decisión correcta.')

<main role="main">
    <section class="top-block">
        <div class="container">
            <div class="pt-4 pt-md-5 d-flex flex-wrap pb-xl-5">
                <div class="title col-12 col-md-7 col-lg-6 col-xl-7 justify-content-center pt-md-0 pb-2 pb-md-0">
                    <h1>Siente tu casa antes de comprarla.</h1>
                    <p class="mb-md-0 mb-xl-4">La mejor plataforma inmobiliaria de tours virtuales.</p>
                    <div class="search d-none d-xl-block">
                        <div class="d-flex flex-wrap mx-md-auto">
                            <div class="col-5 buttons d-flex flex-nowrap">
                                <button class="buy active" objt="vender">Comprar</button>
                                <button class="rent" objt="alquilar">Alquilar</button>
                            </div>
                            <div class="col-7 d-flex flex-nowrap justify-content-between">
                                <div class="tipo w-50">
                                    <div class="dropdown-toggle pl-2" id="selectTipoButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <p class="m-0">Tipo de inmueble</p>
                                        <p class="m-0 value overflow-hidden mt-1">Qué buscas</p>
                                    </div>

                                    <div class="dropdown-menu px-2" aria-labelledby="selectTipoButton">
                                        <div class="options mt-2">
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Piso de obra nueva</button>
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Casa o chalet de obra nueva</button>
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Piso</button>
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Casa o chalet</button>
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Casas rústicas</button>
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Dúplex</button>
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Ático</button>
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Oficina</button>
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Local o nave</button>
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Terreno</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="lugar w-50 ">
                                    <div class="dropdown-toggle pl-2" id="selectLugarButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <p class="m-0">Ubicación</p>
                                        <input class="m-0 overflow-hidden mt-1" placeholder="Dónde buscas"/>
                                    </div>

                                    <div wire:ignore class="dropdown-menu px-2" aria-labelledby="selectLugarButton">
                                        <div class="options mt-2">
                                            @foreach($provincias as $provincia)
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1" data-x="{{strtolower (strtr(utf8_decode($provincia->nombre), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY'))}}">{{$provincia->nombre}}</button>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="lens">
                                    <button>
                                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-search" fill="#fff" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"></path>
                                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"></path>
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="top-img col-12 col-md-5 col-lg-6 col-xl-5 d-md-flex align-items-center">
                    <img class="img-fluid" src="{{ asset('imgs/home/top.png') }}" alt="header img "/>
                </div>

                <div class="search col-12 py-4 d-xl-none">
                    <div class="d-flex flex-wrap mx-md-auto">
                        <div class="buttons col-12 col-md-5 mb-4 d-flex flex-nowrap mb-md-0">
                            <button class="buy active" objt="vender">Comprar</button>
                            <button class="rent" objt="alquilar">Alquilar</button>
                        </div>
                        <div class="col-12 col-md-7 d-flex flex-nowrap justify-content-between">
                            <div class="tipo w-50">
                                <div class="dropdown-toggle pl-2" id="selectTipoButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <p class="m-0">Tipo de inmueble</p>
                                    <p class="m-0 overflow-hidden mt-1 value">Qué buscas</p>
                                </div>

                                <div class="dropdown-menu px-2" aria-labelledby="selectTipoButton">
                                    <div class="options mt-2">
                                        <button class="dropdown-item ml-2 px-1 my-2 py-1">Piso de obra nueva</button>
                                        <button class="dropdown-item ml-2 px-1 my-2 py-1">Casa o chalet de obra nueva</button>
                                        <button class="dropdown-item ml-2 px-1 my-2 py-1">Piso</button>
                                        <button class="dropdown-item ml-2 px-1 my-2 py-1">Casa o chalet</button>
                                        <button class="dropdown-item ml-2 px-1 my-2 py-1">Casas rústicas</button>
                                        <button class="dropdown-item ml-2 px-1 my-2 py-1">Dúplex</button>
                                        <button class="dropdown-item ml-2 px-1 my-2 py-1">Ático</button>
                                        <button class="dropdown-item ml-2 px-1 my-2 py-1">Oficina</button>
                                        <button class="dropdown-item ml-2 px-1 my-2 py-1">Local o nave</button>
                                        <button class="dropdown-item ml-2 px-1 my-2 py-1">Terreno</button>
                                    </div>
                                </div>
                            </div>

                            <div class="lugar w-50 ">
                                <div class="dropdown-toggle pl-2" id="selectLugarButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <p class="m-0">Ubicación</p>
                                    <input class="m-0 overflow-hidden mt-1" placeholder="Dónde buscas"/>
                                </div>

                                <div wire:ignore class="dropdown-menu px-2" aria-labelledby="selectLugarButton">
                                    <div class="options mt-2">
                                        @foreach($provincias as $provincia)
                                            <button class="dropdown-item ml-2 px-1 my-2 py-1 overflow-hidden">{{$provincia->nombre}}</button>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="lens">
                                <button class="px-0">
                                    <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-search" fill="#fff"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd"
                                              d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                                        <path fill-rule="evenodd"
                                              d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="selling mt-5">
        <div class="title container">
            <div class="col-12 d-flex justify-content-between flex-nowrap">
                <span>A LA VENTA</span>
                <a href="/anuncios/vender">VER +</a>
            </div>
            <p class="col-12 mt-2">
                Selecciona las casas que te gustan, mira su tour virtual y asegúrate que quieres visitarlas.
            </p>
        </div>

        <div class="cards-block container mt-4 ">
            <div class="row px-3">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/1.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado ayer</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute" >
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">289.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                6bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                3ba, 224 sqft
                            </p>
                            <p class="description">Urb Golfo, Piso 5</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card px-0 position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/2.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado 20-Oct-2020</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">113.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                3bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                1ba, 67 m²
                            </p>
                            <p class="description">Calle Aranjuez, Bloque 2, 2ºC</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card px-0 position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/6.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado ayer</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">165.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                4bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                2ba, 89 sqft
                            </p>
                            <p class="description">Avenida Martinez, Bloque 5, 3ºE</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4 d-none d-md-block">
                    <div class="card px-0 position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/3.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado 23-Oct-2020</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">198.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                5bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                3ba, 134 m²
                            </p>
                            <p class="description">Calle La Paz, Bloque 1, 9ºA</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4 d-none d-lg-block">
                    <div class="card px-0 position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/4.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado 21-Oct-2020</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">134.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                3bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                3ba, 61 m²
                            </p>
                            <p class="description">Avenida Jureles, Bloque 5, 3ºB</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4 d-none d-lg-block">
                    <div class="card px-0 position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/5.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado 3-Dec-2020</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">166.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                3bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                2ba, 89 m²
                            </p>
                            <p class="description">Avenida Alejandro, Bloque 2, 1ºE</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="selling mt-5">
        <div class="title container">
            <div class="col-12 d-flex justify-content-between flex-nowrap">
                <span>EN ALQUILER</span>
                <a href="/anuncios/alquilar">VER +</a>
            </div>
            <p class="col-12 mt-2">
                Haz una búsqueda de las viviendas que te gustan, mira su tour virtual y asegura la visita perfecta.
            </p>
        </div>

        <div class="cards-block container mt-4 ">
            <div class="row px-3">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/7.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado 4-Dec-2020</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">210.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                5bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                2ba, 110 sqft
                            </p>
                            <p class="description">Calle Albarez, Bloque 6</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card px-0 position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/8.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado 3-Dec-2020</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">215.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                4bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                3ba, 334 sqft
                            </p>
                            <p class="description">Calle Federico, Bloque 5, 7ºD</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card px-0 position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/9.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado ayer</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">127.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                3bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                3ba, 224 sqft
                            </p>
                            <p class="description">Avenida Mariangeles, Bloque 5, 6ºC</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4 d-none d-md-block">
                    <div class="card px-0 position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/10.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado 2-Dec-2020</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">321.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                5bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                4ba, 340 m²
                            </p>
                            <p class="description">Urb Golfo, piso 5</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4 d-none d-lg-block">
                    <div class="card px-0 position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/1.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado 1-Dec-2020</span>
                        <span class="like position-absolute">
                            <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                                <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                            </svg>
                        </span>
                        <span class="share position-absolute">
                            <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                                <g class="a">
                                    <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                                    <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                                </g>
                            </svg>
                        </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">120.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                4bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                3ba, 132 m²
                            </p>
                            <p class="description">Calle La Paz, Bloque 6, 12ºA</p>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6 col-lg-4 d-none d-lg-block">
                    <div class="card px-0 position-relative">
                        <div class="card-img-top">
                            <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/test/2.jpg') }})"></span>
                        </div>
                        <span class="public-date position-absolute py-0 px-2">Publicado ayer</span>
                        <span class="like position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.063" height="20.519" viewBox="0 0 23.063 20.519">
                        <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                    </svg>
                </span>
                        <span class="share position-absolute">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </span>
                        <div class="card-body py-2">
                            <p class="price mb-2">210.000€</p>
                            <p class="details mb-2">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                     viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                2bd,
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                        </g>
                                    </g>
                                    <g>
                                        <g>
                                            <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                        </g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                    <g>
                                    </g>
                                </svg>
                                2ba, 124 m²
                            </p>
                            <p class="description">Avenida Málaga, Bloque 4, 6ºE</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="tips mt-5 pb-3">
        <div class="title container">
            <h5 class="col-12 text-center">¿ERES UN PROFESIONAL INMOBILIARIO?</h5>
            <a href="/dashboard/anuncio/crear" class="col-12 mt-3 d-flex mx-auto justify-content-center">Publica tus tours virtuales</a>
        </div>

        <div class="tips-block container mt-5 ">
            <div class="d-flex flex-wrap">
                <div class="tip col-12 col-md-4 mb-4 text-center">
                    <img src="/icons/tips/tip_1.svg" alt="tip 1 image" >
                    <h5 class="px-2 mb-2">Crea tu cuenta</h5>
                    <p class="px-2">Crea una cuenta como profesional inmobiliario y sube tu información.</p>
                </div>

                <div class="tip col-12 col-md-4 mb-4 text-center">
                    <img src="/icons/tips/tip_2.svg" alt="tip 2 image" >
                    <h5 class="px-2 mb-2">Sube tus tours </h5>
                    <p class="px-2">Publica tus propiedades con toda la información e incluye los tours.</p>
                </div>

                <div class="tip col-12 col-md-4 mb-4 text-center">
                    <img src="/icons/tips/tip_3.svg" alt="tip 3 image" >
                    <h5 class="px-2 mb-2">Logra más visitas</h5>
                    <p class="px-2">Ya lo has conseguido, te has posicionado mejor que la competencia.</p>
                </div>
            </div>
        </div>

        <a class="button d-block text-center mt-4 mx-auto b-dpink" href="/como_empezar" title="como empezar">¿Cómo empezar?</a>
    </section>

    <section class="banner container mt-5">
        <div class="col-12 col-md-6">
            <div>
                <img src="/icons/banner/banner1.svg" alt="banner 1 image" class="img-fluid">
                <a class="b-dpink t-m" href="/crear_tour" title="Creamos tu tour virtual">Enviar fotografías para montar un tour tu tour virtual</a>
            </div>
        </div>

        <div class="col-12 col-md-6">
            <div>
                <img src="/icons/banner/banner2.svg" alt="banner 2 image" class="img-fluid">
                <a class="b-dpink t-m" href="/crear_tour" title="Creamos tu tour virtual">Crear tour completo</a>
            </div>
        </div>
    </section>
</main>
