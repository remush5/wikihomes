{{--<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <div class="mb-4 text-sm text-gray-600">
            {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
        </div>

        @if (session('status') == 'verification-link-sent')
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ __('A new verification link has been sent to the email address you provided during registration.') }}
            </div>
        @endif

        <div class="mt-4 flex items-center justify-between">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf

                <div>
                    <x-jet-button type="submit">
                        {{ __('Resend Verification Email') }}
                    </x-jet-button>
                </div>
            </form>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <button type="submit" class="underline text-sm text-gray-600 hover:text-gray-900">
                    {{ __('Logout') }}
                </button>
            </form>
        </div>
    </x-jet-authentication-card>
</x-guest-layout>--}}

<x-main-layout>
    <div class="login-panel">
        <div class="wrapper">
            <div class="login-form">
                <form class="recuperar-contraseña d-flex flex-column" method="POST" action="{{ route('verification.send') }}">
                    @csrf

                    <div class="type d-flex flex-nowrap justify-content-between mb-4">
                        <a href="javascript:history.back()"><img src="/icons/arrow-left.svg"></a>
                    </div>

                    <h3 class="title h3 mb-2">
                        Enlace de restauración enviado a su email
                    </h3>

                    <p class="mb-4 c-pink">
                        Verifique su correo electrónico
                    </p>

                    <img class="verify-img mb-4 mx-auto" src="{{ asset('icons/email_link_enviado.svg') }}" alt="header img "/>

                    <button type="submit" class="mb-3 w-100 p_s_button">
                        Reenviar email
                    </button>

                    @if (session('status') == 'verification-link-sent')
                        <p class="mb-3 c-pink t-m">
                            {{ __('Se ha enviado un nuevo email al correo proporcionado en el registro.') }}
                        </p>
                    @endif
                </form>
            </div>
        </div>
    </div>
</x-main-layout>
