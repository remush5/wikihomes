{{--}}<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <div class="mb-4 text-sm text-gray-600">
            {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
        </div>

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="block">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="flex items-center justify-end mt-4">
                <x-jet-button>
                    {{ __('Email Password Reset Link') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>--}}
<x-main-layout>
    <div class="login-panel">
        <div class="wrapper">
            <div class="login-form">
                <form class="recuperar-contraseña" method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="type d-flex flex-nowrap justify-content-between mb-4">
                        <a href="javascript:history.back()"><img src="/icons/arrow-left.svg"></a>
                    </div>

                    <h3 class="title h3 mb-4">
                        Recuperar contraseña
                    </h3>

                    <input type="email" placeholder="Email" name="email" value="{{ old('email') }}" required class="mb-3">

                    @if($errors->any())
                        <p class="t-sm" style="color: #ff0000">{{$errors->first()}}</p>
                    @endif

                    <input type="submit" value="Enviar" class="mb-3" >

                </form>
            </div>
        </div>
    </div>
</x-main-layout>
