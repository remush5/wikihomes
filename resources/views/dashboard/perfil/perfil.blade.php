<main id="perfil">
    @if(!$first)
    <section class="main-information mb-5">
        <form wire:submit.prevent="update">
            @csrf

            <h2 class="c-12 text-center title mb-4">Mi perfil / <span class="c-pink">Información</span></h2>

            <div class="container">
                <div class="d-flex flex-wrap">
                    <div class="col-12 col-md-3 mb-2 mb-md-0 user-photo" x-data="{photoName: null, photoPreview: null}">
                        <!-- Profile Photo File Input -->
                        <input type="file" class="invisible position-absolute"
                               x-ref="photo"
                               wire:model.defer="photo"
                               id="photo"
                               />

                        <!-- Current Profile Photo -->
                        <div class="currentPhoto position-relative mx-auto mr-md-2">
                            <span class="block rounded-full w-100 h-100" style="background-image: url('{{ $user->profile_photo_path == null ? $photo == null ? '/imgs/logo/profile.svg' : $photo : '/storage/profile-photos/'.$user->profile_photo_path.'_140.jpg' }}'); background-position: center; background-size: cover;" x-on:click.prevent="$refs.photo.click()">
                            </span>
                            <label for="photo" class="position-absolute mb-0 d-flex justify-content-center align-items-center cursor-pointer" x-on:click.prevent="$refs.photo.click()">
                                <svg width="24px" height="24px" viewBox="0 0 16 16" class="bi bi-camera-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10.5 8.5a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                                    <path fill-rule="evenodd" d="M2 4a2 2 0 0 0-2 2v6a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V6a2 2 0 0 0-2-2h-1.172a2 2 0 0 1-1.414-.586l-.828-.828A2 2 0 0 0 9.172 2H6.828a2 2 0 0 0-1.414.586l-.828.828A2 2 0 0 1 3.172 4H2zm.5 2a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm9 2.5a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0z"/>
                                </svg>
                            </label>
                        </div>

                    </div>

                    <div class="col-12 col-md-9 d-flex flex-wrap px-0">
                        <div class="col-12 col-lg-5 d-flex flex-wrap px-0">
                            <div class="col-12 mb-2">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="nombre" class="t-sm">Nombre y apellidos</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="nombre" value="{{ __('Nombre') }}" class="w-100"/>
                                        <input id="nombre" type="text" class="form-control py-2 px-3 block w-100 @error('nombre') is-invalid @enderror t-m" wire:model.defer="nombre" placeholder="Nombre y apellidos" required/>
                                    </div>

                                    @error('nombre') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>

                            <div class="col-12 mb-2">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="email" class="t-sm">Dirección de email</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="email" value="{{ __('Email') }}" class="w-100"/>
                                        <input id="email" type="email" class="form-control py-2 px-3 block w-100 @error('email') is-invalid @enderror t-m" wire:model.defer="email" placeholder="Dirección de email" required/>
                                    </div>

                                    @error('email') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>

                            <div class="col-12 mb-2 mb-lg-0">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="fecha_de_nacimiento" class="t-sm">Fecha de nacimiento</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="fecha_de_nacimiento" value="{{ __('Fecha de Nacimiento') }}"  class="w-100"/>
                                        <input id="fecha_de_nacimiento" type="date" class="form-control py-2 px-3 block w-100 @error('fecha_de_nacimiento') is-invalid @enderror t-m" wire:model="fecha_de_nacimiento" placeholder="DD/MM/AAAA" max="{{date('d/m/Y')}}"/>
                                    </div>

                                    @error('fecha_de_nacimiento') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>

                        </div>

                        <div class="col-12 col-lg-7">
                            <div class="row">
                                <div class="col-12 pl-4">
                                    <label for="descripcion" class="t-sm">Descripción</label>
                                </div>

                                <div class="col-12">
                                    <label for="descripcion" value="{{ __('Descripción') }}" class="w-100 mb-0"/>
                                    <textarea id="descripcion" class="form-control py-2 px-3 block w-100 @error('descripcion') is-invalid @enderror t-m" placeholder="Descripción opcional" maxlength="150" wire:model="descripcion"></textarea>
                                    <label for="description" class="pl-3 mb-0 t-m">
                                        <span class="t-m">{{$descripcion == '' ? 0 : strlen($descripcion)}}</span>/150 caracteres
                                    </label>
                                </div>

                                @error('descripcion') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                            </div>
                        </div>
                    </div>

                    <div class="col-12 d-flex justify-content-center mt-4">
                        <button type="button" wire:click="cancelMain" wire:loading.attr="disabled" class="d-flex justify-content-center align-items-center mr-3 cancel">Cancelar</button>
                        <button type="submit" wire:loading.attr="disabled" class="d-flex justify-content-center align-items-center save">Guardar</button>
                    </div>
                </div>
            </div>
        </form>
    </section>

    <section id="address" class="adress mb-5">
        <form wire:submit.prevent="updateAddress">
            @csrf

            <h2 class="c-12 text-center title mb-4">Mi perfil / <span class="c-pink">Dirección</span></h2>

            <div class="container">
                <div class="d-flex flex-wrap">
                    <div class="col-12 col-md-9 px-0 ml-auto">
                        <div class="d-flex flex-wrap">
                            <div class="col-12 col-lg-8 mb-2">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="name" class="t-sm">Dirección</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="direccion" value="{{ __('Dirección') }}" class="w-100" />
                                        <input id="direccion" type="text" class="py-2 px-3 block w-100 @error('direccion') is-invalid @enderror t-m" wire:model.defer="direccion" placeholder="Dirección" required/>
                                    </div>

                                    @error('direccion') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>

                            <div class="col-6 col-lg-4 mb-2">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="name" class="t-sm">Provincia</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="provincia" value="{{ __('Provincia') }}" class="w-100" />
                                        <select id="provincia" type="text" class="form-control py-2 px-3 block w-100 @error('provincia') is-invalid @enderror t-m" wire:model.defer="provincia" wire:change="getCiudades" required>
                                            <option value=""></option>
                                            @foreach($provincias as $provincia)
                                                <option value="{{$provincia->nombre}}">{{$provincia->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    @error('provincia') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>

                            <div class="col-6 col-lg-4 mb-2">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="name" class="t-sm">Ciudad</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="ciudad" value="{{ __('Ciudad') }}" class="w-100" />
                                        <select id="ciudad" type="text" class="form-control py-2 px-3 block w-100 @error('ciudad') is-invalid @enderror t-m" wire:model.defer="ciudad" required>
                                            <option value=""></option>
                                            @foreach($ciudades as $ciudad)
                                                <option value="{{$ciudad->nombre}}">{{$ciudad->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    @error('ciudad') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>

                            <div class="col-6 col-lg-4 mb-2">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="name" class="t-sm">Código postal</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="codigo_postal" value="{{ __('Código postal') }}" class="w-100" />
                                        <input id="codigo_postal" type="text" class="py-2 px-3 block w-100 @error('codigo_postal') is-invalid @enderror t-m" wire:model.defer="codigo_postal" placeholder="Código postal" required/>
                                    </div>

                                    @error('codigo_postal') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>

                            <div class="col-6 col-lg-4 mb-4">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="name" class="t-sm">Teléfono (opcional)</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="telefono" value="{{ __('Teléfono') }}" class="w-100" />
                                        <input id="telefono" type="text" class="py-2 px-3 block w-100 @error('telefono') is-invalid @enderror t-m" wire:model.defer="telefono" placeholder="Teléfono" required/>
                                    </div>

                                    @error('telefono') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 d-flex justify-content-center">
                        <button type="button" wire:click="cancelAddress" wire:loading.attr="disabled" class="d-flex justify-content-center align-items-center mr-3 cancel">Cancelar</button>
                        <button type="submit" wire:loading.attr="disabled" class="d-flex justify-content-center align-items-center save">Guardar</button>
                    </div>
                </div>
            </div>
        </form>
    </section>

    <section id="password" class="pasword pb-5">
        <form wire:submit.prevent="updatePassword">
            @csrf
            <h2 class="c-12 text-center title mb-4">Mi perfil / <span class="c-pink">Contraseña</span></h2>

            <div class="container">
                <div class="d-flex flex-wrap">
                    <div class="col-12 col-md-9 px-0 ml-auto">
                        <div class="d-flex flex-wrap">
                            @if(!$not_password)
                            <div class="col-12 col-lg-4 mb-2">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="name" class="t-sm">Contraseña actual</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="contrasena" value="{{ __('Contraseña actual') }}" class="w-100" />
                                        <input id="contrasena" type="password" class="py-2 px-3 block w-100 @error('contrasena') is-invalid @enderror t-m" wire:model.defer="contrasena" placeholder="************" required/>
                                        <span>
                                            <svg class="visible" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 512 512">
                                              <g>
                                                <path d="m496.4,243.1c-63.9-78.7-149.3-122.1-240.4-122.1-91.1,0-176.5,43.4-240.4,122.1-6.1,7.5-6.1,18.2 0,25.7 63.9,78.8 149.3,122.2 240.4,122.2 91.1,0 176.5-43.4 240.4-122.1 6.1-7.5 6.1-18.3 0-25.8zm-240.4,79.8c-36.9,0-66.9-30-66.9-66.9 0-36.9 30-66.9 66.9-66.9 36.9,0 66.9,30 66.9,66.9 0,36.9-30,66.9-66.9,66.9zm-197.8-66.9c37.8-42.2 82.9-71.1 131.5-84.9-25.2,19.7-41.5,50.4-41.5,84.9 0,34.4 16.2,65.1 41.5,84.9-48.6-13.8-93.6-42.7-131.5-84.9zm264.1,84.9c25.2-19.7 41.5-50.4 41.5-84.9 0-34.4-16.2-65.1-41.5-84.9 48.6,13.8 93.7,42.7 131.5,84.9-37.9,42.2-82.9,71.1-131.5,84.9z"/>
                                              </g>
                                            </svg>
                                            <svg class="private active" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 477.871 477.871" style="enable-background:new 0 0 477.871 477.871;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M474.609,228.901c-29.006-38.002-63.843-71.175-103.219-98.287l67.345-67.345c6.78-6.548,6.968-17.352,0.42-24.132
                                                            c-6.548-6.78-17.352-6.968-24.132-0.42c-0.142,0.137-0.282,0.277-0.42,0.42l-73.574,73.506
                                                            c-31.317-17.236-66.353-26.607-102.093-27.307C109.229,85.336,7.529,223.03,3.262,228.9c-4.349,5.983-4.349,14.087,0,20.07
                                                            c29.006,38.002,63.843,71.175,103.219,98.287l-67.345,67.345c-6.78,6.548-6.968,17.352-0.42,24.132
                                                            c6.548,6.78,17.352,6.968,24.132,0.42c0.142-0.137,0.282-0.277,0.42-0.42l73.574-73.506
                                                            c31.317,17.236,66.353,26.607,102.093,27.307c129.707,0,231.407-137.694,235.674-143.565
                                                            C478.959,242.988,478.959,234.884,474.609,228.901z M131.296,322.494c-34.767-23.156-65.931-51.311-92.484-83.558
                                                            c25.122-30.43,106.598-119.467,200.124-119.467c26.609,0.538,52.77,6.949,76.612,18.773L285.92,167.87
                                                            c-39.2-26.025-92.076-15.345-118.101,23.855c-18.958,28.555-18.958,65.691,0,94.246L131.296,322.494z M285.016,217.005
                                                            c3.34,6.83,5.091,14.328,5.12,21.931c0,28.277-22.923,51.2-51.2,51.2c-7.603-0.029-15.101-1.78-21.931-5.12L285.016,217.005z
                                                             M192.856,260.866c-3.34-6.83-5.091-14.328-5.12-21.931c0-28.277,22.923-51.2,51.2-51.2c7.603,0.029,15.101,1.78,21.931,5.12
                                                            L192.856,260.866z M238.936,358.402c-26.609-0.538-52.769-6.949-76.612-18.773l29.628-29.628
                                                            c39.2,26.025,92.076,15.345,118.101-23.855c18.958-28.555,18.958-65.691,0-94.246l36.523-36.523
                                                            c34.767,23.156,65.931,51.312,92.484,83.558C413.937,269.366,332.461,358.402,238.936,358.402z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                </svg>
                                        </span>
                                    </div>

                                    @error('contrasena') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            @endif

                            <div class="col-12 col-lg-4 mb-2">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="name" class="t-sm">Nueva contraseña</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="nueva_contrasena" value="{{ __('Nueva contraseña') }}" class="w-100"/>
                                        <input id="nueva_contrasena" type="password" class="py-2 px-3 block w-100 @error('nueva_contrasena') is-invalid @enderror t-m" wire:model.defer="nueva_contrasena" placeholder="Nueva contraseña" required/>
                                        <span>
                                            <svg class="visible" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 512 512">
                                              <g>
                                                <path d="m496.4,243.1c-63.9-78.7-149.3-122.1-240.4-122.1-91.1,0-176.5,43.4-240.4,122.1-6.1,7.5-6.1,18.2 0,25.7 63.9,78.8 149.3,122.2 240.4,122.2 91.1,0 176.5-43.4 240.4-122.1 6.1-7.5 6.1-18.3 0-25.8zm-240.4,79.8c-36.9,0-66.9-30-66.9-66.9 0-36.9 30-66.9 66.9-66.9 36.9,0 66.9,30 66.9,66.9 0,36.9-30,66.9-66.9,66.9zm-197.8-66.9c37.8-42.2 82.9-71.1 131.5-84.9-25.2,19.7-41.5,50.4-41.5,84.9 0,34.4 16.2,65.1 41.5,84.9-48.6-13.8-93.6-42.7-131.5-84.9zm264.1,84.9c25.2-19.7 41.5-50.4 41.5-84.9 0-34.4-16.2-65.1-41.5-84.9 48.6,13.8 93.7,42.7 131.5,84.9-37.9,42.2-82.9,71.1-131.5,84.9z"/>
                                              </g>
                                            </svg>
                                            <svg class="private active" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 477.871 477.871" style="enable-background:new 0 0 477.871 477.871;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M474.609,228.901c-29.006-38.002-63.843-71.175-103.219-98.287l67.345-67.345c6.78-6.548,6.968-17.352,0.42-24.132
                                                            c-6.548-6.78-17.352-6.968-24.132-0.42c-0.142,0.137-0.282,0.277-0.42,0.42l-73.574,73.506
                                                            c-31.317-17.236-66.353-26.607-102.093-27.307C109.229,85.336,7.529,223.03,3.262,228.9c-4.349,5.983-4.349,14.087,0,20.07
                                                            c29.006,38.002,63.843,71.175,103.219,98.287l-67.345,67.345c-6.78,6.548-6.968,17.352-0.42,24.132
                                                            c6.548,6.78,17.352,6.968,24.132,0.42c0.142-0.137,0.282-0.277,0.42-0.42l73.574-73.506
                                                            c31.317,17.236,66.353,26.607,102.093,27.307c129.707,0,231.407-137.694,235.674-143.565
                                                            C478.959,242.988,478.959,234.884,474.609,228.901z M131.296,322.494c-34.767-23.156-65.931-51.311-92.484-83.558
                                                            c25.122-30.43,106.598-119.467,200.124-119.467c26.609,0.538,52.77,6.949,76.612,18.773L285.92,167.87
                                                            c-39.2-26.025-92.076-15.345-118.101,23.855c-18.958,28.555-18.958,65.691,0,94.246L131.296,322.494z M285.016,217.005
                                                            c3.34,6.83,5.091,14.328,5.12,21.931c0,28.277-22.923,51.2-51.2,51.2c-7.603-0.029-15.101-1.78-21.931-5.12L285.016,217.005z
                                                             M192.856,260.866c-3.34-6.83-5.091-14.328-5.12-21.931c0-28.277,22.923-51.2,51.2-51.2c7.603,0.029,15.101,1.78,21.931,5.12
                                                            L192.856,260.866z M238.936,358.402c-26.609-0.538-52.769-6.949-76.612-18.773l29.628-29.628
                                                            c39.2,26.025,92.076,15.345,118.101-23.855c18.958-28.555,18.958-65.691,0-94.246l36.523-36.523
                                                            c34.767,23.156,65.931,51.312,92.484,83.558C413.937,269.366,332.461,358.402,238.936,358.402z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                </svg>
                                        </span>
                                    </div>

                                    @error('nueva_contrasena') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>

                            <div class="col-12 col-lg-4 mb-4">
                                <div class="row">
                                    <div class="col-12 pl-4">
                                        <label for="name" class="t-sm">Confirmar contraseña</label>
                                    </div>

                                    <div class="col-12">
                                        <label for="nueva_contrasena_confirmation" value="{{ __('Confirmar contraseña') }}" class="w-100"/>
                                        <input id="nueva_contrasena_confirmation" type="password" class="py-2 px-3 block w-100 @error('nueva_contrasena_confirmation') is-invalid @enderror t-m" wire:model.defer="nueva_contrasena_confirmation" placeholder="Confirmar contraseña" required/>
                                        <span>
                                            <svg class="visible" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 512 512">
                                              <g>
                                                <path d="m496.4,243.1c-63.9-78.7-149.3-122.1-240.4-122.1-91.1,0-176.5,43.4-240.4,122.1-6.1,7.5-6.1,18.2 0,25.7 63.9,78.8 149.3,122.2 240.4,122.2 91.1,0 176.5-43.4 240.4-122.1 6.1-7.5 6.1-18.3 0-25.8zm-240.4,79.8c-36.9,0-66.9-30-66.9-66.9 0-36.9 30-66.9 66.9-66.9 36.9,0 66.9,30 66.9,66.9 0,36.9-30,66.9-66.9,66.9zm-197.8-66.9c37.8-42.2 82.9-71.1 131.5-84.9-25.2,19.7-41.5,50.4-41.5,84.9 0,34.4 16.2,65.1 41.5,84.9-48.6-13.8-93.6-42.7-131.5-84.9zm264.1,84.9c25.2-19.7 41.5-50.4 41.5-84.9 0-34.4-16.2-65.1-41.5-84.9 48.6,13.8 93.7,42.7 131.5,84.9-37.9,42.2-82.9,71.1-131.5,84.9z"/>
                                              </g>
                                            </svg>
                                            <svg class="private active" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 477.871 477.871" style="enable-background:new 0 0 477.871 477.871;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M474.609,228.901c-29.006-38.002-63.843-71.175-103.219-98.287l67.345-67.345c6.78-6.548,6.968-17.352,0.42-24.132
                                                            c-6.548-6.78-17.352-6.968-24.132-0.42c-0.142,0.137-0.282,0.277-0.42,0.42l-73.574,73.506
                                                            c-31.317-17.236-66.353-26.607-102.093-27.307C109.229,85.336,7.529,223.03,3.262,228.9c-4.349,5.983-4.349,14.087,0,20.07
                                                            c29.006,38.002,63.843,71.175,103.219,98.287l-67.345,67.345c-6.78,6.548-6.968,17.352-0.42,24.132
                                                            c6.548,6.78,17.352,6.968,24.132,0.42c0.142-0.137,0.282-0.277,0.42-0.42l73.574-73.506
                                                            c31.317,17.236,66.353,26.607,102.093,27.307c129.707,0,231.407-137.694,235.674-143.565
                                                            C478.959,242.988,478.959,234.884,474.609,228.901z M131.296,322.494c-34.767-23.156-65.931-51.311-92.484-83.558
                                                            c25.122-30.43,106.598-119.467,200.124-119.467c26.609,0.538,52.77,6.949,76.612,18.773L285.92,167.87
                                                            c-39.2-26.025-92.076-15.345-118.101,23.855c-18.958,28.555-18.958,65.691,0,94.246L131.296,322.494z M285.016,217.005
                                                            c3.34,6.83,5.091,14.328,5.12,21.931c0,28.277-22.923,51.2-51.2,51.2c-7.603-0.029-15.101-1.78-21.931-5.12L285.016,217.005z
                                                             M192.856,260.866c-3.34-6.83-5.091-14.328-5.12-21.931c0-28.277,22.923-51.2,51.2-51.2c7.603,0.029,15.101,1.78,21.931,5.12
                                                            L192.856,260.866z M238.936,358.402c-26.609-0.538-52.769-6.949-76.612-18.773l29.628-29.628
                                                            c39.2,26.025,92.076,15.345,118.101-23.855c18.958-28.555,18.958-65.691,0-94.246l36.523-36.523
                                                            c34.767,23.156,65.931,51.312,92.484,83.558C413.937,269.366,332.461,358.402,238.936,358.402z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                </svg>
                                        </span>
                                    </div>

                                    @error('nueva_contrasena_confirmation') <span class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-12 d-flex justify-content-center">
                        <button type="button" wire:click="cancelPassword()" wire:loading.attr="disabled" class="d-flex justify-content-center align-items-center mr-3 cancel">Cancelar</button>
                        <button type="submit" wire:loading.attr="disabled" class="d-flex justify-content-center align-items-center save">Guardar</button>
                    </div>
                </div>
            </div>
        </form>
    </section>
    @else
    <section id="splash" wire:click="removeSplash">
        <div class="splash_container">
            <button type="button" class="close" wire:click="removeSplash">
                <svg height="20.46px" viewBox="0 0 329.26933 329" width="20.46px" xmlns="http://www.w3.org/2000/svg" fill="#FFF">
                    <path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/>
                </svg>
            </button>
            <div class="swiper-container" wire:ignore>
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <picture>
                            <source srcset="{{ asset('/imgs/splash/splash_0_md.png') }}" media="(min-width: 768px)">
                            <img class="img-fluid d-block" src="{{ asset('/imgs/splash/splash_0.png') }}" alt="paso 2 img"/>
                        </picture>
                        <p>Publica tus propiedades</p>
                    </div>
                    <div class="swiper-slide">
                        <picture>
                            <source srcset="{{ asset('/imgs/splash/splash_1_md.png') }}" media="(min-width: 768px)">
                            <img class="img-fluid d-block" src="{{ asset('/imgs/splash/splash_1.png') }}" alt="paso 2 img"/>
                        </picture>
                        <p>Filtra en tus búsquedas</p>
                    </div>
                    <div class="swiper-slide">
                        <picture>
                            <source srcset="{{ asset('/imgs/splash/splash_2_md.png') }}" media="(min-width: 768px)">
                            <img class="img-fluid d-block" src="{{ asset('/imgs/splash/splash_2.png') }}" alt="paso 2 img"/>
                        </picture>
                        <p>Filtra en tus búsquedas</p>
                    </div>
                </div>
            </div>

            <div class="swiper-pagination"></div>
        </div>
    </section>
    @endif
    @push('modals')
        <div class="modal fade" id="emailVerificadoModal" tabindex="-1" role="dialog" aria-labelledby="emailVerificadoModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close position-absolute d-none" data-dismiss="modal" aria-label="Close">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24.699 24.699">
                                <g id="Grupo_1390" data-name="Grupo 1390" transform="translate(-281.379 174.621)">
                                    <line id="Línea_199" data-name="Línea 199" y1="20.456" x2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                                    <line id="Línea_200" data-name="Línea 200" x2="20.456" y2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </button>
                        <p class="c-dpink t-b text-center mb-4">
                            Gracias por verificar su email
                        </p>

                        <p class="text-center mb-0">
                            <button type="button" class="b-dpink" data-dismiss="modal">Entendido</button>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="imgModal" tabindex="-1" role="dialog" aria-labelledby="imgModal" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Crop Image Before Upload</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="img-container">
                            <div class="row">
                                <div class="col-md-8">
                                    <img src="" id="sample_image" />
                                </div>
                                <div class="col-md-4">
                                    <div class="preview"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="crop" class="btn btn-primary">Crop</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    @endpush

    @section('styles')
        <link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css" />
        <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
        <link rel="stylesheet" href="/css/swiper-bundle.min.css">
    @endsection

    @section('scripts')
        <script src="https://unpkg.com/dropzone"></script>
        <script src="https://unpkg.com/cropperjs"></script>
        <script src="/js/swiper-bundle.min.js"></script>

        <script>
            var swiper = new Swiper('.swiper-container', {
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                spaceBetween: 30,
            });
        </script>
    @endsection
</main>


