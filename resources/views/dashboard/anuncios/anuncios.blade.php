<main id="mis_anuncios">
    <section class="anuncios mb-5">
        <h2 class="c-12 text-center title mb-4">Mis anuncios / <span class="c-pink">{{count($anuncios)}} Activos</span></h2>

        <div class="cards-block container mt-4 ">
            <div class="row px-3">
                @foreach($anuncios as $anuncio)
                    <div class="col-12 col-md-6 col-lg-4 mb-5">
                        <div class="card position-relative">
                            <a href="{{'/anuncio/' .$anuncio->url}}" class="card-img-top {{ $anuncio->foto_portada != null ? '' :  'd-flex justify-content-center bg-grey' }}">
                                @if($anuncio->foto_portada == null)
                                <img class="m-auto" src="/icons/anuncio_ni.svg">
                                @else
                                <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/anuncios_portadas/'. $anuncio->foto_portada .'_500.jpg')}}"></span>
                                @endif
                            </a>
                            <span class="public-date position-absolute py-0 px-2">{{$anuncio->publico ? 'Publicado '.$anuncio->updated_at->format('d-M-y') : 'Borrador'}}</span>
                            @if($anuncio->publico)
                            <span class="share top position-absolute" data-toggle="modal"
                                  data-target="#shareModal" url="{{str_replace(" ", "%20", $anuncio->url)}}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22">
                                    <g class="a">
                                        <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                                        <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                                    </g>
                                </svg>
                            </span>
                            @endif
                            <div href="{{'/anuncio/' .$anuncio->url}}" class="card-body py-2">
                                <div class="dashboard-buttons d-flex justify-content-between align-items-center mb-3">
                                    <a href="{{'/dashboard/anuncio/' .$anuncio->id}}" class="d-flex justify-content-center align-items-center text-decoration-none c-black">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="18.126" height="18.126" viewBox="0 0 18.126 18.126" class="mr-1" style="margin-top: -2px">
                                            <g id="Grupo_1392" data-name="Grupo 1392" transform="translate(-39 -723)">
                                                <path id="Trazado_7871" data-name="Trazado 7871" d="M14.889,41.905a.647.647,0,0,0-.647.647v6.474a.647.647,0,0,1-.647.647H1.942a.647.647,0,0,1-.647-.647V36.079a.647.647,0,0,1,.647-.647H9.71a.647.647,0,1,0,0-1.295H1.942A1.942,1.942,0,0,0,0,36.079V49.026a1.942,1.942,0,0,0,1.942,1.942H13.595a1.942,1.942,0,0,0,1.942-1.942V42.553A.647.647,0,0,0,14.889,41.905Z" transform="translate(39 690.158)"/>
                                                <path id="Trazado_7872" data-name="Trazado 7872" d="M115.925.73a2.478,2.478,0,0,0-3.505,0l-8.527,8.526a.654.654,0,0,0-.156.253l-1.295,3.884a.647.647,0,0,0,.614.852.658.658,0,0,0,.2-.033l3.884-1.295a.648.648,0,0,0,.253-.157l8.527-8.527A2.478,2.478,0,0,0,115.925.73Z" transform="translate(-59.524 722.996)"/>
                                            </g>
                                        </svg>
                                        Editar
                                    </a>
                                    <button class="d-flex justify-content-center align-items-center" wire:click="renovar('{{$anuncio->id}}')">
                                        <svg id="renovar" xmlns="http://www.w3.org/2000/svg" width="18.297" height="18.29" viewBox="0 0 18.297 18.29" class="mr-1">
                                            <path id="Trazado_7817" data-name="Trazado 7817" d="M22.1,2.058a.761.761,0,0,0-.831.165L18.223,5.272a.762.762,0,0,0,.539,1.3H21.81a.762.762,0,0,0,.762-.762V2.762A.762.762,0,0,0,22.1,2.058Z" transform="translate(-4.282 -0.476)"/>
                                            <path id="Trazado_7818" data-name="Trazado 7818" d="M3.81,16H.762A.762.762,0,0,0,0,16.762V19.81a.762.762,0,0,0,1.3.539L4.349,17.3A.762.762,0,0,0,3.81,16Z" transform="translate(0 -3.807)"/>
                                            <path id="Trazado_7819" data-name="Trazado 7819" d="M.763,8.81a.728.728,0,0,1-.1-.006.761.761,0,0,1-.657-.854A9.2,9.2,0,0,1,9.153,0a9.982,9.982,0,0,1,7.409,3.323.762.762,0,1,1-1.145,1.005,8.438,8.438,0,0,0-6.264-2.8A7.678,7.678,0,0,0,1.517,8.146a.761.761,0,0,1-.754.664Z" transform="translate(0)"/>
                                            <path id="Trazado_7820" data-name="Trazado 7820" d="M9.626,21.249a10.015,10.015,0,0,1-7.415-3.321.762.762,0,0,1,1.143-1.009,8.468,8.468,0,0,0,6.272,2.806A7.679,7.679,0,0,0,17.261,13.1a.762.762,0,1,1,1.51.2,9.2,9.2,0,0,1-9.146,7.949Z" transform="translate(-0.481 -2.959)"/>
                                            <path id="Trazado_7821" data-name="Trazado 7821" d="M14.551,8.87a.571.571,0,0,0-.5-.3H11.676l.651-3.906A.572.572,0,0,0,11.3,4.235L7.11,9.95a.571.571,0,0,0,.461.908h2.4l-.62,4.688a.57.57,0,0,0,.37.612.558.558,0,0,0,.2.035.571.571,0,0,0,.482-.264l4.136-6.478a.573.573,0,0,0,.02-.582Z" transform="translate(-1.665 -0.952)"/>
                                        </svg>
                                        Renovar
                                    </button>
                                    <button class="d-flex justify-content-center align-items-center" wire:click="elegirAnuncio('{{$anuncio->id}}')" data-toggle="modal" data-target="#eliminarAnuncioModal" >
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16.885" height="19.212" viewBox="0 0 16.885 19.212" class="mr-1" style="margin-top: -4px">
                                            <g id="basura" transform="translate(-31)">
                                                <g id="Grupo_1181" data-name="Grupo 1181" transform="translate(31)">
                                                    <g id="Grupo_1180" data-name="Grupo 1180">
                                                        <path id="Trazado_7840" data-name="Trazado 7840" d="M46.2,2.251H42.82V1.689A1.69,1.69,0,0,0,41.131,0H37.754a1.69,1.69,0,0,0-1.689,1.689v.563H32.689A1.69,1.69,0,0,0,31,3.94V5.066a.563.563,0,0,0,.563.563h.607l.99,12.043v0a1.68,1.68,0,0,0,1.682,1.536h9.2a1.68,1.68,0,0,0,1.682-1.536v0l.99-12.043h.607a.563.563,0,0,0,.563-.563V3.94A1.69,1.69,0,0,0,46.2,2.251Zm-9.005-.563a.563.563,0,0,1,.563-.563h3.377a.563.563,0,0,1,.563.563v.563h-4.5ZM44.6,17.576a.56.56,0,0,1-.56.51h-9.2a.56.56,0,0,1-.56-.51L33.3,5.628H45.586ZM46.76,4.5H32.126V3.94a.563.563,0,0,1,.563-.563H46.2a.563.563,0,0,1,.563.563Z" transform="translate(-31)"/>
                                                    </g>
                                                </g>
                                                <g id="Grupo_1183" data-name="Grupo 1183" transform="translate(38.88 6.754)">
                                                    <g id="Grupo_1182" data-name="Grupo 1182">
                                                        <path id="Trazado_7841" data-name="Trazado 7841" d="M241.563,180a.563.563,0,0,0-.563.563v7.955a.563.563,0,1,0,1.126,0v-7.955A.563.563,0,0,0,241.563,180Z" transform="translate(-241 -180)"/>
                                                    </g>
                                                </g>
                                                <g id="Grupo_1185" data-name="Grupo 1185" transform="translate(42.257 6.754)">
                                                    <g id="Grupo_1184" data-name="Grupo 1184">
                                                        <path id="Trazado_7842" data-name="Trazado 7842" d="M331.563,180a.563.563,0,0,0-.563.563v7.955a.563.563,0,1,0,1.126,0v-7.955A.563.563,0,0,0,331.563,180Z" transform="translate(-331 -180)"/>
                                                    </g>
                                                </g>
                                                <g id="Grupo_1187" data-name="Grupo 1187" transform="translate(35.503 6.754)">
                                                    <g id="Grupo_1186" data-name="Grupo 1186">
                                                        <path id="Trazado_7843" data-name="Trazado 7843" d="M151.563,180a.563.563,0,0,0-.563.563v7.955a.563.563,0,1,0,1.126,0v-7.955A.563.563,0,0,0,151.563,180Z" transform="translate(-151 -180)"/>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                        Eliminar
                                    </button>
                                </div>
                                <p class="price mb-3">
                                    {{number_format($anuncio->precio, 0, ',', '.')}}€
                                    @if($anuncio->antiguo_precio && $anuncio->antiguo_precio > $anuncio->precio)
                                        <del class="ml-2 c-lgrey">&nbsp; {{number_format($anuncio->antiguo_precio, 0, ',', '.')}}€ &nbsp;</del>
                                        <svg xmlns="http://www.w3.org/2000/svg" style="fill: #BF0000; transform: rotate(270deg);" height="16px" width="14px" viewBox="0 0 512 512"  xmlns:v="https://vecta.io/nano"><path d="M492 236H68.442l70.164-69.824c7.829-7.792 7.859-20.455.067-28.284s-20.456-7.859-28.285-.068l-104.504 104c-.007.006-.012.013-.018.019-7.809 7.792-7.834 20.496-.002 28.314.007.006.012.013.018.019l104.504 104c7.828 7.79 20.492 7.763 28.285-.068s7.762-20.492-.067-28.284L68.442 276H492c11.046 0 20-8.954 20-20s-8.954-20-20-20z"/></svg>
                                    @endif
                                </p>
                                <p class="details mb-3">
                                    @if($anuncio->habitaciones)
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25" height="21.162" class="bed"
                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                    C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                    c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                    c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                    V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                    c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                    c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                    c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                    c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                            </g>
                                        </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                                <g>
                                                </g>
                                    </svg>
                                    {{$anuncio->habitaciones}}bd,
                                    @endif
                                    @if($anuncio->baños)
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                                    s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                                    c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                                    c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                                    s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                                    c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                                    c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                                    c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                            </g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                    </svg>
                                    {{$anuncio->baños}}ba,
                                    @endif
                                    {{$anuncio->metros_cuadrados ? $anuncio->metros_cuadrados. ' m²' : ''}}
                                </p>
                                <p class="description">{{($anuncio->direccion ? $anuncio->direccion. ', ' : '') . ($anuncio->provincia ? $anuncio->provincia. ', ' : '') . ($anuncio->ciudad ? $anuncio->ciudad. ', ' : '') . $anuncio->codigo_postal}}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        @if(count($anuncios) == 0)
            <div class="col-12 p-0">
                <div class="no_found d-flex justify-content-center align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="135" height="135" viewBox="0 0 94.021 94.021">
                        <path id="video-marketing" d="M45.11,20.439A7.174,7.174,0,0,0,34.34,26.66v18.3A7.156,7.156,0,0,0,45.11,51.179l15.766-9.148a7.2,7.2,0,0,0,0-12.444ZM41.685,44.674V26.943l15.278,8.865Zm44.99-6.294V14.691A7.354,7.354,0,0,0,79.33,7.345H14.691a7.354,7.354,0,0,0-7.345,7.345V56.926a7.354,7.354,0,0,0,7.345,7.345H79.33a7.354,7.354,0,0,0,7.345-7.345,3.673,3.673,0,0,1,7.345,0A14.707,14.707,0,0,1,79.33,71.617H14.691A14.707,14.707,0,0,1,0,56.926V14.691A14.707,14.707,0,0,1,14.691,0H79.33A14.707,14.707,0,0,1,94.021,14.691V38.379a3.673,3.673,0,0,1-7.345,0Zm7.345,48.112a3.672,3.672,0,0,1-3.673,3.673H43.154a3.673,3.673,0,0,1,0-7.345H90.348A3.672,3.672,0,0,1,94.021,86.492Zm-61.885.184a7.346,7.346,0,0,1-13.811,3.489H3.673a3.673,3.673,0,0,1,0-7.345H18.539a7.345,7.345,0,0,1,13.6,3.856Zm0,0" fill="#cecece"/>
                    </svg>
                    <span>Ningún tour publicado</span>
                    <a class="p_s_button d-flex justify-content-center align-items-center" href="/dashboard/anuncio/crear">Publicar vivienda</a>
                </div>
            </div>
        @endif
    </section>

    <div wire:ignore class="modal fade" id="eliminarAnuncioModal" tabindex="-1" role="dialog" aria-labelledby="eliminarAnuncioModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close position-absolute d-none" data-dismiss="modal" aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24.699 24.699">
                            <g id="Grupo_1390" data-name="Grupo 1390" transform="translate(-281.379 174.621)">
                                <line id="Línea_199" data-name="Línea 199" y1="20.456" x2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                                <line id="Línea_200" data-name="Línea 200" x2="20.456" y2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                            </g>
                        </svg>
                    </button>
                    <p class="c-dpink t-b text-center mb-4">
                        ¿Seguro que desea eliminar este anuncio?
                    </p>

                    <p class="text-center">
                        <button type="button" class="b-dpink" wire:click="eliminarAnuncio">Eliminar</button>
                    </p>

                    <p class="text-center mb-0">
                        <button type="button" class="b-dpink" data-dismiss="modal" aria-label="Close">Cancelar</button>
                    </p>
                </div>
            </div>
        </div>
    </div>
</main>
