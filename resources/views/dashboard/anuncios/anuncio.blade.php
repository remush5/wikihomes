<main id="mi_anuncio" class="mb-4">
    <form wire:submit.prevent="save">
        @csrf
        <section class="cabecera mb-4">
            <div class="container h-100">
                <div class="d-flex flex-wrap h-100">
                    <div class="col-4 d-flex align-items-center justify-content-start h-100">
                        <button type="button" class="c-dpink t-m text-left p-0" data-toggle="modal"
                                data-target="#abandonarProcesoModal">Cancelar
                        </button>
                    </div>

                    <div class="col-4 d-flex align-items-center justify-content-center h-100">
                        <span class="t-m">{{$interaccion == 'editar' ? 'Editar' : 'Nuevo'}}</span>
                    </div>

                    <div class="col-4 d-flex align-items-center justify-content-end h-100">
                        <button type="button"
                                class="c-dpink t-m text-right d-flex align-items-center justify-content-end"
                                wire:click="saveAsDraft">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14.582" height="14.582"
                                 viewBox="0 0 14.582 14.582" class="mr-1">
                                <g id="Grupo_1388" data-name="Grupo 1388" transform="translate(-840.26 -102)">
                                    <g id="save" transform="translate(840.26 102)">
                                        <path id="Trazado_7988" data-name="Trazado 7988"
                                              d="M14.048,2.2,12.382.534A1.823,1.823,0,0,0,11.093,0H10.025V3.19a.456.456,0,0,1-.456.456H2.278a.456.456,0,0,1-.456-.456V0H.911A.911.911,0,0,0,0,.911V13.671a.911.911,0,0,0,.911.911H13.671a.911.911,0,0,0,.911-.911V3.489A1.823,1.823,0,0,0,14.048,2.2ZM12.759,12.759H1.823V7.291H12.759Z"
                                              transform="translate(0 0)" fill="#551a9d"/>
                                        <rect id="Rectángulo_14892" data-name="Rectángulo 14892" width="2" height="3"
                                              transform="translate(6.74)" fill="#551a9d"/>
                                    </g>
                                </g>
                            </svg>
                            Borrador
                        </button>
                    </div>
                </div>
            </div>
        </section>

        @if($parte == 1)
            <section class="primera-parte mb-4">
                <div class="container">
                    <div class="d-flex flex-wrap">
                        <div class="col-12 mb-2">
                            <div class="row">
                                <div class="col-12 col-md-6 mb-2 ">
                                    <div class="row">
                                        <div class="col-12 pl-4">
                                            <label for="objetivo" class="t-sm">Objetivo</label>
                                        </div>

                                        <div class="col-12">
                                            <label for="objetivo" value="{{ __('Objetivo') }}" class="w-100"/>
                                            <select id="objetivo" type="text"
                                                    class="form-control py-2 px-3 block w-100 @error('objetivo') is-invalid @enderror t-m"
                                                    wire:model="objetivo" required>
                                                <option value=""></option>
                                                <option value="vender">Vender</option>
                                                <option value="alquilar">Alquilar</option>
                                            </select>
                                        </div>

                                        @error('objetivo') <span
                                            class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 mb-2 ">
                                    <div class="row">
                                        <div class="col-12 pl-4">
                                            <label for="tipo" class="t-sm">Tipo de inmueble</label>
                                        </div>

                                        <div class="col-12">
                                            <label for="tipo" value="{{ __('Tipo de inmueble') }}" class="w-100"/>
                                            <select id="tipo" type="text"
                                                    class="form-control py-2 px-3 block w-100 @error('tipo') is-invalid @enderror t-m"
                                                    wire:model="tipo" required>
                                                <option value=""></option>
                                                <option value="Piso de obra nueva">Piso de obra nueva</option>
                                                <option value="Casa o chalet de obra nueva">Casa o chalet de obra
                                                    nueva
                                                </option>
                                                <option value="Piso">Piso</option>
                                                <option value="Casa o chalet">Casa o chalet</option>
                                                <option value="Casas rústicas">Casas rústicas</option>
                                                <option value="Dúplex">Dúplex</option>
                                                <option value="Ático">Ático</option>
                                                <option value="Oficina">Oficina</option>
                                                <option value="Local o nave">Local o nave</option>
                                                <option value="Terreno">Terreno</option>
                                            </select>
                                        </div>

                                        @error('tipo') <span
                                            class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-2">
                            <div class="row">
                                <div class="col-12 col-md-6 mb-2">
                                    <div class="row">
                                        <div class="col-12 pl-4">
                                            <label for="tour_url" class="t-sm">Adjuntar tour virtual</label>
                                        </div>

                                        <div class="col-12">
                                            <label for="tour_url" value="{{ __('URL') }}"
                                                   class="w-100 position-relative"/>
                                            <input id="tour_url" type="text"
                                                   class="py-2 px-3 pr-5 block w-100 @error('tour_url') is-invalid @enderror t-m"
                                                   wire:model.change="tour_url" placeholder="URL" required/>
                                            <img src="{{asset('icons/favicon.png')}}" alt="wikihoms favicon"
                                                 class="position-absolute" style="top: 9px; right: 14px;">
                                        </div>

                                        @error('tour_url') <span
                                            class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 mb-2">
                                    <div class="row">
                                        <div class="col-12 pl-4">
                                            <label for="tour_url" class="t-sm">Adjuntar foto de portada</label>
                                        </div>

                                        <div class="col-12">
                                            <label for="foto_portada" value="{{ __('Adjuntar foto de portada') }}"
                                                   class="w-100 mb-0 "/>
                                            <input id="foto_portada" type="file"
                                                   class="invisible position-absolute form-control w-0 @error('foto_portada') is-invalid @enderror t-m"
                                                   wire:model="foto_portada" wire:change="changeFieldValue()"/>
                                            <label for="foto_portada"
                                                   class="form-control py-2 px-3 block w-100 plano mb-0 h-100 @error('foto_portada') is-invalid @enderror t-m">{{$image_name}}</label>
                                        </div>

                                        @error('foto_portada') <span
                                            class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-2">
                            <div class="row">
                                <div class="col-12 col-md-6 mb-2">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12 pl-4">
                                                    <label for="precio" class="t-sm">Precio</label>
                                                </div>

                                                <div class="col-12">
                                                    <label for="precio" value="{{ __('Precio') }}" class="w-100"/>
                                                    <input id="precio" type="number"
                                                           class="form-control py-2 px-3 block w-100 @error('precio') is-invalid @enderror t-m"
                                                           wire:model="precio" placeholder="Precio" min="1"
                                                           max="9999999999" required/>
                                                </div>

                                                @error('precio') <span
                                                    class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12 pl-4">
                                                    <label for="habitaciones" class="t-sm">Habitaciones</label>
                                                </div>

                                                <div class="col-12">
                                                    <label for="habitaciones" value="{{ __('Habitaciones') }}"
                                                           class="w-100"/>
                                                    <select id="habitaciones"
                                                            class="form-control py-2 px-3 block w-100 @error('habitaciones') is-invalid @enderror t-m"
                                                            wire:model="habitaciones" required>
                                                        <option value=""></option>
                                                        @for ($i = 0; $i <= 20; $i++)
                                                            <option value="{{$i}}">{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>

                                                @error('habitaciones') <span
                                                    class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 mb-2">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12 pl-4">
                                                    <label for="baños" class="t-sm">Baños</label>
                                                </div>

                                                <div class="col-12">
                                                    <label for="baños" value="{{ __('Baños') }}" class="w-100"/>
                                                    <select id="baños"
                                                            class="form-control py-2 px-3 block w-100 @error('baños') is-invalid @enderror t-m"
                                                            wire:model="baños" required>
                                                        <option value=""></option>
                                                        @for ($i = 0; $i <= 20; $i++)
                                                            <option value="{{$i}}">{{$i}}</option>
                                                        @endfor
                                                    </select>
                                                </div>

                                                @error('baños') <span
                                                    class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12 pl-4">
                                                    <label for="metros_cuadrados" class="t-sm">Metros cuadrados</label>
                                                </div>

                                                <div class="col-12">
                                                    <label for="metros_cuadrados" value="{{ __('Metros cuadrados') }}"
                                                           class="w-100"/>
                                                    <select id="metros_cuadrados"
                                                            class="form-control py-2 px-3 block w-100 @error('metros_cuadrados') is-invalid @enderror t-m"
                                                            wire:model="metros_cuadrados" required>
                                                        <option value=""></option>
                                                        @for ($i = 0; $i <= 400; $i++)
                                                            <option value="{{$i}}">{{$i}}</option>
                                                        @endfor
                                                    </select></div>

                                                @error('metros_cuadrados') <span
                                                    class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-5">
                            <div class="row">
                                <div class="col-12 col-md-6 mb-2">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12 pl-4">
                                                    <label for="año" class="t-sm">Año</label>
                                                </div>

                                                <div class="col-12">
                                                    <label for="año" value="{{ __('Año') }}" class="w-100"/>
                                                    <select id="año"
                                                            class="form-control py-2 px-3 block w-100 @error('año') is-invalid @enderror t-m"
                                                            wire:model="año" required>
                                                        <option value=""></option>
                                                        @for ($i = 1980; $i <= 2021; $i++)
                                                            <option value="{{$i}}">{{$i}}</option>
                                                        @endfor
                                                    </select></div>

                                                @error('año') <span
                                                    class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12  pl-4">
                                                    <label for="estado" class="t-sm">Estado</label>
                                                </div>

                                                <div class="col-12">
                                                    <label for="estado" value="{{ __('Estado') }}" class="w-100"/>
                                                    <select id="estado" type="text"
                                                            class="form-control py-2 px-3 block w-100 @error('estado') is-invalid @enderror t-m"
                                                            wire:model="estado" required>
                                                        <option value=""></option>
                                                        <option value="Obra nueva">Obra nueva</option>
                                                        <option value="Buen estado">Buen estado</option>
                                                        <option value="A reformar">A reformar</option>
                                                    </select>
                                                </div>

                                                @error('estado') <span
                                                    class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 mb-2">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12 pl-4">
                                                    <label for="amueblado" class="t-sm">Amueblado</label>
                                                </div>

                                                <div class="col-12">
                                                    <label for="amueblado" value="{{ __('Amueblado') }}" class="w-100"/>
                                                    <select id="amueblado" type="text"
                                                            class="form-control py-2 px-3 block w-100 @error('amueblado') is-invalid @enderror t-m"
                                                            wire:model="amueblado" required>
                                                        <option value=""></option>
                                                        <option value="Amueblado">Amueblado</option>
                                                        <option value="Sin amueblar">Sin amueblado</option>
                                                        <option value="Cocina amueblada">Cocina amueblada</option>
                                                    </select>
                                                </div>

                                                @error('amueblado') <span
                                                    class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-2">
                            <div class="row justify-content-end">
                                <div class="col-6">
                                    <button class="next ml-1" type="button" wire:click="validateFirstPart">Siguiente
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        @if($parte == 2)
            <section class="segunda-parte mb-4">
                <div class="container">
                    <div class="d-flex flex-wrap">

                        <div class="col-12 mb-2">
                            <div class="row">
                                <div class="col-12 col-md-6 mb-2">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12 pl-4">
                                                    <label for="provincia" class="t-sm">Provincia</label>
                                                </div>

                                                <div class="col-12">
                                                    <label for="provincia" value="{{ __('Provincia') }}" class="w-100"/>
                                                    <select id="provincia" type="text"
                                                            class="form-control py-2 px-3 block w-100 @error('provincia') is-invalid @enderror t-m"
                                                            wire:model.defer="provincia" wire:change="getCiudades"
                                                            required>
                                                        <option value=""></option>
                                                        @foreach($provincias as $provincia)
                                                            <option
                                                                value="{{$provincia->nombre}}">{{$provincia->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                @error('provincia') <span
                                                    class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="row">
                                                <div class="col-12 pl-4">
                                                    <label for="ciudad" class="t-sm">Ciudad</label>
                                                </div>

                                                <div class="col-12">
                                                    <label for="ciudad" value="{{ __('Ciudad') }}" class="w-100"/>
                                                    <select id="ciudad" type="text"
                                                            class="form-control py-2 px-3 block w-100 @error('ciudad') is-invalid @enderror t-m"
                                                            wire:model.defer="ciudad" required>
                                                        <option value=""></option>
                                                        @foreach($ciudades as $ciudad)
                                                            <option
                                                                value="{{$ciudad->nombre}}">{{$ciudad->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                @error('ciudad') <span
                                                    class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 mb-2">
                                    <div class="row">
                                        <div class="col-12 pl-4">
                                            <label for="codigo_postal" class="t-sm">Código postal</label>
                                        </div>

                                        <div class="col-12">
                                            <label for="codigo_postal" value="{{ __('Código postal') }}" class="w-100"/>
                                            <input id="codigo_postal" type="number"
                                                   class="form-control py-2 px-3 block w-100 @error('codigo_postal') is-invalid @enderror t-m"
                                                   wire:model.defer="codigo_postal" placeholder="Código postal" required
                                                   min="1" max="99999"/>
                                        </div>

                                        @error('codigo_postal') <span
                                            class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-5">
                            <div class="row">
                                <div class="col-12 col-md-6 mb-2">
                                    <div class="row">
                                        <div class="col-12 pl-4">
                                            <label for="direccion" class="t-sm">Dirección</label>
                                        </div>

                                        <div class="col-12">
                                            <label for="direccion" value="{{ __('Dirección') }}" class="w-100"/>
                                            <input id="direccion" type="text"
                                                   class="form-control py-2 px-3 block w-100 @error('direccion') is-invalid @enderror t-m"
                                                   wire:model.defer="direccion" placeholder="Dirección" required/>
                                        </div>

                                        @error('direccion') <span
                                            class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                    </div>
                                </div>

                                <div class="col-12 col-md-6 mb-2">
                                    <div class="row">
                                        <div class="col-12 pl-4">
                                            <label for="descripcion" class="t-sm">Descripción</label>
                                        </div>

                                        <div class="col-12">
                                            <label for="descripcion" value="{{ __('URL') }}" class="w-100"/>
                                            <textarea id="descripcion"
                                                      class="form-control py-2 px-3 block w-100 @error('descripcion') is-invalid @enderror t-m"
                                                      wire:model="descripcion" max="800" required></textarea>
                                            <label for="description" class="pl-3 mb-0 t-m">
                                                <span
                                                    class="t-m">{{$descripcion == '' ? 0 : strlen($descripcion)}}</span>/800
                                                caracteres
                                            </label>
                                        </div>

                                        @error('descripcion') <span
                                            class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 mb-2">
                            <div class="row justify-content-end">
                                <div class="col-6 d-flex justify-content-end">
                                    <button class="back" type="button" wire:click="getBack">Atras</button>
                                </div>

                                <div class="col-6">
                                    <button class="next" type="button" wire:click="validateSecondPart">Siguiente
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        @if($parte == 3)
            <section class="tercera-parte mb-4">
                <div class="container">
                    <div class="d-flex flex-wrap">
                        <div class="col-12 mb-5">
                            <div class="row">
                                <div class="col-12 pl-4">
                                    <label for="provincia" class="t-sm">Características</label>
                                </div>

                                @foreach ($a_caracteristicas as $caracteristica)
                                    <div class="col-6 col-md-4 col-lg-3">
                                        <label for="{{$caracteristica}}" value="{{ $caracteristica }}" class="w-100"/>

                                        <div class="checkbox d-flex flex-nowrap align-items-center">
                                            <input type="checkbox"
                                                   class="form-check-input position-absolute ml-0 mt-0 mr-3 invisible"
                                                   class="form-control py-2 px-3 block w-100 @error('caracteristicas.{{$caracteristica}}') is-invalid @enderror t-m"
                                                   wire:model="caracteristicas.{{$caracteristica}}"
                                                   name="{{$caracteristica}}" id="{{$caracteristica}}">
                                            <label class="form-check-label d-flex align-items-center"
                                                   for="{{$caracteristica}}">
                                                <span class="t-m fl-cap">{{str_replace('_', ' ', $caracteristica)}}</span>
                                            </label>
                                        </div>

                                        @error('caracteristicas.{{$caracteristica}}') <span
                                            class="col-12 text-danger error t-sm">{{ $message }}</span>@enderror
                                    </div>

                                @endforeach
                            </div>
                        </div>

                        <div class="col-12 mb-2">
                            <div class="row justify-content-end">
                                <div class="col-6 d-flex justify-content-end">
                                    <button class="back" type="button" wire:click="getBack">Atras</button>
                                </div>

                                <div class="col-6">
                                    <button type="submit" class="save">Siguiente</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    </form>

    <div class="modal fade" id="abandonarProcesoModal" tabindex="-1" role="dialog"
         aria-labelledby="abandonarProcesoModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close position-absolute d-none" data-dismiss="modal"
                            aria-label="Close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24.699 24.699">
                            <g id="Grupo_1390" data-name="Grupo 1390" transform="translate(-281.379 174.621)">
                                <line id="Línea_199" data-name="Línea 199" y1="20.456" x2="20.456"
                                      transform="translate(283.5 -172.5)" fill="none" stroke="#fff"
                                      stroke-linecap="round" stroke-width="3"/>
                                <line id="Línea_200" data-name="Línea 200" x2="20.456" y2="20.456"
                                      transform="translate(283.5 -172.5)" fill="none" stroke="#fff"
                                      stroke-linecap="round" stroke-width="3"/>
                            </g>
                        </svg>
                    </button>
                    <p class="c-dpink t-b text-center mb-4">
                        ¿Seguro que desea abandonar el proceso?
                    </p>

                    <p class="text-center">
                        <button type="button" class="b-dpink" wire:click="saveAsDraft">Guardar borrador</button>
                    </p>

                    <p class="d-flex justify-content-center mb-0">
                        <a href="{{ route('dashboard.anuncios') }}"
                           class="b-dpink text-decoration-none d-flex justify-content-center align-items-center">Si,
                            quiero salir</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

</main>
