<main id="anuncios_view" class="bg-white">
    <section class="cabecera container">
        <div class="d-flex col-12">
            <div class="col-6">
                <a onclick="window.history.back()" class="atras d-flex justify-content-center align-items-center">Atrás</a>
            </div>

            <div class="col-6  d-flex justify-content-end pr-2">
                <button type="button" class="share ml-3" data-toggle="modal"
                        data-target="#shareModal" url="{{str_replace(" ", "%20", $anuncio->url)}}">
                    <svg xmlns="http://www.w3.org/2000/svg" width="29.25" height="29.25" viewBox="0 0 23.22 23.22">
                        <g class="a">
                            <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>
                            <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>
                        </g>
                    </svg>
                </button>
            </div>
        </div>
    </section>

    <section class="tour-container container-xl">
       {{-- <img class="img- w-100" src="{{ asset('imgs/home/top.png') }}" alt="header img "/>--}}
        <iframe id="inlineFrameExample"
                title="virtual tour iframe"
                class="w-100"
                height="500px"
                src="{{$anuncio->tour_url}}">
        </iframe>
    </section>

    <section class="info container mt-4">
        <div class="d-flex wrap justify-content-between">
            <div class="col-12 col-md-7">
                <p class="mb-3">
                    <span class="font-weight-bold c-dpink price mr-2 font-italic t-b">{{number_format($anuncio->precio, 0, ',', '.')}}€</span>
                    <span>
                        @if($anuncio->antiguo_precio && $anuncio->antiguo_precio > $anuncio->precio)
                            <del class="c-lgrey">&nbsp; {{number_format($anuncio->antiguo_precio, 0, ',', '.')}}€ &nbsp;</del>
                            <svg xmlns="http://www.w3.org/2000/svg" style="fill: #BF0000; transform: rotate(270deg);" height="16px" width="14px" viewBox="0 0 512 512"  xmlns:v="https://vecta.io/nano"><path d="M492 236H68.442l70.164-69.824c7.829-7.792 7.859-20.455.067-28.284s-20.456-7.859-28.285-.068l-104.504 104c-.007.006-.012.013-.018.019-7.809 7.792-7.834 20.496-.002 28.314.007.006.012.013.018.019l104.504 104c7.828 7.79 20.492 7.763 28.285-.068s7.762-20.492-.067-28.284L68.442 276H492c11.046 0 20-8.954 20-20s-8.954-20-20-20z"/></svg>
                        @endif
                    </span>
                </p>

                <p class="details mb-3">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25"
                         height="21.162" class="bed"
                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"
                         xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                    C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                    c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                    c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                    V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                    c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                    c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                    c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                    c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                            </g>
                                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g></svg>
                    {{$anuncio->habitaciones}}hab,
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999"
                         style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                        <g>
                            <g>
                                <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                    s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                    c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                    c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                    s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                    c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                    c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                    c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path
                                    d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                            </g>
                        </g>
                        <g>
                            <g>
                                <path
                                    d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                            </g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                        <g>
                        </g>
                    </svg>
                    {{$anuncio->baños}}ba, {{$anuncio->metros_cuadrados}} m²
                </p>

                <p class="c-grey mb-2">
                    {{$anuncio->direccion. ', ' . $anuncio->provincia. ', '. $anuncio->ciudad. ', '. $anuncio->codigo_postal}}
                </p>

                <p class="c-pink mb-4">
                    {{$anuncio->tipo}}
                </p>

                <p class="font-weight-bold c-dpink mb-3">
                    DESCRIPCIÓN
                </p>

                <p class="description c-grey {{strlen($anuncio->descripcion) > 300 ? 'mb-2' : 'mb-4'}}">
                    {{$anuncio->descripcion}}
                </p>

                @if(strlen($anuncio->descripcion) > 300)
                <button type="button" class="leer_mas c-pink pl-0 mb-4">Leer más</button>
                @endif

                <p class="font-weight-bold c-dpink mt-1 mb-1">
                    CARACTERÍSTICAS
                </p>

                <div class="d-flex flex-wrap">
                    @foreach ($a_caracteristicas as $caracteristica)
                        @if($caracteristicas[$caracteristica])
                            <div class="col-12 col-md-6 col-xl-4 mt-3 mb-2 d-flex align-items-center pl-0">
                                <img src="{{asset('/icons/caracteristicas/'.$caracteristica.'.svg')}}" class="mr-4" width="37" height="37" style="min-height: 37px; min-width: 37px;">
                                <span class="c-grey fl-cap">{{str_replace('_', ' ', $caracteristica)}}</span>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="col-5 col-md-4 d-none d-md-block">
                <form wire:submit.prevent="enviarMensaje" id="sendTopContactForm">
                    <div class="anunciante d-flex flex-column align-items-center">
                    <div class="imagen mb-2">
                        <img class="img-fluid" src="/imgs/user/pedro_2.png" alt="header img "/>
                    </div>

                    <p class="nombre mb-2">
                        <span class="font-weight-bold">Pedro Lirola</span>
                    </p>

                    <p class="direccion mb-3" >
                        <span class="c-pink">Agente Exclusivo</span>
                    </p>

                    <div class="row collapse w-100 pt-1" id="contactRightDropdown" wire:ignore.self>
                        <div class="options">
                            @if(!$loged)
                                <input type="text" placeholder="Nombre y apellidos" class="w-100 mb-2 t-m form-control py-2 px-3 pr-5 block" wire:model.defer="nombre" required/>
                                <input type="email" placeholder="Email" class="w-100 mb-2 t-m form-control py-2 px-3 pr-5 block"  wire:model.defer="email" required/>
                            @endif
                            <textarea name="mensaje" id="" cols="30" rows="10" class="w-100 mb-2 t-m form-control py-2 px-3 pr-5 block" placeholder="Mensaje al anunciante"  wire:model.defer="mensaje" required>
                            </textarea>
                        </div>

                        <div class="aditional-info">
                            Responsable: WikiHomes. Finalidad: Responder a las solicitudes de los usuarios. Derechos: Acceso, rectificación, supresión y demás derechos cómo se explica en la
                            <a class="c-pink">información adicional.</a>
                        </div>

                        <div class="tlf c-pink mb-3 mx-auto">
                            +34 620 78 52 43
                        </div>
                    </div>

                    <p class="contactar d-flex justify-content-between" id="contactButton">
                        <button type="submit" class="enviar d-none">Enviar</button>
                        <button type="button" class="abrir">Contactar</button>
                    </p>
                </div>
                </form>
            </div>
        </div>
    </section>

    <section class="map mt-5">
        <div class="container">
            <p class="col-12 font-weight-bold c-dpink mb-3">
                LOCALIZACIÓN
            </p>
        </div>

        <div class="map-container">
            <div id="address-map"></div>
        </div>
    </section>

    @if(count($anuncios) > 0)
        <section class="more">
            <div class="head d-flex align-items-center justify-content-center">
                <span>Resultados </span> <span class="ml-2 c-pink">similares</span>
            </div>

            <div class="cards-block container mt-4 ">
                <div class="row px-3">
                    @foreach($anuncios as $anuncio)
                        <div class="col-12 col-md-6 col-lg-4 mb-5 ">
                            <div class="card position-relative h-100">
                                <a href="{{'/anuncio/' .$anuncio->url}}" class="card-img-top">
                                    <span class="img-fluid d-block" style="background-image: url({{ asset('imgs/anuncios_portadas/'. $anuncio->foto_portada .'_500.jpg') }})"></span>
                                </a>
                               {{--}} <span class="public-date position-absolute py-0 px-2">
                                    Publicado {{$anuncio->updated_at->format('d-M-y')}}
                                </span>--}}
                                <span class="share position-absolute" data-toggle="modal"
                                      data-target="#shareModal" url="{{str_replace(" ", "%20", $anuncio->url)}}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22"
                                         viewBox="0 0 23.22 23.22">
                                        <g class="a">
                                            <path class="b"
                                                  d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0"
                                                  transform="translate(-122.199 0)"/>
                                            <path class="b"
                                                  d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0"
                                                  transform="translate(-0.004 -81.47)"/>
                                        </g>
                                    </svg>
                                </span>
                                <a href="{{'/anuncio/' .$anuncio->url}}" class="card-body py-3">
                                    <p class="price mb-3">
                                        {{number_format($anuncio->precio, 0, ',', '.')}}€
                                        @if($anuncio->antiguo_precio && $anuncio->antiguo_precio > $anuncio->precio)
                                            <del class="ml-2 c-lgrey">&nbsp; {{number_format($anuncio->antiguo_precio, 0, ',', '.')}}€ &nbsp;</del>
                                            <svg xmlns="http://www.w3.org/2000/svg" style="fill: #BF0000; transform: rotate(270deg);" height="16px" width="14px" viewBox="0 0 512 512"  xmlns:v="https://vecta.io/nano"><path d="M492 236H68.442l70.164-69.824c7.829-7.792 7.859-20.455.067-28.284s-20.456-7.859-28.285-.068l-104.504 104c-.007.006-.012.013-.018.019-7.809 7.792-7.834 20.496-.002 28.314.007.006.012.013.018.019l104.504 104c7.828 7.79 20.492 7.763 28.285-.068s7.762-20.492-.067-28.284L68.442 276H492c11.046 0 20-8.954 20-20s-8.954-20-20-20z"/></svg>
                                        @endif
                                    </p>
                                    <p class="details mb-3">
                                        <svg class="bed" xmlns="http://www.w3.org/2000/svg" width="24.25" height="21.162"  viewBox="0 0 24.25 18.162"><path d="M0,353.666v1.522a.71.71,0,0,0,1.421,0v-.811H22.829v.811a.71.71,0,0,0,1.421,0v-3.855H0Z" transform="translate(0 -337.737)"/><path d="M21.637,224.933H2.613A2.616,2.616,0,0,0,0,227.546V229.5H24.25v-1.953A2.616,2.616,0,0,0,21.637,224.933Z" transform="translate(0 -217.323)"/><path d="M175.792,158.533a1.193,1.193,0,0,0-1.192,1.192v.532h7.711v-.532a1.193,1.193,0,0,0-1.192-1.192Z" transform="translate(-166.33 -154.068)"/><path d="M68.073,69.923a2.616,2.616,0,0,1,2.613-2.613h5.327a2.616,2.616,0,0,1,2.613,2.613v.532h3.8V66.879a2.616,2.616,0,0,0-2.613-2.613H66.881a2.616,2.616,0,0,0-2.613,2.613v3.576h3.8Z" transform="translate(-61.224 -64.266)"/></svg>
                                        {{$anuncio->habitaciones}}hab,
                                        <svg xmlns="http://www.w3.org/2000/svg" width="21.15" height="21.151" class="bath" viewBox="0 0 21.15 21.151"><g transform="translate(0.827 14.281)"><path d="M20.021,345.689v1.7a3.961,3.961,0,0,0,3.335,3.906v1.26H24.6v-1.211H34.943v1.211h1.239V351.3a3.961,3.961,0,0,0,3.335-3.906v-1.7Z" transform="translate(-20.021 -345.689)"/></g><g transform="translate(0)"><path d="M2.072,10.561V3.41A2.171,2.171,0,0,1,6.4,3.149,2.9,2.9,0,0,0,4.14,5.971V6.8H9.928V5.971A2.9,2.9,0,0,0,7.642,3.143,3.41,3.41,0,0,0,.833,3.41v7.15H.006v2.481h21.15V10.561Z" transform="translate(-0.006)"/></g></svg>
                                        {{$anuncio->baños}}ba, {{$anuncio->metros_cuadrados}} m²
                                    </p>
                                    <p class="description">{{$anuncio->direccion. ', ' . $anuncio->provincia. ', '. $anuncio->ciudad. ', '. $anuncio->codigo_postal}}</p>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    <section class="mobile_info container-lg mt-5 remove-pm d-none">
        <form class="d-flex justify-content-between align-items-center w-100 flex-wrap " wire:submit.prevent="enviarMensaje" id="sendContactForm">
            <div class="d-flex flex-nowrap w-100 justify-content-between">
                <div class="d-flex flex-nowrap align-items-center">
                    <img class="img-fluid" src="/imgs/user/pedro_1.png" alt="header img "/>
                    <div class="ml-2 d-flex flex-column justify-content-center">
                        <div class="font-weight-bold">Pedro Lirola</div>
                        <div class="c-pink">Agente Exclusivo</div>
                    </div>
                </div>
                <div class="d-flex flex-nowrap">
                    <button type="submit" class="b-dpink mr-2 contact" id="sendContact">Enviar</button>
                    <button type="button" class="b-dpink contact" id="contactBtmButton">Contactar</button>
                </div>
            </div>

            <div class="collapse w-100 pt-4 px-0" id="contactBottomDropdown" wire:ignore.self>
                <div class="d-flex flex-wrap col-lg-10 px-0 pr-lg-4" >
                    @if(!$loged)
                        <div class="col-12 col-md-6 px-0 pr-md-2">
                            <input type="text" placeholder="Nombre y apellidos" class="mb-2 t-m form-control py-2 px-3 pr-5 block" wire:model.defer="nombre" required/>
                        </div>
                        <div class="col-12 col-md-6 px-0 pl-md-2">
                            <input type="email" placeholder="Email" class="mb-2 t-m form-control py-2 px-3 pr-5 block"  wire:model.defer="email" required/>
                        </div>
                    @endif
                    <div class="col-12 px-0">
                        <textarea name="mensaje" id="" cols="30" rows="10" class="w-100 mb-2 t-m form-control py-2 px-3 pr-5 block" placeholder="Mensaje al anunciante"  wire:model.defer="mensaje" required>
                        </textarea>
                    </div>
                    <div class="aditional-info">
                        Responsable: WikiHomes. Finalidad: Responder a las solicitudes de los usuarios. Derechos: Acceso, rectificación, supresión y demás derechos cómo se explica en la
                        <a class="c-pink">información adicional.</a>
                    </div>
                    <div class="col-12 px-0 d-flex d-lg-none">
                        <a href="tel:+34620785243" class="button {{$mostrar_telefono ? 'b-tlf c-dpink' : 'p_s_button'}} mr-1 w-50 d-flex align-items-center justify-content-center cursor-pointer" wire:click="setTelefono">
                            @if($mostrar_telefono)
                                +34 620 78 52 43
                            @else
                                Llamar
                            @endif
                        </a>
                        <button type="submit" class="button p_s_button w-50">
                            Enviar
                        </button>
                    </div>
                </div>
                <div class="col-2 px-0 d-none d-lg-flex flex-column user-info">
                    <a href="tel:+34620785243" class="c-dpink mb-2 t-b d-block">
                        +34 620 78 52 43
                    </a>

                    <div class="direccion">
                        Calle Faro, número 1 Almerimar (el ejido), 04711 - Almería
                    </div>
                </div>
            </div>
        </form>
    </section>
</main>

@section('scripts')
    @parent
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&map_ids=7dd2e481ecd0902b&libraries=geometry,drawing&v=3.42"></script>
    <script src="{{ asset('js/anuncio_view.js')}}"></script>
    <script src="/js/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            var map;
            var anuncio = {!! json_encode($anuncio) !!};

            function load() {
                map = new google.maps.Map(document.getElementById('address-map'), {
                    center: {
                        lat: anuncio.direccion_latitud,
                        lng: anuncio.direccion_longitud
                    },
                    zoom: 15,
                    mapTypeId: 'terrain',
                    options: {
                        streetViewControl: false,
                        clickableIcons: false,
                        mapTypeControl: false,
                        noClear: true,
                        scrollwheel: false,
                        zoomControl: true,
                        scaleControl: true,
                        rotateControl: false,
                        fullscreenControl: false,
                        disableDefaultUi: false
                    },
                    styles: [
                        {
                            featureType: "poi.business",
                            stylers: [{ visibility: "off" }],
                        },
                        {
                            featureType: "transit",
                            elementType: "labels.icon",
                            stylers: [{ visibility: "off" }],
                        },
                    ],
                });

                var markerLatlng = new google.maps.LatLng(parseFloat(anuncio.direccion_latitud),parseFloat(anuncio.direccion_longitud));

                new google.maps.Marker({
                    map: map,
                    position: markerLatlng,
                    icon: '../icons/marker.svg',
                });
            }

            load();

            var today = new Date();
            var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
            var time = today.getHours() + ":" + today.getMinutes();
            var dateTime = date+' '+time;
            $("#form_datetime").datetimepicker({
                format: 'yyyy-mm-dd hh:ii',
                autoclose: true,
                todayBtn: true,
                startDate: dateTime
            });

            $("#contactButton .abrir").on("click", function () {
                $('#contactRightDropdown').addClass('show');
                $('#contactButton .abrir').addClass('d-none');
                $('#contactButton .enviar').removeClass('d-none');
            });

            $("#sendTopContactForm").on("submit", function () {
                $('#contactRightDropdown').removeClass('show');
                $('#contactButton .abrir').removeClass('d-none');
                $('#contactButton .enviar').addClass('d-none');
                $("#solicitudEnviadaModal").modal('show');
            });


        });

    </script>
    <link href="/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
@stop
