<main class="bg-white anuncios_main">
    <section>
        <div class="home-cab">
            <div class="container">
                <div class="d-flex flex-wrap">
                    <div class="col-12 col-md-6 pl-md-0 order-md-1">
                        <img src="{{ asset('imgs/home/gafas_vr.png') }}" class="img-fluid w-100">
                    </div>

                    <div class="col-12 col-md-6 order-md-0">
                        <p class="title"><b>Wiki</b>Homes</p>

                        <p class="subtitle">Inmobiliaria virtual</p>

                        <p class="text">¡Más real que nunca!</p>

                        <a class="empezar_buscar cursor-pointer">Empezar a buscar</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="container d-flex flex-wrap align-content-lg-start">
            <a class="col-12 col-lg-4 home-caract d-flex flex-wrap cursor-pointer go_contact">
                <img src="{{ asset('imgs/home/eco-home.svg') }}" class=" img-fluid col-12 col-md-3 col-lg-12"/>

                <div class="col-12 col-md-9 col-lg-12">
                    <div class="title">Vendemos tu casa</div>
                    <div class="text">Vende tu propiedad rápido y sin complicaciones. Habla con Pedro Lirola y te
                        asesorará en todo.
                    </div>
                    <div class="a">Contactar</div>
                </div>
            </a>

            <a class="col-12 col-lg-4 home-caract d-flex flex-wrap cursor-pointer" href="/como_empezar">
                <img src="{{ asset('imgs/home/furniture.svg') }}" class="col-12 col-md-3 col-lg-12"/>

                <div class="col-12 col-md-9 col-lg-12">
                    <div class="title">Puesta en escena</div>
                    <div class="text">Acondicionamos y decoramos su propiedad para llamar la atención de los
                        compradores.
                    </div>
                    <div class="a">Cómo funciona</div>
                </div>
            </a>

            <a class="col-12 col-lg-4 home-caract d-flex flex-wrap cursor-pointer"  href="/crear_tour">
                <img src="{{ asset('imgs/home/360-degrees.svg') }}" class="col-12 col-md-3 col-lg-12"/>

                <div class="col-12 col-md-9 col-lg-12">
                    <div class="title">Creamos el tour virtual</div>
                    <div class="text">Un profesional preparado realizará las fotografías y videos de su propiedad y
                        montará el tour virtual
                    </div>
                    <div class="a">Crear tour</div>
                </div>
            </a>
        </div>
    </section>

    <section class="cabecera_botones" id="cabecera_botones">
        <div class="d-flex justify-content-center">
            <div class="d-none d-md-block mr-md-4 position-relative">
                <button type="button" class="ordenar_button dropdown-toggle ordernar_normal pl-2" >Ordenar
                </button>

                <div class="dropdown-menu px-2 pt-1" id="ordenarDropdown">
                    <div class="options">
                        <button
                            class="{{$order_by == 'precio' && $order_type == 'ASC' ? 'selected' : ''}} dropdown-item px-4 my-2 py-1"
                            wire:click="order('precio', 'ASC')">Precio más bajo
                        </button>
                        <button
                            class="{{$order_by == 'precio' && $order_type == 'DESC' ? 'selected' : ''}} dropdown-item px-4 my-2 py-1"
                            wire:click="order('precio', 'DESC')">Precio más alto
                        </button>
                        <button
                            class="{{$order_by == 'anuncios.updated_at' && $order_type == 'DESC' ? 'selected' : ''}} dropdown-item px-4 my-2 py-1"
                            wire:click="order('anuncios.updated_at', 'DESC')">Más Reciente
                        </button>
                    </div>
                </div>
            </div>

            <div class="d-none d-md-block mr-md-4">
                <button type="button" class="filtros_button">
                    <svg id="filter" xmlns="http://www.w3.org/2000/svg" width="17.348" height="15.894"
                         viewBox="0 0 17.348 15.894" class="my-auto mr-1">
                        <path id="Trazado_7983" data-name="Trazado 7983"
                              d="M80.936,81.788H70.6a2.132,2.132,0,0,0-4.042,0H64.946a.679.679,0,1,0,0,1.357h1.613a2.132,2.132,0,0,0,4.042,0H80.936a.679.679,0,1,0,0-1.357ZM68.58,83.241a.775.775,0,1,1,.775-.775A.776.776,0,0,1,68.58,83.241Z"
                              transform="translate(-64.267 -80.334)" fill="#ac5cff"/>
                        <path id="Trazado_7984" data-name="Trazado 7984"
                              d="M80.936,210.321H79.323a2.132,2.132,0,0,0-4.042,0H64.946a.679.679,0,0,0,0,1.357H75.281a2.132,2.132,0,0,0,4.042,0h1.613a.679.679,0,1,0,0-1.357ZM77.3,211.774a.775.775,0,1,1,.775-.775A.776.776,0,0,1,77.3,211.774Z"
                              transform="translate(-64.267 -203.052)" fill="#ac5cff"/>
                        <path id="Trazado_7985" data-name="Trazado 7985"
                              d="M80.936,338.854H73.508a2.132,2.132,0,0,0-4.042,0H64.946a.679.679,0,1,0,0,1.357h4.521a2.132,2.132,0,0,0,4.042,0h7.428a.679.679,0,1,0,0-1.357Zm-9.449,1.454a.775.775,0,1,1,.775-.775A.776.776,0,0,1,71.487,340.307Z"
                              transform="translate(-64.267 -325.77)" fill="#ac5cff"/>
                    </svg>
                    <span>Filtros</span>
                </button>
            </div>

            <div>
                <button type="button" class="mapa_button">Ocultar mapa</button>
            </div>
        </div>
    </section>

    <section id="address-map-container">
        <div id="address-map" wire:ignore></div>
    </section>

    <section class="anuncios">
        <div class="cabecera d-flex align-items-center justify-content-center">
            @if($tipo || $provincia)
                <span> {{$tipo}} / {{$provincia}}</span>
            @endif
            <span
                class="ml-2 c-pink">{{count($anuncios)}} {{count($anuncios) > 1 ? 'resultados' : 'resultado'}}</span>
        </div>

        <div class="cards-block container my-4 ">
            <div class="row px-3 ">
                @foreach($anuncios as $anuncio)
                    <div class="col-12 col-md-6 col-lg-4 mb-5 ">
                        <div class="card position-relative h-100">
                            <a href="{{'/anuncio/' .$anuncio->url}}" class="card-img-top">
                                <span class="img-fluid d-block"
                                      style="background-image: url({{ asset('imgs/anuncios_portadas/'. $anuncio->foto_portada .'_500.jpg') }})"></span>
                            </a>
                            {{--}}<span class="public-date position-absolute py-0 px-2">
                                Publicado {{$anuncio->updated_at->format('d-M-y')}}
                            </span>--}}
                            <span class="share position-absolute" data-toggle="modal"
                                  data-target="#shareModal" url="{{str_replace(" ", "%20", $anuncio->url)}}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22"
                                     viewBox="0 0 23.22 23.22">
                                    <g class="a">
                                        <path class="b"
                                              d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0"
                                              transform="translate(-122.199 0)"/>
                                        <path class="b"
                                              d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0"
                                              transform="translate(-0.004 -81.47)"/>
                                    </g>
                                </svg>
                            </span>
                            <a href="{{'/anuncio/' .$anuncio->url}}" class="card-body py-3">
                                <p class="price mb-3">{{number_format($anuncio->precio, 0, ',', '.')}}€
                                    @if($anuncio->antiguo_precio && $anuncio->antiguo_precio > $anuncio->precio)
                                        <del class="ml-2 c-lgrey">&nbsp; {{number_format($anuncio->antiguo_precio, 0, ',', '.')}}€ &nbsp;</del>
                                        <svg xmlns="http://www.w3.org/2000/svg" style="fill: #BF0000; transform: rotate(270deg);" height="16px" width="14px" viewBox="0 0 512 512"  xmlns:v="https://vecta.io/nano"><path d="M492 236H68.442l70.164-69.824c7.829-7.792 7.859-20.455.067-28.284s-20.456-7.859-28.285-.068l-104.504 104c-.007.006-.012.013-.018.019-7.809 7.792-7.834 20.496-.002 28.314.007.006.012.013.018.019l104.504 104c7.828 7.79 20.492 7.763 28.285-.068s7.762-20.492-.067-28.284L68.442 276H492c11.046 0 20-8.954 20-20s-8.954-20-20-20z"/></svg>
                                    @endif
                                </p>
                                <p class="details mb-3">
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24.25"
                                         height="21.162" class="bed"
                                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"
                                         xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M453.776,229.702V87.342c0-28.948-23.551-52.499-52.499-52.499H110.721c-28.948,0-52.497,23.551-52.497,52.499v142.36
                                                    C24.868,237.726,0,267.793,0,303.576v106.183c0,8.424,6.829,15.253,15.253,15.253h42.97v36.891
                                                    c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h334.54v36.891
                                                    c0,8.424,6.829,15.253,15.253,15.253c8.424,0,15.253-6.829,15.253-15.253v-36.891h42.97c8.424,0,15.253-6.829,15.253-15.253
                                                    V303.576C512,267.793,487.132,237.726,453.776,229.702z M88.73,87.342c0-12.126,9.866-21.992,21.991-21.992h290.557
                                                    c12.126,0,21.992,9.866,21.992,21.992v140.242h-50.405V203.52c0-39.87-32.437-72.306-72.306-72.306h-89.116
                                                    c-39.87,0-72.306,32.437-72.306,72.306v24.063H88.73V87.342z M342.359,203.52v24.063H169.641V203.52
                                                    c0-23.049,18.751-41.8,41.8-41.8h89.117C323.606,161.719,342.359,180.472,342.359,203.52z M30.506,394.506v-90.93
                                                    c0-25.081,20.405-45.486,45.486-45.486h360.014c25.081,0,45.486,20.405,45.486,45.486v90.93H30.506z"/>
                                            </g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g></svg>
                                    {{$anuncio->habitaciones}}hab,
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="21.15" height="21.151" class="bath" viewBox="0 0 511.999 511.999"
                                         style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve">
                        <g>
                            <g>
                                <path d="M505.584,297.925c0-8.441-6.844-15.285-15.285-15.285h-30.591V74.24c0-40.936-33.305-74.24-74.241-74.24
                                    s-74.24,33.304-74.24,74.24v27.864c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285V74.24
                                    c0-24.08,19.59-43.67,43.67-43.67c24.08,0,43.67,19.59,43.67,43.67v208.399H21.7c-8.441,0-15.285,6.844-15.285,15.285
                                    c0,68.984,43.631,127.962,104.744,150.808c-11.756,12.358-19.027,29.304-19.027,47.982c0,8.441,6.844,15.285,15.285,15.285
                                    s15.285-6.844,15.285-15.285c0-20.871,15.925-37.851,35.501-37.851c0.719,0,194.875,0,195.594,0
                                    c19.576,0,35.502,16.98,35.502,37.851c0,8.441,6.844,15.285,15.285,15.285c8.441,0,15.285-6.844,15.285-15.285
                                    c0-18.677-7.272-35.623-19.027-47.982C461.954,425.887,505.584,366.908,505.584,297.925z M344.645,428.294h-177.29
                                    c-66.718,0-121.89-50.372-129.479-115.084h436.248C466.535,377.922,411.361,428.294,344.645,428.294z"/>
                            </g>
                        </g>
                                        <g>
                                            <g>
                                                <path
                                                    d="M326.512,143.911c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,150.754,334.953,143.911,326.512,143.911z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path
                                                    d="M326.512,217.279c-8.441,0-15.285,6.844-15.285,15.285v13.247c0,8.441,6.844,15.285,15.285,15.285s15.285-6.844,15.285-15.285v-13.247C341.796,224.122,334.953,217.279,326.512,217.279z"/>
                                            </g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                                        <g>
                                        </g>
                    </svg>

                                    {{$anuncio->baños}}ba, {{$anuncio->metros_cuadrados}} m²
                                </p>
                                <p class="description">{{$anuncio->direccion. ', ' . $anuncio->provincia. ', '. $anuncio->ciudad. ', '. $anuncio->codigo_postal}}</p>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="telefono_botones d-md-none d-none">
        <div class="d-flex justify-content-center flex-wrap">
            <div class="col-12 row">
                <div class="collapse w-100 px-3 py-1" id="ordenarBottomDropdown">
                    <div class="options">
                        <button
                            class="{{$order_by == 'precio' && $order_type == 'ASC' ? 'selected' : ''}} dropdown-item px-3 my-2 py-1"
                            wire:click="order('precio', 'ASC')">Precio más bajo
                        </button>
                        <button
                            class="{{$order_by == 'precio' && $order_type == 'DESC' ? 'selected' : ''}} dropdown-item px-3 my-2 py-1"
                            wire:click="order('precio', 'DESC')">Precio más alto
                        </button>
                        <button
                            class="{{$order_by == 'anuncios.updated_at' && $order_type == 'DESC' ? 'selected' : ''}} dropdown-item px-3 my-2 py-1"
                            wire:click="order('anuncios.updated_at', 'DESC')">Más Reciente
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-12 row">
                <div class="col-6 pr-1">
                    <button type="button" class="ordenar_button ordenar_mobile">Ordenar</button>
                </div>
                <div class="col-6 pl-1">
                    <button type="button" class="filtros_button ">
                        <svg id="filter" xmlns="http://www.w3.org/2000/svg" width="17.348" height="15.894"
                             viewBox="0 0 17.348 15.894" class="my-auto mr-1">
                            <path id="Trazado_7983" data-name="Trazado 7983"
                                  d="M80.936,81.788H70.6a2.132,2.132,0,0,0-4.042,0H64.946a.679.679,0,1,0,0,1.357h1.613a2.132,2.132,0,0,0,4.042,0H80.936a.679.679,0,1,0,0-1.357ZM68.58,83.241a.775.775,0,1,1,.775-.775A.776.776,0,0,1,68.58,83.241Z"
                                  transform="translate(-64.267 -80.334)" fill="#ac5cff"/>
                            <path id="Trazado_7984" data-name="Trazado 7984"
                                  d="M80.936,210.321H79.323a2.132,2.132,0,0,0-4.042,0H64.946a.679.679,0,0,0,0,1.357H75.281a2.132,2.132,0,0,0,4.042,0h1.613a.679.679,0,1,0,0-1.357ZM77.3,211.774a.775.775,0,1,1,.775-.775A.776.776,0,0,1,77.3,211.774Z"
                                  transform="translate(-64.267 -203.052)" fill="#ac5cff"/>
                            <path id="Trazado_7985" data-name="Trazado 7985"
                                  d="M80.936,338.854H73.508a2.132,2.132,0,0,0-4.042,0H64.946a.679.679,0,1,0,0,1.357h4.521a2.132,2.132,0,0,0,4.042,0h7.428a.679.679,0,1,0,0-1.357Zm-9.449,1.454a.775.775,0,1,1,.775-.775A.776.776,0,0,1,71.487,340.307Z"
                                  transform="translate(-64.267 -325.77)" fill="#ac5cff"/>
                        </svg>
                        <span>Filtros</span>
                    </button>
                </div>
            </div>
        </div>
    </section>

    <div class="filtros" wire:ignore.self>
        <div class="col-12 buttons d-flex flex-wrap justify-content-center bg-white p-0">
            <div class="cabecera d-flex align-items-center justify-content-center mb-3">
                Ver <span class="ml-1 c-pink">{{$results}} {{$results > 1 ? 'resultados' : 'resultado'}}</span>
            </div>
        </div>

        <div class="d-flex flex-column mb-3">
            <div class="precio d-flex flex-wrap mt-2">
                <div class="col-12 mb-2">
                    <label class="mb-0 title">
                        Precio
                    </label>
                </div>
                <div class="col-6">
                   {{-- <select  class="form-control py-2 px-3 block w-100 @error('precio_min') is-invalid @enderror t-m"
                             wire:model="precio_min" wire:change="check">
                        <option disabled>Min</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>--}}
                    <div class="dropdown-toggle px-3" id="selectTipoButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <p class="m-0 t-m value">
                            {{$precio_min ? number_format($precio_min, 0, ',', '.') : 'Min'}}
                        </p>
                    </div>

                    <div class="dropdown-menu px-2" aria-labelledby="selectTipoButton">
                        <div class="options mt-2">
                            <button class="dropdown-item ml-2 px-2 mb-2 py-1" wire:click="setItem('precio_min', 60000')">60.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 80000)">80.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 100000)">100.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 120000)">120.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 140000)">140.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 150000)">150.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 160000)">160.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 180000)">180.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 200000)">200.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 220000)">220.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 240000)">240.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 250000)">250.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 260000)">260.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 280000)">280.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 300000)">300.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 320000)">320.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 360000)">360.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 380000)">380.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 400000)">400.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 450000)">450.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 500000)">500.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 550000)">550.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 600000)">600.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 650000)">650.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 700000)">700.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 750000)">750.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 800000)">800.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 900000)">900.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 1000000)">1M</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 1500000)">1.5M</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 2000000)">2M</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', 2500000)">2.5M</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_min', null)">Sin Limite</button>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="dropdown-toggle px-3" id="selectTipoButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <p class="m-0 t-m value">
                            {{$precio_max ? number_format($precio_max, 0, ',', '.') : 'Min'}}
                        </p>
                    </div>

                    <div class="dropdown-menu px-2" aria-labelledby="selectTipoButton">
                        <div class="options mt-2">
                            <button class="dropdown-item ml-2 px-2 mb-2 py-1" wire:click="setItem('precio_max', 60000)">60.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 80000)">80.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 100000)">100.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 120000)">120.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 140000)">140.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 150000)">150.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 160000)">160.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 180000)">180.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 200000)">200.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 220000)">220.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 240000)">240.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 250000)">250.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 260000)">260.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 280000)">280.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 300000)">300.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 320000)">320.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 360000)">360.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 380000)">380.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 400000)">400.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 450000)">450.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 500000)">500.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 550000)">550.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 600000)">600.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 650000)">650.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 700000)">700.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 750000)">750.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 800000)">800.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 900000)">900.000</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 1000000)">1M</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 1500000)">1.5M</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 2000000)">2M</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', 2500000)">2.5M</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('precio_max', null);">Sin Limite</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="habitaciones d-flex flex-wrap mt-4">
                <div class="col-12">
                    <label class="mb-2 title">
                        Habitaciones
                    </label>
                </div>

                <div class="col-12 d-flex flex-nowrap">
                    <input type="radio" wire:model="habitaciones" class="invisible position-absolute" value="all"
                           id="habitaciones_all" name="habitaciones" wire:change="check">
                    <label type="button"
                           class="form-control block px-0 border-0 mr-3 {{$habitaciones == 'all' ? 'c-pink' : ''}}"
                           for="habitaciones_all">Todos</label>

                    @for($i = 1; $i < 5; $i++)
                        <input type="radio" wire:model="habitaciones" class="invisible position-absolute"
                               value="{{$i}}" id="{{'habitaciones_'.$i}}" name="habitaciones" wire:change="check">
                        <label type="button"
                               class="form-control block {{$habitaciones == $i ? 'c-pink font-weight-bold' : ''}}"
                               for="{{'habitaciones_'.$i}}">{{$i}}</label>
                    @endfor

                    <input type="radio" wire:model="habitaciones" class="invisible position-absolute"
                           value="5" id="habitaciones_5" name="habitaciones" wire:change="check">
                    <label type="button"
                           class="form-control block {{$habitaciones == 5 ? 'c-pink font-weight-bold' : ''}}"
                           for="habitaciones_5">5+</label>
                </div>
            </div>

            <div class="baños d-flex flex-wrap mt-2">
                <div class="col-12">
                    <label class="mb-2 title">
                        Baños
                    </label>
                </div>

                <div class="col-12 d-flex flex-nowrap">
                    <input type="radio" wire:model="baños" class="invisible position-absolute" value="all"
                           id="baños_all" name="baños" wire:change="check">
                    <label type="button"
                           class="form-control block px-0 border-0 mr-3 {{$baños == 'all' ? 'c-pink' : ''}}"
                           for="baños_all">Todos</label>

                    @for($i = 1; $i < 5; $i++)
                        <input type="radio" wire:model="baños" class="invisible position-absolute" value="{{$i}}"
                               id="{{'baños_'.$i}}" name="baños" wire:change="check">
                        <label type="button"
                               class="form-control block {{$baños == $i ? 'c-pink font-weight-bold' : ''}}"
                               for="{{'baños_'.$i}}">{{$i}}</label>
                    @endfor

                    <input type="radio" wire:model="baños" class="invisible position-absolute" value="5"
                           id="baños_5" name="baños" wire:change="check">
                    <label type="button"
                           class="form-control block {{$baños == 5 ? 'c-pink font-weight-bold' : ''}}"
                           for="baños_5">5+</label>
                </div>
            </div>

            <div class="superficie d-flex flex-wrap mt-2">
                <div class="col-12 mb-2">
                    <label class="mb-0 title">
                        Superficie
                    </label>
                </div>
                <div class="col-6">
                    <div class="dropdown-toggle px-3" id="selectTipoButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <p class="m-0 t-m value">
                            {{$superficie_min ? number_format($superficie_min, 0, ',', '.') : 'Min'}}
                        </p>
                    </div>

                    <div class="dropdown-menu px-2" aria-labelledby="selectTipoButton">
                        <div class="options mt-2">
                            <button class="dropdown-item ml-2 px-2 mb-2 py-1" wire:click="setItem('superficie_min', 60')">60</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 80')">80</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 100)">100</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 120)">120</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 140)">140</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 160)">160</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 180)">180</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 200)">200</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 220)">220</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 240)">240</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 250)">250</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 300)">300</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 350)">350</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 400)">400</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 500)">500</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 550)">550</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 600)">600</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 650)">650</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 700)">700</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', 750)">750</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_min', null)">Sin Limite</button>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="dropdown-toggle px-3" id="selectTipoButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <p class="m-0 t-m value">
                            {{$superficie_max ? number_format($superficie_max, 0, ',', '.') : 'Min'}}
                        </p>
                    </div>

                    <div class="dropdown-menu px-2" aria-labelledby="selectTipoButton">
                        <div class="options mt-2">
                            <button class="dropdown-item ml-2 px-2 mb-2 py-1" wire:click="setItem('superficie_max', 60')">60</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 80')">80</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 100)">100</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 120)">120</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 140)">140</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 160)">160</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 180)">180</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 200)">200</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 220)">220</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 240)">240</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 250)">250</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 300)">300</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 350)">350</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 400)">400</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 500)">500</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 550)">550</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 600)">600</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 650)">650</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 700)">700</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', 750)">750</button>
                            <button class="dropdown-item ml-2 px-2 my-2 py-1" wire:click="setItem('superficie_max', null)">Sin Limite</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="caracteristicas d-flex flex-wrap mt-4">
                <div class="col-12">
                    <label class="mb-2 title">
                        Características
                    </label>
                </div>

                @foreach ($a_caracteristicas as $caracteristica)
                    <label class="col-12 mt-2 mb-0 d-flex flex-wrap justify-content-between cursor-pointer"
                           for="{{$caracteristica}}">
                        <div>
                            <label class=" cursor-pointer fl-cap"
                                   for="{{$caracteristica}}">{{str_replace('_', ' ', $caracteristica)}}</label>
                        </div>

                        <div class="checkbox d-flex flex-nowrap align-items-center">
                            <input type="checkbox" class="form-check-input position-absolute ml-0 mt-0 mr-3 invisible"
                                   class="form-control py-2 px-3 block w-100 t-m"
                                   wire:model="caracteristicas.{{$caracteristica}}" name="{{$caracteristica}}"
                                   id="{{$caracteristica}}" wire:change="check">
                            <label
                                class="form-check-label text-capitalize d-flex align-items-center pr-2 cursor-pointer"
                                for="{{$caracteristica}}"></label>
                        </div>
                    </label>
                @endforeach
            </div>
        </div>

        <div class="col-12 bottom-buttons d-flex flex-wrap justify-content-center bg-white">
            <div class="col-6 pl-0">
                <button type="button" wire:click="resetFilters" class="h">Cancelar</button>
            </div>

            <div class="col-6 pr-0">
                <button type="button" wire:click="applyFilters" class="{{$active_filt ? 'active' : ''}}">Aplicar</button>
            </div>
        </div>
    </div>

    <div class="mt-4 py-5 bg-lpink">
        <section class="contact container" id="contact">
            <form class="d-flex justify-content-between align-items-start w-100 flex-wrap py-md-5"
                  wire:submit.prevent="enviarMensaje" id="sendMainContactForm">
                <div class="d-flex flex-wrap align-items-center col-12 col-md-3 col-lg-2 pl-md-0 mb-4 mb-md-0">
                    <img src="/imgs/user/pedro_2.png" alt="header img" class="mb-3 mx-auto"/>
                    <div class="d-flex flex-column justify-content-center mx-auto w-100">
                        <div class="font-weight-bold text-center t-lg mb-2">Pedro Lirola</div>
                        <div class="c-pink text-center">Agente Exclusivo</div>
                    </div>
                </div>

                <div class="d-flex flex-wrap col-12 col-md-9 col-lg-8 pr-lg-5 pl-lg-4">
                    @if(!$loged)
                        <div class="col-12 col-md-6 px-0 pr-md-2">
                            <input type="text" placeholder="Nombre y apellidos"
                                   class="mb-2 t-m form-control py-2 px-3 pr-5 block" wire:model.defer="nombre"
                                   required/>
                        </div>
                        <div class="col-12 col-md-6 px-0 pl-md-2">
                            <input type="email" placeholder="Email" class="mb-2 t-m form-control py-2 px-3 pr-5 block"
                                   wire:model.defer="email" required/>
                        </div>
                    @endif
                    <div class="col-12 px-0">
                        <textarea name="mensaje" id="" cols="30" rows="10"
                                  class="w-100 mb-2 t-m form-control py-2 px-3 pr-5 block"
                                  placeholder="Mensaje al anunciante" wire:model.defer="mensaje" required>
                        </textarea>
                    </div>
                    <div class="aditional-info">
                        Responsable: WikiHomes. Finalidad: Responder a las solicitudes de los usuarios. Derechos:
                        Acceso, rectificación, supresión y demás derechos cómo se explica en la
                        <a href="/politicas_privacidad" class="c-pink">información adicional.</a>
                    </div>
                    <div class="col-12 px-0 d-flex d-lg-none flex-column">
                        <a href="tel:+34620785243" class="c-dpink mb-2 t-b d-block">
                            +34 620 78 52 43
                        </a>

                        <div class="direccion mb-3">
                            Calle Faro, número 1 Almerimar (el ejido), 04711 - Almería
                        </div>

                        <button type="submit" class="button b-dpink w-100">
                            Enviar
                        </button>
                    </div>
                </div>
                <div class="col-2 px-0 d-none d-lg-flex flex-column user-info pr-md-0">
                    <a href="tel:+34620785243" class="c-dpink mb-2 t-b d-block">
                        +34 620 78 52 43
                    </a>

                    <div class="direccion mb-4">
                        Calle Faro, número 1 Almerimar (el ejido), 04711 - Almería
                    </div>

                    <button type="submit" class="b-dpink contactar w-100">Enviar</button>
                </div>
            </form>
        </section>
    </div>
</main>

@section('scripts')
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&map_ids=7dd2e481ecd0902b&libraries=geometry,drawing&v=3.42"
        defer></script>

    <script src="{{ asset('js/anuncio_main.js')}}" defer></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var map;
            var markers = {!! json_encode($anuncios) !!};
            var marks = [];
            var latitude = 36.699660;
            var longitude = -2.791153;

            function load() {
                map = new google.maps.Map(document.getElementById('address-map'), {
                    center: {
                        lat: latitude,
                        lng: longitude
                    },
                    zoom: 12,
                    mapTypeId: 'terrain',
                    options: {
                        streetViewControl: false,
                        clickableIcons: false,
                        mapTypeControl: false,
                        noClear: true,
                        scrollwheel: false,
                        zoomControl: true,
                        scaleControl: true,
                        rotateControl: false,
                        fullscreenControl: false,
                        disableDefaultUi: false
                    },
                    styles: [
                        {
                            featureType: "poi.business",
                            stylers: [{visibility: "off"}],
                        },
                        {
                            featureType: "transit",
                            elementType: "labels.icon",
                            stylers: [{visibility: "off"}],
                        },
                    ],
                });

                for (var i = 0; i < markers.length; i++) {
                    marks[i] = addMarker(markers[i]);
                }
            }

            function addMarker(marker) {
                var mydate = new Date(marker.updated_at);
                const ye = new Intl.DateTimeFormat('es', {year: 'numeric'}).format(mydate);
                const mo = new Intl.DateTimeFormat('es', {month: 'short'}).format(mydate);
                const da = new Intl.DateTimeFormat('es', {day: '2-digit'}).format(mydate);

                var html = '<div class="card map-card position-relative">' +
                    '<a href="/anuncio/' + marker.url + '" class="card-img-top" style="height: 149px;"><span class="img-fluid d-block" style="background-image: url(&#039;/imgs/anuncios_portadas/' + marker.foto_portada + '_500.jpg&#039;); height: 149px;" ></span></a>' +
                   // '<span class="public-date position-absolute py-0 px-2">Publicado ' + da + '-' + mo + '-' + ye + '</span>' +
                    '<span class="share position-absolute" data-toggle="modal" data-target="#shareModal" url="' + marker.url + '">' +
                    '<svg xmlns="http://www.w3.org/2000/svg" width="23.22" height="23.22" viewBox="0 0 23.22 23.22"> <g class="a"> <path class="b" d="M145.215,6.752,138.927.222a.726.726,0,0,0-1.248.5V3.87h-.242A9.444,9.444,0,0,0,128,13.3v1.451a.716.716,0,0,0,.565.694.648.648,0,0,0,.16.019.751.751,0,0,0,.663-.413,7.936,7.936,0,0,1,7.138-4.412h1.149v3.144a.726.726,0,0,0,1.248.5l6.289-6.531A.726.726,0,0,0,145.215,6.752Zm0,0" transform="translate(-122.199 0)"/>  <path class="b" d="M20.321,104.689H2.906a2.906,2.906,0,0,1-2.9-2.9V88.242a2.906,2.906,0,0,1,2.9-2.9h2.9a.967.967,0,0,1,0,1.935h-2.9a.968.968,0,0,0-.968.968v13.545a.968.968,0,0,0,.968.968H20.321a.968.968,0,0,0,.967-.968v-7.74a.968.968,0,1,1,1.935,0v7.74A2.906,2.906,0,0,1,20.321,104.689Zm0,0" transform="translate(-0.004 -81.47)"/>  </g> </svg> ' +
                    '</span>' +
                    '<a href="/anuncio/' + marker.url + '" class="card-body py-2">' +
                    '<p class="price mb-2">' + marker.precio + '€</p>' +
                    '<p class="details mb-2">' +
                     marker.habitaciones + 'hab, ' +
                    marker.baños + 'ba, ' + marker.metros_cuadrados + 'm²</p>' +
                    '<p class="description">' + marker.direccion + ', ' + marker.provincia + ', ' + marker.ciudad + ', ' + marker.codigo_postal + '</p>' +
                    '</a></div>';

                var markerLatlng = new google.maps.LatLng(parseFloat(marker.direccion_latitud), parseFloat(marker.direccion_longitud));

                var mark = new google.maps.Marker({
                    map: map,
                    position: markerLatlng,
                    icon: '/icons/marker.svg',
                    content: marker
                });

                var infoWindow = new google.maps.InfoWindow;
                google.maps.event.addListener(mark, 'click', function () {
                    infoWindow.setContent(html);
                    infoWindow.open(map, mark);
                });

                google.maps.event.addListener(map, "click", function() {
                    infoWindow.close();
                    mark.open = false;
                });

                return mark;
            };

            if ('{!! $provincia !!}' != '') {
                var geocoder = new google.maps.Geocoder();

                geocoder.geocode({'address': '{!! $provincia !!}, España'}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        latitude = results[0].geometry.location.lat();
                        longitude = results[0].geometry.location.lng();
                        console.log(results[0].geometry.location.lng())
                    }

                    load();
                });
            } else {
                load();
            }

            $("#sendMainContactForm").on("submit", function () {
                $("#solicitudEnviadaModal").modal('show');
            });

            /*$(".prec-min .dropdown-item").on("click", function () {

                document.addEventListener('livewire:load', function () {
                    @this.precio = $(this).html();
                })
            });*/
        });
    </script>
@stop
