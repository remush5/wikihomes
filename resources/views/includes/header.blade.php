<!-- Page Heading -->
<header class="">
    <nav class="navbar py-md-4">
        <div class="container">
            <div class="w-100 d-flex flex-nowrap justify-content-between">
                <div class="hamburguesa d-lg-none d-flex align-items-center justify-content-between col">
                    <button class="navbar-toggle" data-toggle="collapse" data-target="#pages"
                            aria-controls="pages">
                        <span class="barra"></span>
                        <span class="barra"></span>
                        <span class="barra"></span>
                    </button>
                </div>

                <div class="justify-content-start d-none d-lg-block col pr-lg-0" id="#pages">
                    <ul class="navbar-nav flex flex-row">
                        @if (Request::is('dashboard/*') || Request::is('dashboard') )
                            <li class="nav-item active mr-4">
                                <a class="nav-link t-m" href="{{route('dashboard')}}">Mi perfil</a>
                            </li>
                            <li class="nav-item active mr-4">
                                <a class="nav-link t-m" href="{{route('dashboard.anuncios')}}">Mis anuncios</a>
                            </li>
                            <li class="nav-item active mr-4">
                                <a class="nav-link t-m" href="{{route('dashboard.favoritos')}}">Mis favoritos</a>
                            </li>
                        @else
                            <li class="nav-item active mr-4">
                                <a class="nav-link t-m" href="/como_empezar">Cómo funciona</a>
                            </li>
                            <li class="nav-item active mr-4">
                                <a class="nav-link t-m" href="/crear_tour">Creamos tu tour</a>
                            </li>
                        @endif
                    </ul>
                </div>

                <a href="/" class="logo navbar-brand d-block text-center px-0 mr-0 d-md-none">
                    <img src="{{ asset('imgs/logo/logo-mini.png') }}" alt=""
                         class="d-inline-block align-middle"/>
                </a>

                <a href="/" class="logo navbar-brand text-center mr-0 d-none d-md-block pr-lg-0 pl-lg-0">
                    <img src="{{ asset('imgs/logo/logo.png') }}" alt=""
                         class="d-inline-block align-middle"/>
                </a>

                <div class="right-items d-flex align-items-center justify-content-end pl-lg-0">
                    @if (Request::is('dashboard/*') || Request::is('dashboard') )
                        <ul class="nav navbar-nav flex flex-row flex-nowrap">
                            <li class="publish nav-item d-none d-lg-flex">
                                <a href="/dashboard/anuncio/crear">Publicar vivienda</a>
                            </li>
                        </ul>
                    @endif
                        {{--<ul class="nav navbar-nav flex flex-row flex-nowrap">
                            <li class="publish nav-item d-none d-lg-flex">
                                <a href="/dashboard/anuncio/crear">Publicar vivienda</a>
                            </li>

                            <li class="search nav-item">
                                <button class="bg-transparent px-0" data-toggle="collapse" data-target="#searchDropdown" aria-expanded="false" aria-controls="searchDropdown">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="28" viewBox="0 0 27.994 28">
                                        <g id="Grupo_1202" data-name="Grupo 1202" transform="translate(1408.47 1253.5)">
                                            <g id="_003-search" data-name="003-search" transform="translate(-1407.97 -1253)">
                                                <path id="Trazado_4" data-name="Trazado 4" d="M26.811,25.623l-6.561-6.561A11.522,11.522,0,1,0,19.112,20.2l6.561,6.561a.813.813,0,0,0,.569.239.789.789,0,0,0,.569-.239A.809.809,0,0,0,26.811,25.623ZM1.66,11.512a9.9,9.9,0,1,1,9.9,9.9A9.907,9.907,0,0,1,1.66,11.512Z" transform="translate(-0.05)" fill="#fff" stroke="#fff" stroke-width="1"/>
                                            </g>
                                        </g>
                                    </svg>
                                </button>
                            </li>

                            <li class="user-panel nav-item">
                                <div>
                                    @auth
                                        <span style="background-image: url('{{ Auth::user()->profile_photo_path == null ? '/imgs/logo/profile.svg' : '/storage/profile-photos/'.Auth::user()->profile_photo_path.'_140.jpg' }}'); background-position: center; background-size: cover;">
                                        </span>
                                    @else
                                        <svg id="perfil-del-usuario" xmlns="http://www.w3.org/2000/svg" width="19" height="27.365" viewBox="0 0 23.043 27.365">
                                            <path id="Trazado_7696" data-name="Trazado 7696" d="M24.6,22.875H21.829a8.627,8.627,0,0,0-8.749-8.482,8.627,8.627,0,0,0-8.748,8.482H1.559A11.4,11.4,0,0,1,13.081,11.62,11.4,11.4,0,0,1,24.6,22.875Z" transform="translate(-1.559 4.49)" fill="#fff"/>
                                            <path id="Trazado_7697" data-name="Trazado 7697" d="M11.989,15.209a7.6,7.6,0,1,1,7.606-7.6A7.614,7.614,0,0,1,11.989,15.209Zm0-12.436A4.832,4.832,0,1,0,16.822,7.6,4.836,4.836,0,0,0,11.989,2.773Z" transform="translate(-0.467)" fill="#fff"/>
                                        </svg>
                                    @endauth
                                </div>
                            </li>
                        </ul>--}}
                </div>
            </div>
        </div>
    </nav>

    {{--<div class="user-menu container">
        <div class="buttons-container col-12">
            @auth
                <a href="{{route('dashboard') }}">Dashboard</a>

                <form method="post" action="{{ route('logout') }}">
                    @csrf
                    <button type="submit" >Logout</button>
                </form>
            @else
                <a href="{{route('login')}}">Acceder</a>
                <a href="{{route('register')}}">Registrarse</a>
            @endauth
        </div>
    </div>--}}

    <div class="left-menu d-flex flex-column">
        <div class="top">
            <button class="close">
                <svg height="20.46px" viewBox="0 0 329.26933 329" width="20.46px" xmlns="http://www.w3.org/2000/svg" fill="#FFF">
                    <path d="m194.800781 164.769531 128.210938-128.214843c8.34375-8.339844 8.34375-21.824219 0-30.164063-8.339844-8.339844-21.824219-8.339844-30.164063 0l-128.214844 128.214844-128.210937-128.214844c-8.34375-8.339844-21.824219-8.339844-30.164063 0-8.34375 8.339844-8.34375 21.824219 0 30.164063l128.210938 128.214843-128.210938 128.214844c-8.34375 8.339844-8.34375 21.824219 0 30.164063 4.15625 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921875-2.089844 15.082031-6.25l128.210937-128.214844 128.214844 128.214844c4.160156 4.160156 9.621094 6.25 15.082032 6.25 5.460937 0 10.921874-2.089844 15.082031-6.25 8.34375-8.339844 8.34375-21.824219 0-30.164063zm0 0"/>
                </svg>
            </button>

            <img src="{{ asset('imgs/logo/logo-mini.png') }}" alt=""
                 class="logo"/>
        </div>

        <div class="publish">
            <a href="/">Buscar propiedades</a>
        </div>

        <div class="pages">
            <ul class="list-unstyled">
                @if (Request::is('dashboard/*') || Request::is('dashboard') )
                    <li>
                        <a class="perfil_link">
                            <svg id="perfil-del-usuario" xmlns="http://www.w3.org/2000/svg" width="21.3" height="27.365" viewBox="0 0 23.043 27.365">
                                <path id="Trazado_7696" data-name="Trazado 7696" d="M24.6,22.875H21.829a8.627,8.627,0,0,0-8.749-8.482,8.627,8.627,0,0,0-8.748,8.482H1.559A11.4,11.4,0,0,1,13.081,11.62,11.4,11.4,0,0,1,24.6,22.875Z" transform="translate(-1.559 4.49)" fill="#fff"/>
                                <path id="Trazado_7697" data-name="Trazado 7697" d="M11.989,15.209a7.6,7.6,0,1,1,7.606-7.6A7.614,7.614,0,0,1,11.989,15.209Zm0-12.436A4.832,4.832,0,1,0,16.822,7.6,4.836,4.836,0,0,0,11.989,2.773Z" transform="translate(-0.467)" fill="#fff"/>
                            </svg>
                            Mi perfil
                        </a>

                        <ul class="list-unstyled d-none perfil_links">
                            <li>
                                <a href="{{route('dashboard.anuncios')}}">
                                    <div class="dot">
                                        <span class="bg-white"></span>
                                        <span class="back"></span>
                                    </div>
                                    Información
                                </a>
                            </li>

                            <li>
                                <a href="/dashboard/#address">
                                    <div class="dot">
                                        <span class="bg-white"></span>
                                        <span class="back"></span>
                                    </div>
                                    Dirección
                                </a>
                            </li>

                            <li>
                                <a href="/dashboard/#password">
                                    <div class="dot">
                                        <span class="bg-white"></span>
                                        <span class="back"></span>
                                    </div>
                                    Contraseña
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{route('dashboard.anuncios')}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24.25" height="25.78" viewBox="0 0 161.233 182.88">
                                <g id="Grupo_1389" data-name="Grupo 1389" transform="translate(1312.838 1808.587)">
                                    <g id="Grupo_1306" data-name="Grupo 1306">
                                        <path id="Trazado_8000" data-name="Trazado 8000" d="M134.968,275.09V331.7a6.023,6.023,0,0,0,1.219,3.682c2.449,3.188,9.044,9.488,23.773,11.257a2.885,2.885,0,0,0,3.228-2.856V284.726a2.86,2.86,0,0,0-2.25-2.8c-4.428-.971-14.436-3.64-21.326-9.092A2.87,2.87,0,0,0,134.968,275.09Z" transform="translate(-1421.433 -2014.879)" fill="#fff"/>
                                        <path id="Trazado_8001" data-name="Trazado 8001" d="M217.513,275.021v57.747a2.827,2.827,0,0,1-.443,1.524c-1.592,2.508-7.843,10.369-24.6,12.367a2.847,2.847,0,0,1-3.176-2.82V284.714a2.825,2.825,0,0,1,2.216-2.765c4.424-.967,14.524-3.652,21.434-9.158A2.826,2.826,0,0,1,217.513,275.021Z" transform="translate(-1395.462 -2014.895)" fill="#fff"/>
                                        <path id="Trazado_8002" data-name="Trazado 8002" d="M167.089,318.651V291.2a4.593,4.593,0,0,0-3.821-4.542c-5.927-.989-17.477-3.819-25.4-11.5a4.613,4.613,0,0,1,.832-7.274,92.893,92.893,0,0,1,37.864-12.1c3.024-.293,6.18-.452,9.461-.445a98.027,98.027,0,0,1,11.346.695,112.393,112.393,0,0,1,38.851,12.423,4.61,4.61,0,0,1,1.2,7.275c-3.345,3.445-10.159,7.927-23.908,10.737a4.628,4.628,0,0,0-3.723,4.518v27.674a4.609,4.609,0,0,1-4.61,4.609H171.7A4.61,4.61,0,0,1,167.089,318.651Z" transform="translate(-1420.72 -2022.949)" fill="#fff"/>
                                        <path id="Trazado_8003" data-name="Trazado 8003" d="M129.5,268.523l-8.7,17.555a2.956,2.956,0,0,0,4.167,3.847l68.608-41.049a2.955,2.955,0,0,1,3.009-.015L267.615,290.4a2.955,2.955,0,0,0,4.183-3.771l-8.614-19.008a2.955,2.955,0,0,0-1.2-1.33l-65.366-38.27a2.953,2.953,0,0,0-3.057.041l-62.981,39.267A2.936,2.936,0,0,0,129.5,268.523Z" transform="translate(-1428.359 -2036.2)" fill="#fff"/>
                                        <path id="Trazado_8004" data-name="Trazado 8004" d="M227.71,303.28a1.319,1.319,0,0,0-1.958,1.16l.127,13.074a1.8,1.8,0,0,0,.853,1.559c3.277,2.032,12.066,11.248,5.874,21.131-7.637,12.191-33.715,10.453-33.715,10.453V340.2a1.009,1.009,0,0,0-1.743-.693l-16.565,15.852a1.668,1.668,0,0,0-.031,2.26l16.907,16.5a.82.82,0,0,0,1.432-.547v-10.85s29.06,3.716,42.762-11.924C252.608,338.3,251.149,315.9,227.71,303.28Z" transform="translate(-1399.844 -2000.104)" fill="#fff"/>
                                        <path id="Trazado_8005" data-name="Trazado 8005" d="M200.656,303.28a1.319,1.319,0,0,1,1.958,1.16l-.127,13.074a1.8,1.8,0,0,1-.853,1.559c-3.277,2.032-12.066,11.248-5.874,21.131,7.637,12.191,33.715,10.453,33.715,10.453V340.2a1.009,1.009,0,0,1,1.743-.693l16.565,15.852a1.668,1.668,0,0,1,.031,2.26l-16.907,16.5a.82.82,0,0,1-1.432-.547v-10.85s-29.06,3.716-42.762-11.924C175.757,338.3,177.216,315.9,200.656,303.28Z" transform="translate(-1492.963 -2000.104)" fill="#fff"/>
                                    </g>
                                </g>
                            </svg>
                            Mis anuncios
                        </a>
                    </li>
                    <li>
                        <a href="{{route('dashboard.favoritos')}}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24.25" height="21.58" viewBox="0 0 23.063 20.519" fill="#FFF">
                                <path class="a" d="M21.234,2.008A6.2,6.2,0,0,0,16.62,0,5.8,5.8,0,0,0,13,1.251a7.414,7.414,0,0,0-1.465,1.53,7.41,7.41,0,0,0-1.465-1.53A5.8,5.8,0,0,0,6.443,0,6.2,6.2,0,0,0,1.829,2.008,7.209,7.209,0,0,0,0,6.931,8.584,8.584,0,0,0,2.287,12.55a48.774,48.774,0,0,0,5.726,5.374c.793.676,1.693,1.443,2.627,2.259a1.354,1.354,0,0,0,1.783,0c.934-.817,1.834-1.583,2.627-2.26a48.752,48.752,0,0,0,5.726-5.374,8.583,8.583,0,0,0,2.287-5.619A7.208,7.208,0,0,0,21.234,2.008Zm0,0"/>
                            </svg>
                            Mis favoritos
                        </a>
                    </li>
                @else
                    <li>
                        <a href="/como_empezar">Como funciona</a>
                    </li>
                    <li>
                        <a href="{{route('crear_tour')}}">Crear tour</a>
                    </li>
                    <li>
                        <a href="/home/#contact" >Contacto</a>
                    </li>
                @endif
            </ul>
        </div>

        <div class="bottom pt-4">
            <div class="social-media">
                <div class="d-flex justify-content-center py-3 position-relative">
                    <a class="d-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="37.266" height="37.266" viewBox="0 0 37.266 37.266" fill="#551A9D">
                            <g id="Grupo_1162" data-name="Grupo 1162" transform="translate(-2016.793 -2349)">
                                <path id="facebook-4" d="M37.266,18.633A18.633,18.633,0,1,0,18.633,37.266c.109,0,.218,0,.328-.007V22.76h-4V18.094h4V14.659c0-3.981,2.431-6.15,5.983-6.15a32.519,32.519,0,0,1,3.588.182v4.163H26.086c-1.929,0-2.307.917-2.307,2.264v2.97H28.4l-.6,4.665H23.779V36.545A18.638,18.638,0,0,0,37.266,18.633Z" transform="translate(2016.793 2349)"/>
                            </g>
                        </svg>
                    </a>

                    <a class="d-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="37.266" height="37.266" viewBox="0 0 37.266 37.266" fill="#551A9D">
                            <g id="Grupo_1161" data-name="Grupo 1161" transform="translate(-2116.48 -2349)">
                                <g id="instagram-3" transform="translate(2116.48 2349)">
                                    <path id="Trazado_7659" data-name="Trazado 7659" d="M214.133,210.566A3.566,3.566,0,1,1,210.566,207,3.566,3.566,0,0,1,214.133,210.566Zm0,0" transform="translate(-191.933 -191.934)"/>
                                    <path id="Trazado_7660" data-name="Trazado 7660" d="M152.6,137.95a3.543,3.543,0,0,0-2.031-2.031,5.92,5.92,0,0,0-1.987-.368c-1.128-.051-1.467-.063-4.324-.063s-3.2.011-4.324.062a5.924,5.924,0,0,0-1.987.368,3.545,3.545,0,0,0-2.031,2.031,5.922,5.922,0,0,0-.368,1.987c-.051,1.128-.063,1.466-.063,4.324s.011,3.2.063,4.324a5.92,5.92,0,0,0,.368,1.986,3.543,3.543,0,0,0,2.031,2.031,5.913,5.913,0,0,0,1.987.368c1.128.051,1.466.062,4.323.062s3.2-.011,4.324-.062a5.913,5.913,0,0,0,1.987-.368,3.543,3.543,0,0,0,2.031-2.031,5.925,5.925,0,0,0,.368-1.986c.051-1.128.062-1.467.062-4.324s-.011-3.2-.062-4.324A5.913,5.913,0,0,0,152.6,137.95Zm-8.341,11.8a5.494,5.494,0,1,1,5.494-5.494A5.494,5.494,0,0,1,144.256,149.754Zm5.711-9.921a1.284,1.284,0,1,1,1.284-1.284A1.284,1.284,0,0,1,149.967,139.832Zm0,0" transform="translate(-125.623 -125.627)"/>
                                    <path id="Trazado_7661" data-name="Trazado 7661" d="M18.633,0A18.633,18.633,0,1,0,37.266,18.633,18.635,18.635,0,0,0,18.633,0ZM29.268,23.044a7.85,7.85,0,0,1-.5,2.6,5.471,5.471,0,0,1-3.129,3.129,7.855,7.855,0,0,1-2.6.5c-1.141.052-1.505.065-4.411.065s-3.27-.013-4.411-.065a7.855,7.855,0,0,1-2.6-.5A5.471,5.471,0,0,1,8.5,25.641a7.848,7.848,0,0,1-.5-2.6c-.053-1.141-.065-1.506-.065-4.411s.012-3.27.065-4.411a7.851,7.851,0,0,1,.5-2.6A5.475,5.475,0,0,1,11.624,8.5a7.857,7.857,0,0,1,2.6-.5c1.141-.052,1.505-.065,4.411-.065s3.27.013,4.411.065a7.859,7.859,0,0,1,2.6.5,5.472,5.472,0,0,1,3.129,3.129,7.849,7.849,0,0,1,.5,2.6c.052,1.141.064,1.505.064,4.411S29.32,21.9,29.268,23.044Zm0,0" transform="translate(0 0)"/>
                                </g>
                            </g>
                        </svg>
                    </a>

                    <a class="d-block">
                        <svg xmlns="http://www.w3.org/2000/svg" width="37.266" height="37.266" viewBox="0 0 37.266 37.266" fill="#551A9D">
                            <g id="Grupo_1160" data-name="Grupo 1160" transform="translate(-2216.166 -2349)">
                                <path id="tik-tok" d="M18.633,0A18.633,18.633,0,1,0,37.266,18.633,18.635,18.635,0,0,0,18.633,0Zm9.348,14.257v2.523a8.8,8.8,0,0,1-5.387-1.831l.018,7.766a6.5,6.5,0,0,1-1.951,4.629A6.641,6.641,0,0,1,16.95,29.2a6.838,6.838,0,0,1-1,.074,6.663,6.663,0,0,1-4.084-1.381,6.774,6.774,0,0,1-.628-.548,6.529,6.529,0,0,1-.295-9A6.662,6.662,0,0,1,15.948,16.1a6.838,6.838,0,0,1,1,.074V19.7a3.141,3.141,0,1,0,2.16,2.984l0-5.2V7.993h3.474a5.378,5.378,0,0,0,5.381,5.328h.01v.936Z" transform="translate(2216.166 2349)"/>
                            </g>
                        </svg>
                    </a>

                    <a class="mr-0 d-block">
                        <svg id="youtube-3" xmlns="http://www.w3.org/2000/svg" width="37.266" height="37.266" viewBox="0 0 37.266 37.266" fill="#551A9D">
                            <path id="Trazado_7662" data-name="Trazado 7662" d="M224.113,215.021l6.061-3.491-6.061-3.491Zm0,0" transform="translate(-207.801 -192.897)"/>
                            <path id="Trazado_7663" data-name="Trazado 7663" d="M18.633,0A18.633,18.633,0,1,0,37.266,18.633,18.635,18.635,0,0,0,18.633,0ZM30.276,18.652a30.753,30.753,0,0,1-.479,5.6,2.918,2.918,0,0,1-2.052,2.052c-1.822.48-9.111.48-9.111.48s-7.27,0-9.111-.5A2.918,2.918,0,0,1,7.47,24.234a30.618,30.618,0,0,1-.48-5.6,30.733,30.733,0,0,1,.48-5.6,2.977,2.977,0,0,1,2.052-2.072c1.822-.48,9.111-.48,9.111-.48s7.289,0,9.111.5A2.918,2.918,0,0,1,29.8,13.032,29.183,29.183,0,0,1,30.276,18.652Zm0,0" transform="translate(0 0)"/>
                        </svg>
                    </a>
                </div>
            </div>

            <div class="links">
                <ul class="list-unstyled">
                    <li>
                        <a href="/terminos_condiciones_uso" title="terminos y condiciones de uso">
                            <div class="dot">
                                <span class="bg-white"></span>
                                <span class="back"></span>
                            </div>
                            Términos y condiciones
                        </a>
                    </li>

                    <li>
                        <a href="politicas_privacidad" title="politicas de privacidad">
                            <div class="dot">
                                <span class="bg-white"></span>
                                <span class="back"></span>
                            </div>
                            Política y privacidad
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="collapse search pt-4 pb-4" id="searchDropdown">
        <div class="col-12 d-flex flex-wrap mx-md-auto">
            <div class="buttons col-12 col-md-5 mb-4 d-flex flex-nowrap mb-md-0">
                <button class="buy active" objt="vender">Comprar</button>
                <button class="rent" objt="alquilar">Alquilar</button>
            </div>
            <div class="col-12 col-md-7 d-flex flex-nowrap justify-content-between">
                <div class="tipo w-50">
                    <div class="dropdown-toggle pl-2" id="selectTipoButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <p class="m-0">Tipo de inmueble</p>
                        <p class="m-0 overflow-hidden mt-1 value">Qué buscas</p>
                    </div>

                    <div class="dropdown-menu px-2" aria-labelledby="selectTipoButton">
                        <div class="options mt-2">
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Piso de obra nueva</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Casa o chalet de obra nueva</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Piso</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Casa o chalet</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Casas rústicas</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Dúplex</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Ático</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Oficina</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Local o nave</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Terreno</button>
                        </div>
                    </div>
                </div>

                <div class="lugar w-50 ">
                    <div class="dropdown-toggle pl-2" id="selectLugarButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <p class="m-0">Ubicación</p>
                        <input class="m-0 overflow-hidden mt-1" placeholder="Dónde buscas"/>
                    </div>

                    <div wire:ignore class="dropdown-menu px-2" aria-labelledby="selectLugarButton">
                        <div class="options mt-2">
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Albacete</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Alicante/Alacant</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Almería</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Araba/Álava</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Asturias</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Ávila</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Badajoz</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">"Balears, Illes"</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Barcelona</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Bizkaia</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Burgos</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Cáceres</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Cádiz</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Cantabria</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Castellón/Castelló</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Ciudad Real</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Córdoba"</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">"Coruña, A"</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Cuenca</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Gipuzkoa</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Girona</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Granada</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Burgos</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Guadalajara</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Huelva</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Huesca</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Jaén</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">León</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Lleida</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Lugo</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Madrid</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Málaga</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Murcia</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Navarra</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Ourense</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Palencia</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">"Palmas, Las"</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Pontevedra</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">"Rioja, La"</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Salamanca</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Santa Cruz de Tenerife</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Segovia</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Sevilla</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Soria</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Tarragona</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Teruel</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Toledo</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Valencia/València</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Valladolid</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Zamora</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Zaragoza</button>
                            <button class="dropdown-item ml-2 px-1 my-2 py-1">Ceuta</button>
                        </div>
                    </div>
                </div>

                <div class="lens">
                    <button class="px-0">
                        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-search" fill="#fff"
                             xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd"
                                  d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
                            <path fill-rule="evenodd"
                                  d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
</header>
