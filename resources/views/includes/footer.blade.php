<footer class="page-footer mt-auto">
    <div class="social-media mb-3">
        <div class="d-flex justify-content-center py-3 position-relative">
            <a class="d-block">
                <svg xmlns="http://www.w3.org/2000/svg" width="37.266" height="37.266" viewBox="0 0 37.266 37.266">
                    <g id="Grupo_1162" data-name="Grupo 1162" transform="translate(-2016.793 -2349)">
                        <path id="facebook-4" d="M37.266,18.633A18.633,18.633,0,1,0,18.633,37.266c.109,0,.218,0,.328-.007V22.76h-4V18.094h4V14.659c0-3.981,2.431-6.15,5.983-6.15a32.519,32.519,0,0,1,3.588.182v4.163H26.086c-1.929,0-2.307.917-2.307,2.264v2.97H28.4l-.6,4.665H23.779V36.545A18.638,18.638,0,0,0,37.266,18.633Z" transform="translate(2016.793 2349)"/>
                    </g>
                </svg>
            </a>

            <a class="d-block">
                <svg xmlns="http://www.w3.org/2000/svg" width="37.266" height="37.266" viewBox="0 0 37.266 37.266">
                    <g id="Grupo_1161" data-name="Grupo 1161" transform="translate(-2116.48 -2349)">
                        <g id="instagram-3" transform="translate(2116.48 2349)">
                            <path id="Trazado_7659" data-name="Trazado 7659" d="M214.133,210.566A3.566,3.566,0,1,1,210.566,207,3.566,3.566,0,0,1,214.133,210.566Zm0,0" transform="translate(-191.933 -191.934)"/>
                            <path id="Trazado_7660" data-name="Trazado 7660" d="M152.6,137.95a3.543,3.543,0,0,0-2.031-2.031,5.92,5.92,0,0,0-1.987-.368c-1.128-.051-1.467-.063-4.324-.063s-3.2.011-4.324.062a5.924,5.924,0,0,0-1.987.368,3.545,3.545,0,0,0-2.031,2.031,5.922,5.922,0,0,0-.368,1.987c-.051,1.128-.063,1.466-.063,4.324s.011,3.2.063,4.324a5.92,5.92,0,0,0,.368,1.986,3.543,3.543,0,0,0,2.031,2.031,5.913,5.913,0,0,0,1.987.368c1.128.051,1.466.062,4.323.062s3.2-.011,4.324-.062a5.913,5.913,0,0,0,1.987-.368,3.543,3.543,0,0,0,2.031-2.031,5.925,5.925,0,0,0,.368-1.986c.051-1.128.062-1.467.062-4.324s-.011-3.2-.062-4.324A5.913,5.913,0,0,0,152.6,137.95Zm-8.341,11.8a5.494,5.494,0,1,1,5.494-5.494A5.494,5.494,0,0,1,144.256,149.754Zm5.711-9.921a1.284,1.284,0,1,1,1.284-1.284A1.284,1.284,0,0,1,149.967,139.832Zm0,0" transform="translate(-125.623 -125.627)"/>
                            <path id="Trazado_7661" data-name="Trazado 7661" d="M18.633,0A18.633,18.633,0,1,0,37.266,18.633,18.635,18.635,0,0,0,18.633,0ZM29.268,23.044a7.85,7.85,0,0,1-.5,2.6,5.471,5.471,0,0,1-3.129,3.129,7.855,7.855,0,0,1-2.6.5c-1.141.052-1.505.065-4.411.065s-3.27-.013-4.411-.065a7.855,7.855,0,0,1-2.6-.5A5.471,5.471,0,0,1,8.5,25.641a7.848,7.848,0,0,1-.5-2.6c-.053-1.141-.065-1.506-.065-4.411s.012-3.27.065-4.411a7.851,7.851,0,0,1,.5-2.6A5.475,5.475,0,0,1,11.624,8.5a7.857,7.857,0,0,1,2.6-.5c1.141-.052,1.505-.065,4.411-.065s3.27.013,4.411.065a7.859,7.859,0,0,1,2.6.5,5.472,5.472,0,0,1,3.129,3.129,7.849,7.849,0,0,1,.5,2.6c.052,1.141.064,1.505.064,4.411S29.32,21.9,29.268,23.044Zm0,0" transform="translate(0 0)"/>
                        </g>
                    </g>
                </svg>
            </a>

            <a class="d-block">
                <svg xmlns="http://www.w3.org/2000/svg" width="37.266" height="37.266" viewBox="0 0 37.266 37.266">
                    <g id="Grupo_1160" data-name="Grupo 1160" transform="translate(-2216.166 -2349)">
                        <path id="tik-tok" d="M18.633,0A18.633,18.633,0,1,0,37.266,18.633,18.635,18.635,0,0,0,18.633,0Zm9.348,14.257v2.523a8.8,8.8,0,0,1-5.387-1.831l.018,7.766a6.5,6.5,0,0,1-1.951,4.629A6.641,6.641,0,0,1,16.95,29.2a6.838,6.838,0,0,1-1,.074,6.663,6.663,0,0,1-4.084-1.381,6.774,6.774,0,0,1-.628-.548,6.529,6.529,0,0,1-.295-9A6.662,6.662,0,0,1,15.948,16.1a6.838,6.838,0,0,1,1,.074V19.7a3.141,3.141,0,1,0,2.16,2.984l0-5.2V7.993h3.474a5.378,5.378,0,0,0,5.381,5.328h.01v.936Z" transform="translate(2216.166 2349)"/>
                    </g>
                </svg>
            </a>

            <a class="mr-0 d-block">

                <svg id="youtube-3" xmlns="http://www.w3.org/2000/svg" width="37.266" height="37.266" viewBox="0 0 37.266 37.266">
                    <path id="Trazado_7662" data-name="Trazado 7662" d="M224.113,215.021l6.061-3.491-6.061-3.491Zm0,0" transform="translate(-207.801 -192.897)"/>
                    <path id="Trazado_7663" data-name="Trazado 7663" d="M18.633,0A18.633,18.633,0,1,0,37.266,18.633,18.635,18.635,0,0,0,18.633,0ZM30.276,18.652a30.753,30.753,0,0,1-.479,5.6,2.918,2.918,0,0,1-2.052,2.052c-1.822.48-9.111.48-9.111.48s-7.27,0-9.111-.5A2.918,2.918,0,0,1,7.47,24.234a30.618,30.618,0,0,1-.48-5.6,30.733,30.733,0,0,1,.48-5.6,2.977,2.977,0,0,1,2.052-2.072c1.822-.48,9.111-.48,9.111-.48s7.289,0,9.111.5A2.918,2.918,0,0,1,29.8,13.032,29.183,29.183,0,0,1,30.276,18.652Zm0,0" transform="translate(0 0)"/>
                </svg>
            </a>
        </div>
    </div>

    <div class="bottom pt-5 pb-2 pb-xl-4">
        <div class="container">
            <div class="row">
                <div class="links col-12 col-md-6 col-lg-5 col-xl-4 ml-lg-auto order-md-1">
                    <div class="list d-flex flex-wrap">
                        <div class="link list-item col-5 mb-2">
                            <a href="/como_empezar" title="">
                                <div class="dot">
                                    <span class="bg-white"></span>
                                    <span class="back"></span>
                                </div>
                                Como funciona
                            </a>
                        </div>

                        <div class="link list-item col-7 mb-2 pl-4">
                            <a href="/crear_tour" title="">
                                <div class="dot">
                                    <span class="bg-white"></span>
                                    <span class="back"></span>
                                </div>
                                Crear tour
                            </a>
                        </div>

                        <div class="link list-item col-5 mb-2">
                            <a href="/anuncios" title="">
                                <div class="dot">
                                    <span></span>
                                    <span class="back"></span>
                                </div>
                                Anuncios
                            </a>
                        </div>

                        <div class="link list-item col-7 mb-2 pl-4">
                            <a href="/politicas_privacidad" title="">
                                <div class="dot">
                                    <span class="bg-white"></span>
                                    <span class="back"></span>
                                </div>
                                Política de privacidad
                            </a>
                        </div>

                        <div class="link list-item col-5 mb-2">
                            <a href="#" title="">
                                <div class="dot">
                                    <span class="bg-white"></span>
                                    <span class="back"></span>
                                </div>
                                Política de Cookies</a>
                        </div>

                        <div class="link list-item col-7 mb-2 pl-4">
                            <a href="/terminos_condiciones_uso" title="">
                                <div class="dot">
                                    <span class="bg-white"></span>
                                    <span class="back"></span>
                                </div>
                                Condiciones de uso</a>
                        </div>
                    </div>
                </div>

                <div class="newsletter col-12 col-md-6 col-xl-4 order-md-3 order-xl-2 mx-md-auto mt-4 my-xl-auto">
                    <form id="keep-updated-form" onsubmit="return false;">

                        <div class="col-12 form-group position-relative d-flex flex-nowrap">
                            <label for="newsletter-email" class="d-none">Email address</label>
                            <input type="email" class="form-control pl-4" id="newsletter-email" placeholder="Email" required>
                            <input type="submit" value="Send!" class="position-absolute">
                        </div>
                    </form>
                </div>

                <div class="details col-12 col-md-6 col-lg-5 col-xl-4 mr-lg-auto order-md-2 order-xl-3 mt-4 mt-md-0 text-center">
                    <p class="logo"><img src="{{ asset('imgs/logo/logo.png') }}" alt="" class="img-fluid"/></p>
                    <p class="copyright">Copyright 2020 - todos los derechos reservados a <span class="d-block">Grupo empresarial PL</span></p>
                </div>
            </div>
        </div>
    </div>
</footer>
