<div class="modal fade" id="solicitudEnviadaModal" tabindex="-1" role="dialog" aria-labelledby="solicitudEnviadaModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close position-absolute d-none" data-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24.699 24.699">
                        <g id="Grupo_1390" data-name="Grupo 1390" transform="translate(-281.379 174.621)">
                            <line id="Línea_199" data-name="Línea 199" y1="20.456" x2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                            <line id="Línea_200" data-name="Línea 200" x2="20.456" y2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                        </g>
                    </svg>
                </button>
                <p class="c-dpink t-b text-center mb-4">
                    Su solicitud de contacto ha sido enviada.
                </p>

                <p class="text-center mb-0">
                    <button type="button" class="b-dpink" data-dismiss="modal">Entendido</button>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="solicitudTourEnviadaModal" tabindex="-1" role="dialog" aria-labelledby="solicitudTourEnviadaModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close position-absolute d-none" data-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24.699 24.699">
                        <g id="Grupo_1390" data-name="Grupo 1390" transform="translate(-281.379 174.621)">
                            <line id="Línea_199" data-name="Línea 199" y1="20.456" x2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                            <line id="Línea_200" data-name="Línea 200" x2="20.456" y2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                        </g>
                    </svg>
                </button>
                <p class="c-dpink t-b text-center mb-4">
                    Su solicitud de tour ha sido enviada.
                </p>

                <p class="text-center mb-0">
                    <button type="button" class="b-dpink" data-dismiss="modal">Entendido</button>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="favoriteErrorModal" tabindex="-1" role="dialog" aria-labelledby="favoriteErrorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close position-absolute d-flex justify-content-center align-items-center" data-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24.699 24.699">
                        <g id="Grupo_1390" data-name="Grupo 1390" transform="translate(-281.379 174.621)">
                            <line id="Línea_199" data-name="Línea 199" y1="20.456" x2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                            <line id="Línea_200" data-name="Línea 200" x2="20.456" y2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                        </g>
                    </svg>
                </button>

                <p class="c-dpink t-b text-center mb-4">
                    Primero deber <a href="{{ route('login') }}">acceder</a> para guardar favoritos.
                </p>

                <p  class="d-flex justify-content-center">
                    <a href="{{ route('register') }}" class="b-dpink d-flex align-items-center justify-content-center w-100">Crear cuenta</a>
                </p>


            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="shareModal" tabindex="-1" role="dialog" aria-labelledby="shareModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close position-absolute d-flex justify-content-center align-items-center" data-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 24.699 24.699">
                        <g id="Grupo_1390" data-name="Grupo 1390" transform="translate(-281.379 174.621)">
                            <line id="Línea_199" data-name="Línea 199" y1="20.456" x2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                            <line id="Línea_200" data-name="Línea 200" x2="20.456" y2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                        </g>
                    </svg>
                </button>

                <p class="c-black t-b mb-4 mt-3 mr-4 c-dpink">
                    Comparte este alojamiento con tus amigos y familiares
                </p>

                <p class="social fb">
                    <a href="" target="_blank" rel="noopener noreferrer" class="d-flex align-items-center c-dpink w-100 ">
                        <img src="/icons/share/facebook.svg" alt="facebook share logo" class="mr-2">Facebook
                    </a>
                </p>

                <p class="social tw">
                    <a href="" target="_blank" rel="noopener noreferrer" class="d-flex align-items-center c-dpink w-100">
                        <img src="/icons/share/twitter.svg" alt="twitter share logo" class="mr-2">Twitter
                    </a>
                </p>

                <p class="social wt">
                    <a href="" target="_blank" rel="noopener noreferrer" class="d-flex align-items-center c-dpink w-100">
                        <img src="/icons/share/whatsapp.svg" alt="whatsapp share logo" class="mr-2">Whatsapp
                    </a>
                </p>

                <p class="social gm">
                    <a href="" target="_blank" rel="noopener noreferrer" class="d-flex align-items-center c-dpink w-100">
                        <img src="/icons/share/email.svg" alt="email share logo" class="mr-2">Gmail
                    </a>
                </p>

                <p class="social">
                    <a class="d-flex align-items-center w-100 share-clipboard cursor-pointer c-dpink" url="">
                        <img src="/icons/share/link.svg" alt="link share logo" class="mr-2">Copiar enlace
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="newsletterEnviadaModal" tabindex="-1" role="dialog" aria-labelledby="newsletterEnviadaModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close position-absolute d-none" data-dismiss="modal" aria-label="Close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24.699 24.699">
                        <g id="Grupo_1390" data-name="Grupo 1390" transform="translate(-281.379 174.621)">
                            <line id="Línea_199" data-name="Línea 199" y1="20.456" x2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                            <line id="Línea_200" data-name="Línea 200" x2="20.456" y2="20.456" transform="translate(283.5 -172.5)" fill="none" stroke="#fff" stroke-linecap="round" stroke-width="3"/>
                        </g>
                    </svg>
                </button>
                <p class="c-dpink t-b text-center mb-4">
                    Gracias por suscribirse a nuestro NewsLetter, le mantendremos al día.
                </p>

                <p class="text-center mb-0">
                    <button type="button" class="b-dpink" data-dismiss="modal">Entendido</button>
                </p>
            </div>
        </div>
    </div>
</div>
