<div
    style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';background-color:#ffffff;color:#000000;height:100%;line-height:1.4;margin:0;padding:0;width:100%!important">
    <table width="100%" cellpadding="0" cellspacing="0" role="presentation"
           style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';background-color:#ffffff;margin:0;padding:0;width:100%">
        <tbody>
        <tr>
            <td align="center"
                style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol'">
                <table width="100%" cellpadding="0" cellspacing="0" role="presentation"
                       style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';margin:0;padding:0;width:100%">
                    <tbody>
                    <tr>
                        <td style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';padding:25px 0 51px;text-align:center">
                            <a href="http://localhost"
                               style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';color:#3d4852;font-size:19px;font-weight:bold;text-decoration:none;display:inline-block"
                               target="_blank"
                               data-saferedirecturl="https://www.google.com/url?q=http://localhost&amp;source=gmail&amp;ust=1608133779993000&amp;usg=AFQjCNGaarIUYy5xG3NZYGrBFKvrJ9xyYg">
                                <img
                                    src="https://ci6.googleusercontent.com/proxy/8OPyYu6tnuh4cZtyGMIA1Sy48gL-BKcGQMdNuY7v62d296BDK24Y22kDJVrtXzliHLFhlckERjsG_w=s0-d-e1-ft#https://wikihomes.es/imgs/logo/logo_morado.png"
                                    alt="Wikihomes Logo"
                                    style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';max-width:100%;border:none;height:75px;width:75px"
                                    class="CToWUd">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" cellpadding="0" cellspacing="0"
                            style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';background-color:#ffffff;margin:0;padding:0;width:100%">
                            <table align="center" width="570" cellpadding="0" cellspacing="0" role="presentation"
                                   style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';background-color:#f8f8f8;border-radius:24px;margin:0 auto;width:852px;padding:20px 36px 53px 34px;">
                                <tbody>
                                <tr>
                                    <td style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';max-width:100vw; padding:27px 34px 27px 48px; width: 466px; background: #FFFFFF 0% 0% no-repeat padding-box; border-radius: 25px;">
                                        <p style="font-size: 16px;">
                                            <b style="margin-right: 14px; color: #0F0F0F;">{{ $details['nombre'] }}</b>
                                            <span style="color: #AC5CFF;">{{ $details['email'] }}</span>
                                        </p>
                                        <h4 style="font-size: 24px; height: 29px; color: #551A9D; margin-top: 0px; margin-bottom: 10px; font-weight: bold;">
                                            @if(!$details['como_empezar'])
                                                Solicitud de contacto
                                            @else
                                                Solicitud de tour
                                            @endif
                                        </h4>
                                        <p style="font-size: 14px; color: #0F0F0F;">
                                            {{ $details['mensaje'] }}
                                        </p>
                                    </td>

                                    <td style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';max-width:100vw;padding:26px">
                                        @if(!$details['como_empezar'])
                                        <a href="{{ $details['url']}}"
                                           style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';color:#6A6A6A;font-size:16px;font-weight:bold;text-decoration:none;display:inline-block"
                                           target="_blank"
                                           data-saferedirecturl="https://www.google.com/url?q=http://localhost&amp;source=gmail&amp;ust=1608133779993000&amp;usg=AFQjCNGaarIUYy5xG3NZYGrBFKvrJ9xyYg">
                                            <img
                                                src="{{ $details['foto_portada']}}"
                                                alt="Anuncio imagen de portada"
                                                style="box-sizing:border-box;max-width:100%;border:none;width: 281px;height: 149px; border-radius: 25px;"
                                                class="CToWUd">
                                            <p style="margin-left: 18px; font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';color:#6A6A6A;font-size:16px;font-weight: normal;">{{ $details['direccion']}}</p>
                                        </a>
                                        @else
                                            <img
                                                src="https://ci6.googleusercontent.com/proxy/8OPyYu6tnuh4cZtyGMIA1Sy48gL-BKcGQMdNuY7v62d296BDK24Y22kDJVrtXzliHLFhlckERjsG_w=s0-d-e1-ft#https://wikihomes.es/imgs/como_empezar/360-degrees.svg"
                                                alt="Wikihomes Logo"
                                                style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';max-width:100%;border:none;width: 159px;height: 146px;"
                                                class="CToWUd">
                                        @endif
                                    </td>
                                </tr>
                                <tr width="100%" style="padding: 30px;">
                                    <td>
                                        <img style="width: 63px; height: 71px;">
                                        <span style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol'; margin-left: 43px">
                                        La misión de Airbnb es ayudar a establecer vínculos entre personas y hacer de este mundo un lugar más abierto e inclusivo. En otras palabras: construir un mundo en el que la gente pueda sentirse como en casa donde vaya. Nuestra comunidad se basa en la confianza, y para ganárnosla es fundamental que todos los miembros tengan claro cómo utilizamos sus datos y protegemos su derecho a la privacidad.
                                        </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol'">
                            <table align="center" width="570" cellpadding="0" cellspacing="0" role="presentation"
                                   style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';margin:0 auto;padding:0;text-align:center;width:570px">
                                <tbody>
                                <tr>
                                    <td align="center"
                                        style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';max-width:100vw;padding:30px 50px">
                                        <p style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';line-height:1.5em;margin-top:0;color:#b0adc5;font-size:12px;text-align:center">
                                            © 2020 Wikihomes. Todos los derechos reservados.</p>

                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="yj6qo"></div>
    <div class="adL">
    </div>
</div>
