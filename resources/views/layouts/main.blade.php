<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="@yield('description', 'Wikihomes, plataforma inmobilaria de tours virtuales')">

    <title>@yield('title', 'Wikihomes')</title>

    <link rel="shortcut icon" href="{{{ asset('icons/favicon.png') }}}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @livewireStyles
    @yield('styles')

    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.0/dist/alpine.js" defer></script>
</head>
<body class="antialiased">
    <!-- Page Heading -->
    @include('includes.header')

    <!-- Page Content -->
    {{ $slot }}

    <!-- Page Footer -->
    @include('includes.footer')

    <div class="static-background d-none">
    </div>

    <div class="static-background-grey d-none">
    </div>

    @include('includes.modals')
@stack('modals')

@livewireScripts

<!-- Scripts -->
<script src="{{ asset('js/app.js')}}"></script>
@yield('scripts')
<script type="text/javascript">
    $("#keep-updated-form").submit(function (event) {
        $.post("/subscribeNewsletter", {email: $("#newsletter-email").val(), "_token": "{{ csrf_token() }}"}).done(function( data ) {
            $("#newsletterEnviadaModal").modal('show');
            $("#newsletter-email").val("");
        });
    });
</script>
</body>

</html>
