<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NewsLetterController;
use App\Http\Livewire\Perfil;
use App\Http\Livewire\CrearTour;
use App\Http\Livewire\ComoEmpezar;
use App\Http\Livewire\Dashboard\Favoritos;
use App\Http\Livewire\Dashboard\Anuncios;
use App\Http\Livewire\Dashboard\Anuncio;
use App\Http\Livewire\Anuncios\Main;
use App\Http\Livewire\Anuncios\View;
use App\Http\Livewire\Home;
use App\Http\Livewire\PoliticaPrivacidad;
use App\Http\Livewire\TerminosUso;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\FacebookController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Home
/*Route::get('', Home::class)
    ->name('home');

Route::get('/', Home::class)
    ->name('home');*/

Route::get('/politicas_privacidad', PoliticaPrivacidad::class)
    ->name('politicas_privacidad');

Route::get('/terminos_condiciones_uso', TerminosUso::class)
    ->name('terminos_condiciones_uso');

//Email
Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware(['auth'])->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/dashboard/perfil');
})->middleware(['signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('status', 'verification-link-sent');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

//Google auth
Route::get('auth/google', [GoogleController::class, 'redirectToGoogle'])->name('google.auth');
Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);

//Facebook auth
Route::get('auth/facebook', [FacebookController::class, 'redirectToFacebook'])->name('facebook.auth');
Route::get('auth/facebook/callback', [FacebookController::class, 'handleFacebookCallback']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::group(['middleware' => ['auth', 'verified']], function () {
    // User & Profile...
    Route::get('/dashboard', Perfil::class)
        ->name('dashboard');

    Route::get('/dashboard/perfil', Perfil::class)
        ->name('perfil.show');

    Route::get('/dashboard/anuncios', Anuncios::class)
        ->name('dashboard.anuncios');

    Route::get('/dashboard/anuncio/{id}', Anuncio::class)
        ->name('dashboard.anuncio');

    Route::get('/dashboard/favoritos', Favoritos::class)
        ->name('dashboard.favoritos');
});

//Anuncios
Route::get('', Main::class)
    ->name('anuncios');

Route::get('/', Main::class)
    ->name('anuncios');

Route::get('/home', Main::class)
    ->name('anuncios');

//Anuncio
Route::get('/anuncio/{url}', View::class)
    ->name('anuncio');

//Crear Tour
Route::get('/crear_tour', CrearTour::class)
    ->name('crear_tour');

//Como Empezar
Route::get('/como_empezar', ComoEmpezar::class)
    ->name('como_empezar');


//Register in newsletter
Route::post('subscribeNewsletter' , [NewsLetterController::class, 'register']);
