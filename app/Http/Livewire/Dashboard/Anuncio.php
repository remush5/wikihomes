<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\Ciudad;
use Livewire\Component;
use App\Rules\ArrayAtLeastOneRequired;
use App\Models\Anuncio as AnuncioModel;
use App\Models\Caracteristica;
use App\Models\Provincia;
use Geocoder;
use Auth;
use \Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use ImageCompressor;

class Anuncio extends Component
{
    use WithFileUploads;

    public $anuncio;
    public $last_caracteristicas;
    public $interaccion = 'crear';
    public $parte = 1;

    public $objetivo;
    public $tipo;
    public $tour_url;
    public $primer_precio;
    public $precio;
    public $habitaciones;
    public $baños;
    public $metros_cuadrados;
    public $año;
    public $estado;
    public $amueblado;
    public $foto_portada;

    public $image_name = "Examinar";

    public $provincia;
    public $ciudad;
    public $codigo_postal;
    public $direccion;
    public $descripcion;

    public $provincias;
    public $ciudades;

    public $a_caracteristicas = [
        'aire_acondicionado',
        'terraza',
        'despacho',
        'armarios_empotrados',
        'ascensor',
        'garaje',
        'jardin',
        'piscina',
        'pista_de_padel',
        'zona_de_juego',
    ];

    public $caracteristicas = [
        'aire_acondicionado' => false,
        'terraza' => false,
        'despacho' => false,
        'armarios_empotrados' => false,
        'ascensor' => false,
        'garaje' => false,
        'jardin' => false,
        'piscina' => false,
        'pista_de_padel' => false,
        'zona_de_juego' => false,
    ];

    public function mount ($id) {
        if ( $id == 'crear') {
            $this->interaccion = 'crear';
            $this->anuncio = new AnuncioModel();
            $this->last_caracteristicas = new Caracteristica();
        } else {
            $this->anuncio = AnuncioModel::find($id);
            $this->last_caracteristicas = Caracteristica::where('source_type', 'anuncio')->where('source_id', $id)->first();
            $this->interaccion = 'editar';

            $this->objetivo = $this->anuncio->objetivo;
            $this->tipo = $this->anuncio->tipo;
            $this->tour_url = $this->anuncio->tour_url;
            $this->precio = $this->anuncio->precio;
            $this->primer_precio= $this->anuncio->precio;
            $this->habitaciones = $this->anuncio->habitaciones;
            $this->baños = $this->anuncio->baños;
            $this->metros_cuadrados = $this->anuncio->metros_cuadrados;
            $this->año = $this->anuncio->año;
            $this->estado = $this->anuncio->estado;
            $this->amueblado = $this->anuncio->amueblado;
            $this->foto_portada = $this->anuncio->foto_portada;
            $this->provincia = $this->anuncio->provincia;
            $this->ciudad = $this->anuncio->ciudad;
            $this->codigo_postal = $this->anuncio->codigo_postal;
            $this->direccion = $this->anuncio->direccion;
            $this->descripcion = $this->anuncio->descripcion;

            $this->image_name = 'Imagen Subida';

            $this->caracteristicas = [
                'aire_acondicionado' => $this->last_caracteristicas->aire_acondicionado,
                'terraza' => $this->last_caracteristicas->terraza,
                'despacho' => $this->last_caracteristicas->despacho,
                'armarios_empotrados' => $this->last_caracteristicas->armarios_empotrados,
                'ascensor' => $this->last_caracteristicas->ascensor,
                'garaje' => $this->last_caracteristicas->garaje,
                'jardin' => $this->last_caracteristicas->jardin,
                'piscina' => $this->last_caracteristicas->piscina,
                'pista_de_padel' => $this->last_caracteristicas->pista_de_padel,
                'zona_de_juego' => $this->last_caracteristicas->zona_de_juego,
            ];
        }

        $this->provincias = Provincia::get();
        if($this->provincia){
            $this->ciudades = Ciudad::where('provincia_id', Provincia::where('nombre', $this->provincia)->first()->id)->get();
        } else {
            $this->ciudades = [];
        }
    }

    public function render()
    {
        return view('dashboard.anuncios.anuncio')->layout('layouts.main');
    }

    /**
     * Validate first part of fields and show second part.
     *
     * @return void
     */
    public function validateFirstPart()
    {

        $this->validate([
            'objetivo' => ['required', 'string', 'max:255'],
            'tipo' => ['required', 'string', 'max:255'],
            'tour_url' => ['required', 'url', 'max:255', 'unique:anuncios,tour_url,'.$this->anuncio->id],
            'foto_portada' => ['required', $this->interaccion == 'crear' ? 'image' : 'string', 'max:1024'],
            'precio' => ['required', 'integer', 'min:1', 'digits_between: 1,10'],
            'habitaciones' => ['required', 'integer', 'min:1', 'digits_between: 1,2'],
            'baños' => ['required', 'integer', 'min:1', 'digits_between: 1,2'],
            'metros_cuadrados' => ['required', 'integer', 'min:1', 'digits_between: 1,4'],
            'año' => ['required', 'integer', 'min:1800', 'digits_between: 1,4'],
            'estado' => ['required', 'string', 'max:255'],
            'amueblado' => ['required', 'string', 'max:255'],
        ]);

        $this->parte = 2;
    }

    /**
     * Validate first part of fields and show second part.
     *
     * @return void
     */
    public function validateSecondPart()
    {
        $this->validate([
            'provincia' => ['required', 'string', 'max:255'],
            'ciudad' => ['required', 'string', 'max:255'],
            'codigo_postal' => ['required', 'numeric', 'digits_between: 5,5'],
            'direccion' => ['required', 'string', 'max:255'],
            'descripcion' => ['required', 'string', 'max:800'],
        ]);

        $this->parte = 3;
    }

    /**
     * Validate first part of fields and show second part.
     *
     * @return void
     */
    public function save()
    {
        $this->validate([
            'caracteristicas' => ['required', 'array', new ArrayAtLeastOneRequired()],
        ]);

        $this->fillAnuncioData(true);

    }

    /**
     * Get back to last part.
     *
     * @return void
     */
    public function saveAsDraft() {
        $this->fillAnuncioData(false);
    }


    /**
     * Get back to last part.
     *
     * @return void
     */
    public function getBack() {
        switch ($this->parte) {
            case 2:
                $this->parte = 1;
                break;
            case 3:
                $this->parte = 2;
                break;
        }

    }

    /**
     * Fill data.
     *
     * @return void
     */
    public function fillAnuncioData($publish) {

        $this->anuncio->forceFill([
            'objetivo' => $this->objetivo,
            'tipo' => $this->tipo,
            'tour_url' => $this->tour_url,
            'precio' => $this->precio,
            'habitaciones' => $this->habitaciones,
            'baños' => $this->baños,
            'metros_cuadrados' => $this->metros_cuadrados,
            'año' => $this->año,
            'estado' => $this->estado,
            'amueblado' => $this->amueblado,
            'provincia' => $this->provincia,
            'ciudad' => $this->ciudad,
            'codigo_postal' => $this->codigo_postal,
            'direccion' => $this->direccion,
            'descripcion' => $this->descripcion,
            'user_id' => Auth::user()->id,
            'publico' => $publish,
        ]);

        if($this->primer_precio != $this->anuncio->precio) {
            $this->anuncio->forceFill([
                'antiguo_precio' => $this->primer_precio
            ]);
        }

        if ($publish) {
            $location = Geocoder::getCoordinatesForAddress($this->direccion . ', ' . $this->provincia. ', '. $this->ciudad . ', España');

            $this->anuncio->forceFill([
                'direccion_latitud' => $location['lat'],
                'direccion_longitud' => $location['lng'],
            ]);

            $this->anuncio->url = $this->ciudad . preg_replace('/\s+/', '', strtolower($this->direccion)) . rand(1,100);
        }

        $this->anuncio->save();

        if (isset($this->foto_portada) && $this->foto_portada != $this->anuncio->foto_portada) {
            $imageName = time().$this->anuncio->id;

            $img = ImageCompressor::make($this->foto_portada)
                ->resize(500, null, function ($constraint) { $constraint->aspectRatio(); } )
                ->encode('jpg', 60);
            Storage::disk('anuncios_portadas')->put($imageName . '_500.jpg',  $img);

            $this->anuncio->forceFill([
                'foto_portada' => $imageName,
            ]);

            $this->anuncio->save();
        }

        $this->last_caracteristicas->forceFill([
            'aire_acondicionado' => $this->caracteristicas['aire_acondicionado'],
            'terraza' => $this->caracteristicas['terraza'],
            'despacho' => $this->caracteristicas['despacho'],
            'armarios_empotrados' => $this->caracteristicas['armarios_empotrados'],
            'ascensor' => $this->caracteristicas['ascensor'],
            'garaje' => $this->caracteristicas['garaje'],
            'jardin' => $this->caracteristicas['jardin'],
            'piscina' => $this->caracteristicas['piscina'],
            'pista_de_padel' => $this->caracteristicas['pista_de_padel'],
            'zona_de_juego' => $this->caracteristicas['zona_de_juego'],
            'source_type' => 'anuncio',
            'source_id' => $this->anuncio->id,
        ]);

        $this->last_caracteristicas->save();

        return redirect('/dashboard/anuncios/');
    }

    public function  changeFieldValue() {
        $this->image_name = 'Imagen Subida';
    }

    /**
     * Restore user passowrd information
     *
     * @return void
     */
    public function getCiudades()
    {
        if($this->provincia){
            $fa_provincia = Provincia::where("nombre", $this->provincia)->first();
            $this->ciudades = Ciudad::where("provincia_id", $fa_provincia->id)->get();
        }
    }

}
