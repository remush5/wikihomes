<?php

namespace App\Http\Livewire\Dashboard;

use Livewire\Component;
use App\Models\Anuncio;
use Carbon\Carbon;
use Auth;

class Anuncios extends Component
{
    public $anuncios = [];
    public $anuncio_elegido;

    public function mount()
    {
        $this->anuncios = Anuncio::where('user_id', Auth::user()->id)->get();
    }

    public function render()
    {
        return view('dashboard.anuncios.anuncios')->layout('layouts.main');
    }

    /**
     * Send user to anuncio page
     *
     * @return void
     */
    public function editar($id)
    {
        return redirect('/dashboard/anuncio/' . $id);
    }

    /**
     * Send user to anuncio page
     *
     * @return void
     */
    public function elegirAnuncio($id)
    {
        $this->anuncio_elegido = Anuncio::find($id);
    }

    /**
     * Send user to anuncio page
     *
     * @return void
     */
    public function eliminar()
    {
        $this->anuncio_elegido->forceDelete();
        $this->anuncios = Anuncio::where('user_id', Auth::user()->id)->get();
    }

    /**
     * Send user to anuncio page
     *
     * @return void
     */
    public function renovar($id)
    {
        $this->anuncio = Anuncio::find($id);
        $this->anuncio->updated_at = Carbon::now();
        $this->anuncio->save();
    }
}
