<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\Favorite;
use App\Models\Anuncio;
use Livewire\Component;
use Auth;

class Favoritos extends Component
{
    public $favoritos = [];
    public $anuncios = [];

    public function mount()
    {
        $this->favoritos = Favorite::where('user_id', Auth::user()->id)->pluck('anuncio_id')->toArray();
        $this->anuncios = Anuncio::whereIn('id', $this->favoritos)->get();
    }

    public function render()
    {
        return view('dashboard.favoritos.favoritos')->layout('layouts.main');
    }
}
