<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TerminosUso extends Component
{
    public function render()
    {
        return view('livewire.terminos-uso')->layout('layouts.main');
    }
}
