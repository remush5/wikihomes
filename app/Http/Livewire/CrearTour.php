<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Auth;
use Mail;

class CrearTour extends Component
{
    public $nombre;
    public $email;
    public $mensaje;
    public $mostrar_telefono = false;

    public $loged = false;

    public function mount ()
    {
        if (Auth::user()) {
            $this->loged = true;
        }
    }

    public function render()
    {
        return view('livewire.crear-tour')->layout('layouts.main');
    }

    public function enviarMensaje() {

        $this->validate([
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'mensaje' => ['required', 'string', 'max:800'],
        ]);

        $details = [
            'nombre' => $this->nombre,
            'email' => $this->email,
            'mensaje' => $this->mensaje,
            'como_empezar' => true,
        ];

        Mail::to("lorena.lpg@gmail.com")->send(new \App\Mail\Contacto($details));

        $this->nombre = '';
        $this->email = '';
        $this->mensaje = '';
    }

    public function setTelefono() {
        $this->mostrar_telefono = true;
    }
}
