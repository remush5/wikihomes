<?php

namespace App\Http\Livewire\Anuncios;

use App\Models\Caracteristica;
use App\Models\User;
use App\Models\Favorite;
use Livewire\Component;
use App\Models\Anuncio;
use Auth;
use Mail;

class View extends Component
{
    public $anuncio;
    public $anunciante;
    public $caracteristicas;
    public $favorite;
    public $loged;

    public $nombre;
    public $email;
    public $mensaje;
    public $nombre_sala;
    public $fecha_sala;
    public $mostrar_telefono = false;
    public $videochat = false;

    public $a_caracteristicas = [
        'aire_acondicionado',
        'terraza',
        'despacho',
        'armarios_empotrados',
        'ascensor',
        'garaje',
        'jardin',
        'piscina',
        'pista_de_padel',
        'zona_de_juego',
    ];

    public function mount ($url) {
            $this->anuncio = Anuncio::where('url', $url)->first();
            $this->caracteristicas = Caracteristica::where('source_id', $this->anuncio->id)->where('source_type', 'anuncio')->first();
            $this->favorite = Favorite::where('anuncio_id', $this->anuncio->id)->first();
            $this->anunciante = User::find($this->anuncio->user_id);

            if(Auth::user()) {
                $this->favorite = Favorite::where('anuncio_id', $this->anuncio->id)->where('user_id', Auth::user()->id)->first();
                $this->loged = true;

                $this->nombre = Auth::user()->name;
                $this->email = Auth::user()->email;
            } else {
                $this->favorite = new Favorite();
            }
    }

    public function render()
    {
        return view('anuncios.view',
        [
            'anuncios' => Anuncio::select('caracteristicas.*', 'anuncios.*')->where('anuncios.id','<>', $this->anuncio->id)->where('publico', 1)->where('objetivo', 'vender')->leftJoin('caracteristicas', function($join)
            {
                $join->on('caracteristicas.source_id', '=', 'anuncios.id')
                    ->where('caracteristicas.source_type', '=', 'anuncio');
            })->where(function ($query) {
                if($this->anuncio->habitaciones != null && $this->anuncio->habitaciones != 'all')
                {
                    $query->orWhere('habitaciones', '>=', $this->anuncio->habitaciones);
                }
            })->where(function ($query) {
                if ($this->anuncio->baños != null && $this->anuncio->baños != 'all') {
                    $query->orWhere('baños', '>=', $this->anuncio->baños);
                }
            })->where(function ($query) {
                if ($this->anuncio->metros_cuadrados != null) {
                    $query->orWhere('metros_cuadrados', '>=', $this->anuncio->metros_cuadrados);
                }
            })->where(function ($query) {
                foreach($this->a_caracteristicas as $caracteristica){
                    $query->orWhere('caracteristicas.' . $caracteristica, $this->caracteristicas[$caracteristica]);
                }
            })->get(),
        ])->layout('layouts.main');
    }

    public function like($url) {
        if(Auth::user()) {
            Favorite::setFavorite(Anuncio::where('url', $url)->first()->id);

            $this->anuncios = [];
            $this->searchByFilters();
        }
    }

    public function enviarMensaje() {

        $this->validate([
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'mensaje' => ['required', 'string', 'max:800'],
        ]);

        $details = [
            'nombre' => $this->nombre,
            'email' => $this->email,
            'mensaje' => $this->mensaje,
            'direccion' => $this->anuncio->direccion. ', ' . $this->anuncio->provincia. ', '. $this->anuncio->ciudad. ', '. $this->anuncio->codigo_postal,
            'url' => 'https://www.wikihomes.es/anuncio/'.$this->anuncio->url,
            'foto_portada' => 'https://wikihomes.es/imgs/anuncios_portadas/'.$this->anuncio->foto_portada,
            'como_empezar' => false,
        ];

        Mail::to("lorena.lpg@gmail.com")->send(new \App\Mail\Contacto($details));

        $this->nombre = '';
        $this->email = '';
        $this->mensaje = '';
    }

    public function setTelefono() {
        $this->mostrar_telefono = true;
    }
}
