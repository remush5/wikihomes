<?php

namespace App\Http\Livewire\Anuncios;

use App\Models\Anuncio;
use App\Models\Caracteristica;
use App\Models\Favorite;
use Illuminate\Http\Request;
use Livewire\Component;
use Auth;
use DB;
use Mail;

class Main extends Component
{
    public $a_caracteristicas = [
        'aire_acondicionado',
        'terraza',
        'despacho',
        'armarios_empotrados',
        'ascensor',
        'garaje',
        'jardin',
        'piscina',
        'pista_de_padel',
        'zona_de_juego',
    ];

    public $precio_min;
    public $precio_max;
    public $habitaciones = 'all';
    public $baños = 'all';
    public $superficie_min;
    public $superficie_max;
    public $tipo;
    public $provincia;
    public $order_by = 'direccion';
    public $order_type = 'DESC';

    public $caracteristicas = [
        'aire_acondicionado' => false,
        'terraza' => false,
        'despacho' => false,
        'armarios_empotrados' => false,
        'ascensor' => false,
        'garaje' => false,
        'jardin' => false,
        'piscina' => false,
        'pista_de_padel' => false,
        'zona_de_juego' => false,
    ];

    public $f_precio_min;
    public $f_precio_max;
    public $f_habitaciones = 'all';
    public $f_baños = 'all';
    public $f_superficie_min;
    public $f_superficie_max;

    public $f_caracteristicas = [
        'aire_acondicionado' => false,
        'terraza' => false,
        'despacho' => false,
        'armarios_empotrados' => false,
        'ascensor' => false,
        'garaje' => false,
        'jardin' => false,
        'piscina' => false,
        'pista_de_padel' => false,
        'zona_de_juego' => false,
    ];

    public $url = '';
    public $anuncios = [];
    public $loged = false;

    public $nombre;
    public $email;
    public $mensaje;
    public $mostrar_telefono = false;

    public $active_filt;

    public  function mount (Request $request) {
        if(Auth::user()){
            $this->loged = true;
        }
        $this->tipo = $request->get('tipo');
        $this->provincia = $request->get('provincia');

        $this->precio_min = $request->get('precio_min');
        $this->precio_max = $request->get('precio_max');

        if($request->get('habitaciones')){
            $this->habitaciones = $request->get('habitaciones');
        }

        if($request->get('baños')){
            $this->baños = $request->get('baños');
        }

        $this->superficie_min = $request->get('superficie_min');
        $this->superficie_max = $request->get('superficie_max');

        if($request->get('caracteristicas')){
            $caracteristicas =  explode(',', $request->get('caracteristicas'));
            foreach ($caracteristicas as $caracteristica => $value) {
                $this->caracteristicas[$value] = true;
            }
        }

        $this->searchByFilters();
    }

    public function render()
    {
        return view('anuncios.main', [
            'results' => Anuncio::select('caracteristicas.*', 'anuncios.*')->where('publico', 1)->where('objetivo', 'vender')->leftJoin('caracteristicas', function($join)
            {
                $join->on('caracteristicas.source_id', '=', 'anuncios.id')
                    ->where('caracteristicas.source_type', '=', 'anuncio');
            })->where(function ($query) {
                if($this->precio_min)
                {
                    $query->where('precio', '>=', $this->precio_min);
                }
            })->where(function ($query) {
                if($this->precio_max)
                {
                    $query->where('precio', '<=', $this->precio_max);
                }
            })->where(function ($query) {
                if($this->habitaciones != null && $this->habitaciones != 'all')
                {
                    $query->where('habitaciones', '>=', $this->habitaciones);
                }
            })->where(function ($query) {
                if ($this->baños != null && $this->baños != 'all') {
                    $query->where('baños', '>=', $this->baños);
                }
            })->where(function ($query) {
                if ($this->superficie_min != null) {
                    $query->where('metros_cuadrados', '>=', $this->superficie_min);
                }
            })->where(function ($query) {
                if ($this->f_superficie_min != null) {
                    $query->where('metros_cuadrados', '>=', $this->superficie_min);
                }
            })->where(function ($query) {
                if ($this->superficie_max != null) {
                    $query->where('metros_cuadrados', '<=', $this->superficie_max);
                }
            })->where(function ($query) {
                if(count($this->caracteristicas))
                {
                    foreach($this->caracteristicas as $caracteristica => $value){
                        if ($value) {
                            $query->where('caracteristicas.' . $caracteristica, 1);
                        }
                    }
                }
            })->count(),

        ])->layout('layouts.main');
    }

    public function applyFilters()
    {
        $this->f_precio_min = $this->precio_min;
        $this->f_precio_max = $this->precio_max;
        $this->f_habitaciones = $this->habitaciones;
        $this->f_baños = $this->baños;
        $this->f_superficie_min = $this->superficie_min;
        $this->f_superficie_max = $this->superficie_max;

        $this->f_caracteristicas = [
            'aire_acondicionado' => $this->caracteristicas['aire_acondicionado'],
            'terraza' => $this->caracteristicas['terraza'],
            'despacho' => $this->caracteristicas['despacho'],
            'armarios_empotrados' => $this->caracteristicas['armarios_empotrados'],
            'ascensor' => $this->caracteristicas['ascensor'],
            'garaje' => $this->caracteristicas['garaje'],
            'jardin' => $this->caracteristicas['jardin'],
            'piscina' => $this->caracteristicas['piscina'],
            'pista_de_padel' => $this->caracteristicas['pista_de_padel'],
            'zona_de_juego' => $this->caracteristicas['zona_de_juego'],
        ];

        $this->anuncios = [];
        $this->searchByFilters();
    }

    public function resetFilters()
    {
        $this->precio_min = $this->f_precio_min;
        $this->precio_max = $this->f_precio_max;
        $this->habitaciones = $this->f_habitaciones;
        $this->baños = $this->f_baños;
        $this->superficie_min = $this->f_superficie_min;
        $this->superficie_max = $this->f_superficie_max;

        $this->caracteristicas = [
            'aire_acondicionado' => $this->f_caracteristicas['aire_acondicionado'],
            'terraza' => $this->f_caracteristicas['terraza'],
            'despacho' => $this->f_caracteristicas['despacho'],
            'armarios_empotrados' => $this->f_caracteristicas['armarios_empotrados'],
            'ascensor' => $this->f_caracteristicas['ascensor'],
            'garaje' => $this->f_caracteristicas['garaje'],
            'jardin' => $this->f_caracteristicas['jardin'],
            'piscina' => $this->f_caracteristicas['piscina'],
            'pista_de_padel' => $this->f_caracteristicas['pista_de_padel'],
            'zona_de_juego' => $this->f_caracteristicas['zona_de_juego'],
        ];

        $this->searchByFilters();
    }

    public function addToUrl($prop, $value, $caracteristicas = 0)
    {
        if($caracteristicas == 1){
            $this->url = $this->url . ','. $value;
        } else {
            if($this->url != ''){
                $this->url = $this->url . '&';
            }

            $this->url = $this->url . $prop . '=' . $value;
        }
    }

    public function searchByFilters()
    {
        $this->url = '';

        $query = Anuncio::select('favorites.*', 'caracteristicas.*', 'anuncios.*')->where('publico', 1)->where('objetivo', 'vender')->leftJoin('caracteristicas', function($join)
        {
            $join->on('caracteristicas.source_id', '=', 'anuncios.id')
                ->where('caracteristicas.source_type', '=', 'anuncio');
        });

         if(Auth::user()) {
             $query->leftJoin('favorites', function($join)
             {
                 $join->on('favorites.anuncio_id', '=', 'anuncios.id')
                     ->where('favorites.user_id', '=', Auth::user()->id);
             });
         } else {
             $query->leftJoin('favorites', function($join)
             {
                 $join->on('favorites.anuncio_id', '=', 'anuncios.id')
                     ->where('favorites.user_id', '=', 0);
             });
             $query->addSelect(DB::raw("'not_loged' as 'favorite'"));
         }

        if ($this->f_precio_min != null) {
            $query->where('precio', '>=', $this->f_precio_min);
            $this->addToUrl('precio_min', $this->f_precio_min);
        }

        if ($this->f_precio_max != null) {
            $query->where('precio', '<=', $this->f_precio_max);
            $this->addToUrl('precio_min', $this->f_precio_min);
        }

        if ($this->f_habitaciones != null && $this->f_habitaciones != 'all') {
            $query->where('habitaciones', '>=', $this->f_habitaciones);
            $this->addToUrl('habitaciones', $this->f_habitaciones);
        }

        if ($this->f_baños != null && $this->f_baños != 'all') {
            $query->where('baños', '>=', $this->f_baños);
            $this->addToUrl('baños', $this->f_baños);
        }

        if ($this->f_superficie_min != null) {
            $query->where('metros_cuadrados', '>=', $this->f_superficie_min);
            $this->addToUrl('superficie_min', $this->f_superficie_min);
        }

        if ($this->f_superficie_max != null) {
            $query->where('metros_cuadrados', '<=', $this->f_superficie_max);
            $this->addToUrl('superficie_max', $this->f_superficie_max);
        }

        $numCaracteristicas = 0;

        foreach ($this->caracteristicas as $caracteristica => $value) {
            if ($value) {

                if ($numCaracteristicas == 1) {
                    $query->orWhere('caracteristicas.' . $caracteristica, 1);
                    $this->addToUrl('caracteristicas', $caracteristica, $numCaracteristicas);
                } else {
                    $query->where('caracteristicas.' . $caracteristica, 1);
                    $this->addToUrl('caracteristicas', $caracteristica, $numCaracteristicas);
                    $numCaracteristicas = 1;
                }
            }
        }

        if ($this->tipo != null) {
            $query->where('tipo', $this->tipo);
            $this->addToUrl('tipo', $this->tipo);
        }

        if ($this->provincia != null) {
            $query->where('provincia', $this->provincia);
            $this->addToUrl('provincia', $this->provincia);
        }

        $this->anuncios = $query->orderBy($this->order_by, $this->order_type)->get();

        $this->emit('urlChange', $this->url);
    }

    public function order($by, $type) {
        $this->order_by = $by;
        $this->order_type = $type;

        $this->searchByFilters();
    }

    public function like($url) {
        if(Auth::user()) {
            Favorite::setFavorite(Anuncio::where('url', $url)->first()->id);

            $this->anuncios = [];
            $this->searchByFilters();
        }
    }

    public function enviarMensaje() {

        $this->validate([
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'mensaje' => ['required', 'string', 'max:800'],
        ]);

        $details = [
            'nombre' => $this->nombre,
            'email' => $this->email,
            'mensaje' => $this->mensaje,
            'como_empezar' => true,
        ];

        Mail::to("lorena.lpg@gmail.com")->send(new \App\Mail\Contacto($details));

        $this->nombre = '';
        $this->email = '';
        $this->mensaje = '';
    }

    public function setTelefono() {
        $this->mostrar_telefono = true;
    }

    public function check() {
        $this->active_filt = false;

        if ($this->precio_min != null) {
            $this->active_filt = true;
        }

        if ($this->precio_max != null) {
            $this->active_filt = true;
        }

        if ($this->habitaciones != null && $this->habitaciones != 'all') {
            $this->active_filt = true;
        }

        if ($this->baños != null && $this->baños != 'all') {
            $this->active_filt = true;
        }

        if ($this->superficie_min != null) {
            $this->active_filt = true;
        }

        if ($this->superficie_max != null) {
            $this->active_filt = true;
        }

        foreach ($this->caracteristicas as $caracteristica => $value) {
            if ($value) {
                $this->active_filt = true;
            }
        }

        if ($this->tipo != null) {
            $this->active_filt = true;
        }

        if ($this->provincia != null) {
            $this->active_filt = true;
        }
    }

    public function setItem($var, $val)
    {
        switch ($var) {
            case 'precio_min':
                $this->precio_min = $val;
                break;
            case 'precio_max':
                $this->precio_max = $val;
                break;
            case 'superficie_min':
                $this->superficie_min = $val;
                break;
            case 'superficie_max':
                $this->superficie_max = $val;
                break;
        }

        $this->check();
    }
}
