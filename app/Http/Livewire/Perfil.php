<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Laravel\Fortify\Rules\Password;
use Livewire\Component;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Livewire\WithFileUploads;
use App\Models\Provincia;
use App\Models\Ciudad;
use ImageCompressor;

class Perfil extends Component
{
    use WithFileUploads;

    //base
    public $user;
    public $nombre;
    public $email;
    public $photo;
    public $fecha_de_nacimiento;
    public $descripcion;

    //splash
    public $first;

    //password
    public $contrasena;
    public $nueva_contrasena;
    public $nueva_contrasena_confirmation;
    public $not_password;

    //address
    public $direccion;
    public $provincia;
    public $ciudad;
    public $codigo_postal;
    public $telefono;

    public $provincias = [];
    public $ciudades = [];

    protected $listeners = [
        'set:photo' => 'setPhoto'
    ];

    public function mount()
    {
        //base
        $this->user = Auth::user();
        $this->nombre = $this->user->name;
        $this->email = $this->user->email;
        $this->photo = $this->user->profile_photo_path;
        $this->fecha_de_nacimiento = $this->user->fecha_de_nacimiento;
        $this->descripcion = $this->user->descripcion;

        //address
        $this->direccion = $this->user->direccion;
        $this->provincia = $this->user->provincia;
        $this->ciudad = $this->user->ciudad;
        $this->codigo_postal = $this->user->codigo_postal;
        $this->telefono = $this->user->telefono;

        //password if sign up with google or facebook
        $this->not_password = $this->user->not_password;

        $this->provincias = Provincia::get();
        if($this->provincia) {
            $this->ciudades = Ciudad::where('provincia_id', Provincia::where('nombre', $this->provincia)->first()->id)->get();
        }

        $this->first = $this->user->first;
    }

    public function render()
    {
        return view('dashboard.perfil.perfil')->layout('layouts.main');
    }

    /**
     * Validate and update the given user's profile information.
     *
     * @return void
     */
    public function update()
    {
        $this->validate([
            'nombre' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', 'unique:users,email,'.$this->user->id],
            'fecha_de_nacimiento' => ['nullable', 'date'],
            'descripcion' => ['nullable', 'string', 'max:150'],
        ]);

        if (isset($this->photo) && $this->photo != $this->user->profile_photo_path){
            $imageName = time().($this->user->id * 7);

            $img = ImageCompressor::make($this->photo)
                ->resize(null, 140, function ($constraint) { $constraint->aspectRatio(); } )
                ->encode('jpg', 60);
            Storage::disk('profile')->put($imageName . '_140.jpg',  $img);

            $this->user->forceFill([
                'profile_photo_path' => $imageName,
            ])->save();
        }

        if ($this->email !== $this->user->email &&
            $this->user instanceof MustVerifyEmail) {
            $this->updateVerifiedUser();
        } else {
            $this->user->forceFill([
                'name' => $this->nombre,
                'fecha_de_nacimiento' => $this->fecha_de_nacimiento,
                'descripcion' => $this->descripcion,
            ])->save();
        }
    }

    /**
     * Update the given verified user's profile information.
     *
     * @return void
     */
    protected function updateVerifiedUser()
    {
        $this->user->forceFill([
            'name' => $this->nombre,
            'email' => $this->email,
            'fecha_de_nacimiento' => $this->fecha_de_nacimiento,
            'descripcion' => $this->descripcion,
            'email_verified_at' => null,
        ])->save();

        $this->user->sendEmailVerificationNotification();
    }

    /**
     * Restore user main information
     *
     * @return void
     */
    public function cancelMain()
    {
        $this->nombre = $this->user->nombre;
        $this->email = $this->user->email;
        $this->photo = $this->user->photo;
        $this->fecha_de_nacimiento = $this->user->fecha_de_nacimiento;
        $this->descripcion = $this->user->descripcion;
    }

    /**
     * Validate and update the given user's profile address information.
     *
     * @return void
     */
    public function updateAddress()
    {
        $this->validate([
            'direccion' => ['required', 'string', 'max:255'],
            'provincia' => ['required', 'string', 'max:150'],
            'ciudad' => ['required', 'string', 'max:150'],
            'codigo_postal' => ['required', 'numeric', 'min:10000', 'max:99999'],
            'telefono' => ['nullable', 'numeric', 'min:100000000', 'max:999999999'],
        ]);

        $this->user->forceFill([
            'direccion' => $this->direccion,
            'provincia' => $this->provincia,
            'ciudad' => $this->ciudad,
            'codigo_postal' => $this->codigo_postal,
            'telefono' => $this->telefono,
        ])->save();
    }

    /**
     * Restore user address information
     *
     * @return void
     */
    public function cancelAddress()
    {
        $this->direccion = $this->user->direccion;
        $this->provincia = $this->user->provincia;
        $this->ciudad = $this->user->ciudad;
        $this->codigo_postal = $this->user->codigo_postal;
        $this->telefono = $this->user->telefono;
    }

    /**
     * Validate and update the user's password.
     *
     * @return void
     */
    public function updatePassword()
    {
        Validator::make([
            'contrasena' => $this->contrasena,
            'nueva_contrasena' => $this->nueva_contrasena,
            'nueva_contrasena_confirmation' => $this->nueva_contrasena_confirmation
        ],[
            'contrasena' => ['nullable', 'string'],
            'nueva_contrasena' => ['required', 'string', new Password, 'confirmed'],
        ])->after(function ($validator) {
            if ($this->contrasena == null && !$this->not_password) {
                $validator->errors()->add('contrasena', __('La contraseña actual es necesaría.'));
            }
            if (! Hash::check($this->contrasena, $this->user->password) && !$this->not_password) {
                $validator->errors()->add('contrasena', __('La contraseña facilitada no coincide con la contraseña actual.'));
            }
        })->validateWithBag('updatePassword');

        if($this->not_password) {
            $this->user->forceFill([
                'password' => Hash::make($this->nueva_contrasena),
                'not_password' => 0,
            ])->save();
            $this->not_password = 0;
        } else {
            $this->user->forceFill([
                'password' => Hash::make($this->nueva_contrasena),
            ])->save();
        }

        Auth::login($this->user);
       $this->redirect('/dashboard');
    }

    /**
     * Restore user passowrd information
     *
     * @return void
     */
    public function cancelPassword()
    {
        $this->contrasena = null;
        $this->nueva_contrasena = null;
        $this->nueva_contrasena_confirmation = null;
    }

    /**
     * Restore user passowrd information
     *
     * @return void
     */
    public function getCiudades()
    {
        if($this->provincia){
            $fa_provincia = Provincia::where("nombre", $this->provincia)->first();
            $this->ciudades = Ciudad::where("provincia_id", $fa_provincia->id)->get();
        }
    }

    public function setPhoto($photo){
        $this->photo = $photo;
    }

    public function removeSplash(){
        $this->first = 0;

        $this->user->forceFill([
            'first' => 0,
        ])->save();
    }

}
