<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Provincia;


class Home extends Component
{
    public $provincias;

    public function mount()
    {
        $this->provincias = Provincia::get();
    }

    public function render()
    {
        return view('welcome')->layout('layouts.main');
    }

}
