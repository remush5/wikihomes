<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FacebookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {
        try {

            $user = Socialite::driver('facebook')->user();

            $finduser = User::where('facebook_id', $user->id)->first();

            if ($finduser) {

                Auth::login($finduser);

                return redirect()->intended('dashboard/perfil');

            } else {
                $fields = [
                    'name' => $user->name,
                    'email' => $user->email,
                ];

                $validator = Validator::make($fields, [
                    'name' => 'required|max:120',
                    'email' => 'required|email|unique:users',
                ]);

                if ($validator->fails()) {
                    return redirect('/login')->withErrors(
                        $validator
                    );
                } else {
                    $newUser = new User();

                    $newUser->forceFill([
                        'name' => $user->name,
                        'email' => $user->email,
                        'facebook_id'=> $user->id,
                        'password' => encrypt($this->rand_string(8)),
                        'not_password' => 1,
                        'email_verified_at' => now(),
                    ])->save();

                    Auth::login($newUser);

                    return redirect()->intended('dashboard/perfil');
                }
            }

        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }

    public function rand_string($length)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }

}
