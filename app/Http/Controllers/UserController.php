<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Livewire\Component;

class UserController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Show the user profile screen.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function show(Request $request)
    {
        return view('dashboard.perfil.show', [
            'request' => $request,
            'user' => $request->user(),
        ]);
    }


}
