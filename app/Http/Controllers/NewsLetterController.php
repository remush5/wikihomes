<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class NewsLetterController extends Controller
{
    public function register(Request $request) {
       if(DB::table('newslettersubscribers')->where('email', $request->email)->count() == 0) {
           DB::table('newslettersubscribers')->insertOrIgnore([
               [
                   'email' => $request->email,
                   'created_at' => Carbon::now(),
                   'updated_at' => Carbon::now()
               ],
           ]);

           return "success";
       }
       return "error";
    }
}
