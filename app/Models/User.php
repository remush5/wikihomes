<?php

namespace App\Models;

use App\Notifications\CustomResetNotification as ResetPasswordNotification;
use App\Notifications\CustomVerifyNotification as VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use TwoFactorAuthenticatable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'apellido',
        'fecha_de_nacimiento',
        'provincia',
        'ciudad',
        'direccion',
        'codigo_postal',
        'telefono',
        'descripcion',
        'profile_photo_path',
        'first'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
        'google_id',
        'facebook_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    /*protected $appends = [
        'profile_photo_url',
    ];*/

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    /* public function sendPasswordResetNotification($token)
     {
         $this->notify(new ResetPasswordNotification($token));
     }*/

     /**
      * Send the email verification notification.
      *
      * @return void
      */
   /* public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }*/
}
