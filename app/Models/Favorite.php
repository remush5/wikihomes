<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Favorite extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'favorites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'active',
    ];

    public static function setFavorite($anuncio) {
        if(Favorite::where('user_id', Auth::user()->id)->where('anuncio_id', $anuncio)->first()){
            $favorite = Favorite::where('user_id', Auth::user()->id)->where('anuncio_id', $anuncio)->first();

            $favorite->forceFill([
                'active' => !$favorite->active,
            ])->save();
        } else {
            $favorite = new Favorite();

            $favorite->forceFill([
                'user_id' => Auth::user()->id,
                'anuncio_id' => $anuncio,
            ])->save();
        }
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'user_id',
        'anuncio_id',
    ];
}
