<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id' => '444247599545-kjnlg4qmcdpjq18vqkrs3h2tj5d6odln.apps.googleusercontent.com',
        'client_secret' => '7F7QWh_f2TUmKKSQxiMLjscD',
        'redirect' => 'https://wikihomes.es/auth/google/callback',
    ],

    'facebook' => [
        'client_id' => '146079367313747',
        'client_secret' => '107747b759ccf7aad7c9cfacc649c602',
        'redirect' => 'https://wikihomes.es/auth/facebook/callback',
    ],
];
