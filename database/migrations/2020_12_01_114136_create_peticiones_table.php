<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeticionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peticiones', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('tipo');
            $table->string('provincia');
            $table->integer('habitaciones');
            $table->integer('baños');
            $table->integer('plantas');
            $table->string('amueblado');
            $table->integer('superficie');
            $table->foreignId('user_id');
            $table->text('planos')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peticiones');
    }
}
