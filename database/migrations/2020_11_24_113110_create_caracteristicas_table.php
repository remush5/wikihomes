<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaracteristicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caracteristicas', function (Blueprint $table) {
            $table->id();
            $table->boolean('aire_acondicionado')->default(0);
            $table->boolean('terraza')->default(0);
            $table->boolean('despacho')->default(0);
            $table->boolean('armarios_empotrados')->default(0);
            $table->boolean('ascensor')->default(0);
            $table->boolean('garaje')->default(0);
            $table->boolean('jardin')->default(0);
            $table->boolean('piscina')->default(0);
            $table->boolean('pista_de_padel')->default(0);
            $table->boolean('zona_de_juego')->default(0);
            $table->string('source_type');
            $table->foreignId('source_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caracteristicas');
    }
}
