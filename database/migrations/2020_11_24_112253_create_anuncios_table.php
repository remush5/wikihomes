<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnunciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anuncios', function (Blueprint $table) {
            $table->id();
            $table->string('objetivo')->nullable();
            $table->string('tipo')->nullable();
            $table->string('tour_url')->nullable();
            $table->text('foto_portada')->nullable();
            $table->integer('precio')->nullable();
            $table->integer('habitaciones')->nullable();
            $table->integer('baños')->nullable();
            $table->integer('metros_cuadrados')->nullable();
            $table->integer('año')->nullable();
            $table->string('estado')->nullable();
            $table->string('amueblado')->nullable();
            $table->string('provincia')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('direccion')->nullable();
            $table->double('direccion_latitud')->nullable();
            $table->double('direccion_longitud')->nullable();
            $table->integer('codigo_postal')->nullable();
            $table->text('descripcion')->nullable();
            $table->boolean('publico')->default(0);
            $table->string('url')->nullable();
            $table->foreignId('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anuncios');
    }
}
